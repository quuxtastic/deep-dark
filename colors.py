"""
Color constants
"""

import tcod

hud = {
    'messages': {
        'bg': tcod.Color(27, 28, 22),
        'text': tcod.Color(0xf8, 0xf8, 0xf2),
        'high_text': tcod.orange,
        'low_text': tcod.light_grey,
        'debug_text': tcod.red,
        'mouseover_text': tcod.light_grey,
    },
    'bars': {
        'bg': tcod.Color(27, 28, 22),
        'text': tcod.Color(0xf8, 0xf8, 0xf2),
        'alt_bg': tcod.lightest_gray,
        'alt_text': tcod.black,
        'hp': {
            'bg': tcod.darkest_red,
            'fg': tcod.dark_red,
            'text': tcod.lightest_gray
        },
        'hunger': {
            'bg': tcod.darkest_green,
            'fg': tcod.dark_green,
            'text': tcod.lightest_gray
        },
        'fuel': {
            'bg': tcod.darkest_flame,
            'fg': tcod.dark_flame,
            'text': tcod.lightest_gray
        },
        'inventory': {
            'bg': tcod.darkest_blue,
            'fg': tcod.dark_blue,
            'text': tcod.lightest_gray
        }
    },
    'command': {
        'bg': tcod.darkest_grey,
        'text': tcod.lightest_gray
    },
    'targeting': {
        'active_fg': tcod.light_red,
        'inactive_fg': tcod.light_gray
    }
}

world_map = {
    'bg': tcod.black,
    'wall_dark': tcod.darker_grey,
    'ground_dark': tcod.darkest_grey,
    'wall_light': tcod.darker_sepia,
    'ground_light': tcod.darkest_sepia,
    'wall_dark_diggable': tcod.dark_grey,
    'wall_light_diggable': tcod.dark_sepia
}

ui = {
    'bg': tcod.Color(27, 28, 22),
    'text': tcod.Color(0xf8, 0xf8, 0xf2),
    'desc_text': tcod.light_grey,
    'title_text': tcod.Color(0xf8, 0xf8, 0xf2),
    'title_bg': tcod.Color(27, 28, 22),
    'frame': tcod.Color(0xf8, 0xf8, 0xf2),
    'scrollbar': tcod.lightest_gray,
    'scrollbar_bg': tcod.darkest_grey,
    'selected_bg': tcod.darkest_grey,
    'textinput_bg': tcod.darkest_grey,
    'textinput_text': tcod.lightest_gray,
    'error_text': tcod.light_red,
    'list_entry_bg': tcod.black,
    'list_entry_alt_bg': tcod.darkest_grey,
    'col_header_bg': tcod.lightest_gray,
    'col_header_text': tcod.black,
    'alt_bg': tcod.lightest_gray,
    'alt_text': tcod.black
}
