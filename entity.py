"""
Class to represent all interactable objects
"""

from component import create_component, insert_component_sorted


class Entity:
    """
    Base used to represent all objects that exist on the world map
    Also implements component message-passing
    """

    def __init__(self, factory, uid, template):
        self.factory = factory
        self.__uid = uid
        self.template = template

        self.name = 'unnamed'
        self.clsname = 'unknown'
        self.description = '?'

        self.__components = []

        self.__send_flag = 0
        self.__add_attachments = list()
        self.__remove_attachments = list()

    def __attach(self, comp):
        comp.owner = self
        insert_component_sorted(comp, self.__components)

    def __detach(self, comp):
        self.__components.remove(comp)

    def attach(self, comp):
        comp.owner = self
        if self.__send_flag:
            self.__add_attachments.append(comp)
        else:
            self.__attach(comp)

    def detach(self, comp):
        if self.__send_flag:
            self.__remove_attachments.append(comp)
        else:
            self.__detach(comp)

    def send(self, event, chain):
        """
        Sends a message to all components of this entity
        :param event: Event object (from events)
        :param chain: List that will be appended with any events generated
            by this send call
        :returns: True if any component of this entity processed the event, or
            False otherwise
        """
        chain.append(event)

        # We need to be careful when iterating over the components list because
        # events may cause components to be added or removed. While in this
        # loop we save any add/removes to be done after the loop ends
        self.__send_flag += 1

        result = False

        for comp in self.__components:
            ret = comp.send(event, chain)
            if ret is None:
                continue
            elif not ret:
                result = True
                break
            else:
                result = True

        self.__send_flag -= 1

        if self.__send_flag == 0:
            for comp in self.__add_attachments:
                self.__attach(comp)
            for comp in self.__remove_attachments:
                self.__detach(comp)

            self.__add_attachments.clear()
            self.__remove_attachments.clear()

        return result

    def get(self, key, get_all=False):
        """
        Will try to get the value associated with key from each component
        :param key: Key name
        :param get_all: If True, return all keys instead of the first
        :returns: Value of the key/keys, or None/[] if none found
        """

        results = []

        for comp in self.__components:
            if hasattr(comp, key):
                if get_all:
                    results.append(getattr(comp, key))
                else:
                    return getattr(comp, key)

        if not get_all:
            return None

        return results

    def get_component(self, name):
        """
        :returns: The entity component with the given name, or None
        """
        for comp in self.__components:
            if comp.name() == name:
                return comp

        return None

    def get_all_components(self):
        """
        :returns: List of all components attached to this entity
        """
        return self.__components

    def id(self):
        """
        :returns: unique identifier of this entity
        """
        return self.__uid

    def __eq__(self, other):
        if other is None:
            return False

        return self.id() == other.id()

    def __hash__(self):
        return hash(self.id())

    def __str__(self):
        return '{}: {}'.format(self.name,
                               [comp.name() for comp in self.__components])

    def apply_template(self, table):
        """
        Used to assign components to a newly-created entity based on a template
        :param table: Template table (stored in EntityFactory)
        """
        for entry in table:
            name = entry[0]

            if name == 'entity':
                for key, val in entry[1].items():
                    setattr(self, key, val)
            else:
                if len(entry) == 2:
                    comp = create_component(name, self, **entry[1])
                else:
                    comp = create_component(name, self)

                self.__attach(comp)

    def serialize(self):
        components = [[
            'entity',
            {
                'name': self.name,
                'clsname': self.clsname,
                'description': self.description
            }
        ]]

        for component in self.__components:
            serialized = component.serialize()
            if serialized is not None:
                components.append([component.name(), serialized])

        return {'template': self.template, 'components': components}

    def deserialize(self, data):
        to_add = list()

        for i in range(len(data['components'])):
            name = data['components'][i][0]
            comp_data = data['components'][i][1]

            if name == 'entity':
                self.name = comp_data['name']
                self.clsname = comp_data['clsname']
                self.description = comp_data['description']
            else:
                comp_obj = self.get_component(name)
                if comp_obj is None:
                    comp_obj = create_component(name, self)
                    to_add.append((i, comp_obj))

                comp_obj.deserialize(comp_data)

        for index, comp_obj in to_add:
            self.__components.insert(index, comp_obj)


def filter_entities(ent_list, selector=None):
    """
    Filters a list of entities using the given selector arguments
    :param selector: Filters only entities with the listed components
                     present and with the specified properties
    :returns: Filtered list
    """
    if not selector:
        return ent_list

    out = []

    def check(ent):
        ent_props = selector.get('entity')
        comp_list = selector.get('components')

        if ent_props:
            for k, v in ent_props.items():
                if v != ent.__dict__[k]:
                    return False

        if comp_list:
            for name in comp_list.keys():
                comp = ent.get_component(name)
                if not comp:
                    return False

                vals = comp_list[name]
                if vals is not None:
                    for k, v in vals.items():
                        if v != comp.__dict__[k]:
                            return False

        return True

    for ent in ent_list:
        if check(ent):
            out.append(ent)

    return out
