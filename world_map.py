"""
The world map
"""

import collections
import copy
import io
import numpy
import tcod

from geom import Coord, Rect
from events import SpawnEvent
from entity import filter_entities

# bitmap for tiles
PASSABLE_BIT = 0b00000001
TRANSPARENT_BIT = 0b00000010
DIGGABLE_BIT = 0b00000100
EXPLORED_BIT = 0b10000000


class WorldMap:
    """
    World map made up of a tile map and Entities
    """

    def __init__(self, index, width, height, ctx):
        """
        Create a new map
        :param width,height: Dimensions of the world map
        """
        self.index = index
        self.width = width
        self.height = height

        self.__map_rect = Rect(Coord(0, 0), Coord(width, height))

        self.tile_map = numpy.zeros([width, height], numpy.uint8)
        self.dig_map = numpy.zeros([width, height], numpy.uint8)

        self.__map = tcod.map.Map(width, height)

        self.__entity_map = collections.defaultdict(list)
        self.__entity_list = []

        self.ctx = ctx

        self.player = None
        self.player_spawn_pos = None

        self.stairs_up_pos = None
        self.stairs_down_pos = None

    def __get_ents_on_map(self, pos):
        if not self.__map_rect.contains(pos):
            return []

        ents = self.__entity_map.get((pos.x, pos.y))
        if ents is None:
            return []

        return ents

    def __update_los(self, pos):
        self.__map.transparent[pos.y, pos.x] = True
        if not self.tile_map[pos.x, pos.y] & TRANSPARENT_BIT:
            self.__map.transparent[pos.y, pos.x] = False
            return

        for ent in self.__get_ents_on_map(pos):
            if ent.block_los:
                self.__map.transparent[pos.y, pos.x] = False
                return

    def __update_walk(self, pos):
        self.__map.walkable[pos.y, pos.x] = 1
        if not self.tile_map[pos.x, pos.y] & PASSABLE_BIT:
            self.__map.walkable[pos.y, pos.x] = 0
            return

        for ent in self.__get_ents_on_map(pos):
            if ent.block_mov:
                self.__map.walkable[pos.y, pos.x] = 0
                return

    def __put_ent_at(self, pos, ent):
        tile_list = self.__entity_map.get((pos.x, pos.y))
        if tile_list is not None:
            tile_list.append(ent)
        else:
            self.__entity_map[(pos.x, pos.y)].append(ent)

    def __del_ent_at(self, pos, ent):
        try:
            tile_list = self.__entity_map.get((pos.x, pos.y))
            if tile_list is not None:
                tile_list.remove(ent)
        except ValueError:
            print("pos = {}, ent = {}".format(pos, ent.owner.name))

    def create(self,
               template_name,
               chain,
               position,
               defer_placement=False,
               overrides=None,
               level=None):
        """
        As factory.create(), but also spawns the entity onto the world map
        """

        if level is None:
            level = self.index

        ent = self.ctx.entity_factory.create(
            template_name, overrides, level=level)
        ent.send(
            SpawnEvent(None, self.ctx, self, position, defer_placement), chain)

        return ent

    def update_tile(self, pos):
        self.__update_los(pos)
        self.__update_walk(pos)

    def spawn(self, ent):
        comp = ent.get_component('world')
        self.__put_ent_at(comp.position, comp)
        self.update_tile(comp.position)

        self.__entity_list.append(ent)

    def despawn(self, ent):
        """
        Called when an entity is removed from the map
        """
        comp = ent.get_component('world')
        self.__del_ent_at(comp.position, comp)
        self.update_tile(comp.position)

        self.__entity_list.remove(ent)

    def on_move_ent(self, ent, start, dest):
        """
        Called when an entity moves so the map can update internal
        state
        """
        comp = ent.get_component('world')
        self.__del_ent_at(start, comp)
        self.update_tile(start)

        self.__put_ent_at(dest, comp)
        self.update_tile(dest)

    def is_passable(self, pos):
        """
        :returns: True if the given tile is walkable
        """
        return self.__map.walkable[pos.y, pos.x]

    def is_transparent(self, pos):
        """
        :returns: True if the given tile does not block LoS
        """
        return self.__map.transparent[pos.y, pos.x]

    def can_dig(self, pos):
        return self.tile_map[pos.x, pos.y] & DIGGABLE_BIT

    def dig(self, pos, dmg):
        if not self.tile_map[pos.x, pos.y] & DIGGABLE_BIT:
            return None

        self.dig_map[pos.x, pos.y] = max(self.dig_map[pos.x, pos.y] - dmg, 0)
        if self.dig_map[pos.x, pos.y] == 0:
            self.tile_map[pos.x, pos.y] |= PASSABLE_BIT
            self.tile_map[pos.x, pos.y] |= TRANSPARENT_BIT
            self.tile_map[pos.x, pos.y] &= ~DIGGABLE_BIT
            self.update_tile(pos)

        return self.dig_map[pos.x, pos.y]

    def get_all_ents(self, selector=None):
        return filter_entities(self.__entity_list, selector)

    def get_ents_at(self, pos, selector=None):
        """
        :returns: A list of all entities at the given tile
        """
        return filter_entities(
            [ent.owner for ent in self.__get_ents_on_map(pos)], selector)

    def get_ents_in_fov(self,
                        viewer,
                        view_distance,
                        algorithm=0,
                        selector=None):
        self.compute_fov(
            viewer, view_distance=view_distance, algorithm=algorithm)

        out = []
        for ent in self.get_all_ents(selector):
            if self.in_fov(
                    viewer,
                    ent.get_component('world').position,
                    view_distance=view_distance,
                    algorithm=algorithm,
                    recompute=False):
                out.append(ent)

        return out

    def get_nearest_ents_in_fov(self,
                                viewer,
                                view_distance,
                                algorithm=0,
                                selector=None):
        self.compute_fov(
            viewer, view_distance=view_distance, algorithm=algorithm)

        def check(pos):
            if self.in_fov(
                    viewer,
                    pos,
                    view_distance=view_distance,
                    algorithm=0,
                    recompute=False):
                return self.get_ents_at(pos, selector)

            return None

        # scan concentric rectangles
        radius = 1
        while radius < view_distance:
            # top of rectangle
            for x in range(-radius, radius + 1):
                ents = check(Coord(viewer.x + x, viewer.y - radius))
                if ents:
                    return ents
            # bottom of rectangle
            for x in range(-radius, radius + 1):
                ents = check(Coord(viewer.x + x, viewer.y + radius))
                if ents:
                    return ents
            # rectangle sides
            for y in range(-radius, radius + 1):
                ents = check(Coord(viewer.x - radius, viewer.y + y))
                if ents:
                    return ents
                ents = check(Coord(viewer.x + radius, viewer.y + y))
                if ents:
                    return ents

            radius = radius + 1

        return []

    def get_ents_in_radius(self, position, radius, selector=None):
        out = []
        for y in range(-radius, radius + 1):
            for x in range(-radius, radius + 1):
                out.extend(
                    self.get_ents_at(
                        Coord(position.x + x, position.y + y), selector))

        return out

    def get_ents_in_rect(self, rect, selector=None):
        out = []
        for coord in rect.coords():
            out.extend(self.get_ents_at(coord, selector))

        return out

    def calc_path(self, start, goal):
        """
        Uses A* to calculate a path from (sx, sy) to (tx, ty),
        taking into account walkable tiles and any entities that
        block movement.
        :param sx,sy: Starting location
        :param tx,ty: Goal location
        :returns: A list of coordinate tuples making up the full path.
        """
        walk_map = numpy.zeros([self.width, self.height], numpy.uint8)
        for y in range(self.height):
            for x in range(self.width):
                walk_map[x, y] = self.__map.walkable[y, x]
        walk_map[start.x, start.y] = 1
        walk_map[goal.x, goal.y] = 1

        astar = tcod.path.AStar(walk_map)
        return astar.get_path(start.x, start.y, goal.x, goal.y)

    def compute_fov(self,
                    viewer,
                    view_distance,
                    algorithm=0,
                    recompute=True,
                    set_explored=False):
        """
        Get a map of all tiles visible from the perspective of viewer.
        :param viewer: the viewer position
        :param recompute: The result of a FoV computation is cached. If
            you know you are going to perform many FoV comparisons from
            the same origin, you can skip the computation step to speed
            things up.
        :param set_explored: If True, the map will use this FoV info to
            automatically set the EXPLORED_BIT on visible tiles. Used
            during rendering when computing the player's FoV
        :returns: A boolean map, from the viewer's perspective, of all
            tiles visible to the viewer. True only if the tiles are
            visible. NOTE: this array is in column-major order, so
            must be indexed as [y, x]
        """

        if not self.__map_rect.contains(viewer):
            raise ValueError("{} outside map bounds".format(viewer))

        if recompute:
            self.__map.compute_fov(
                viewer.x,
                viewer.y,
                radius=view_distance,
                algorithm=algorithm,
                light_walls=True)

        if set_explored:
            for y in range(self.height):
                for x in range(self.width):
                    if self.__map.fov[y, x]:
                        self.tile_map[x, y] |= EXPLORED_BIT

        return self.__map.fov

    def in_fov(self, viewer, pos, view_distance, algorithm=0, recompute=True):
        """
        Check if a location is within a viewer's LoS
        :param viewer: the viewer position
        :param x,y: Location in question
        :param recompute: See compute_fov(); we use the cached value here
        :returns: True if the given location is in the viewer's LoS
        """
        if not (self.__map_rect.contains(viewer)
                and self.__map_rect.contains(pos)):
            return False

        return self.compute_fov(
            viewer,
            view_distance=view_distance,
            algorithm=algorithm,
            recompute=recompute)[pos.y, pos.x]

    def set_tile(self, pos, tile_data, dig_data=None):
        """
        Sets the data of a single tile and updates pathing/LOS info
        """
        self.tile_map[pos.x, pos.y] = tile_data
        if dig_data is not None:
            self.dig_map[pos.x, pos.y] = dig_data

        self.update_tile(pos)

    def calc_explored(self):
        open_tiles = 0
        explored_tiles = 0
        for y in range(self.height):
            for x in range(self.width):
                if self.tile_map[x, y] & PASSABLE_BIT:
                    open_tiles += 1
                    if self.tile_map[x, y] & EXPLORED_BIT:
                        explored_tiles += 1

        return (open_tiles, explored_tiles)

    def reveal_map(self):
        for coord in self.__map_rect.coords():
            self.tile_map[coord.x, coord.y] |= EXPLORED_BIT

    def serialize(self):
        def _serialize_tiles(tiles):
            memfile = io.BytesIO()
            numpy.save(memfile, tiles, allow_pickle=False)
            memfile.seek(0)
            return memfile.read().decode('latin-1')

        saved_ents = collections.defaultdict(list)
        for y in range(self.__map_rect.dim.y):
            for x in range(self.__map_rect.dim.x):
                for ent in self.get_ents_at(Coord(x, y)):
                    saved_ents['{},{}'.format(x, y)].append(ent.id())

        return {
            'dimensions': [self.width, self.height],
            'tiles':
            _serialize_tiles(self.tile_map),
            'dig':
            _serialize_tiles(self.dig_map),
            'entities':
            saved_ents,
            'player':
            int(self.player.id()) if self.player else None,
            'stairs_up': [self.stairs_up_pos.x, self.stairs_up_pos.y]
            if self.stairs_up_pos else None,
            'stairs_down': [self.stairs_down_pos.x, self.stairs_down_pos.y]
            if self.stairs_down_pos else None
        }

    def deserialize(self, data):
        def _deserialize_tiles(tile_data):
            memfile = io.BytesIO()
            memfile.write(tile_data.encode('latin-1'))
            memfile.seek(0)
            return numpy.load(memfile)

        self.tile_map = _deserialize_tiles(data['tiles'])
        self.dig_map = _deserialize_tiles(data['dig'])

        for k, v in data['entities'].items():
            coord = Coord(*[int(s) for s in k.split(',')])
            for ent_id in v:
                ent = self.ctx.entity_factory.get_ent_by_id(ent_id)
                self.__put_ent_at(coord, ent.get_component('world'))
                self.__entity_list.append(ent)
                ent.send(SpawnEvent(None, self.ctx, self, coord, True), [])

        for y in range(self.height):
            for x in range(self.width):
                self.__update_los(Coord(x, y))
                self.__update_walk(Coord(x, y))

        self.stairs_up_pos = Coord(
            *[int(x)
              for x in data['stairs_up']]) if data['stairs_up'] else None
        self.stairs_down_pos = Coord(
            *[int(x)
              for x in data['stairs_down']]) if data['stairs_down'] else None

        if data['player']:
            self.player = self.ctx.entity_factory.get_ent_by_id(
                int(data['player']))

    def _fill_map(self, entities, tiles=None):
        """
        Called when a new map is generated to set all the tiles and entities
        Mainly a convenience so we only need to update __map once for each
        x, y coord at the end.
        """

        if tiles:
            self.tile_map = copy.copy(tiles)

        self.__entity_map.clear()
        for ent in entities:
            world_comp = ent.get_component('world')
            if world_comp is not None and self.__map_rect.contains(
                    world_comp.position):
                self.__put_ent_at(world_comp.position, world_comp)
                self.__entity_list.append(ent)
                ent.send(
                    SpawnEvent(None, self.ctx, self, world_comp.position,
                               True), [])

        for y in range(self.height):
            for x in range(self.width):
                self.__update_los(Coord(x, y))
                self.__update_walk(Coord(x, y))

    def _debug_print(self):
        lines = []

        for y in range(self.height):
            line = []
            for x in range(self.width):
                tile_coord = Coord(x, y)

                if tile_coord == self.player.get_component('world').position:
                    line.append('@')
                elif self.get_ents_at(
                        tile_coord, selector={'components': {
                            'stairs': None
                        }}):
                    line.append('>')
                elif self.get_ents_at(tile_coord):
                    line.append('x')
                elif self.tile_map[x, y] & DIGGABLE_BIT:
                    line.append('H')
                elif self.tile_map[x, y] & TRANSPARENT_BIT:
                    line.append(' ')
                else:
                    line.append('#')

            lines.append(''.join(line))

        return lines
