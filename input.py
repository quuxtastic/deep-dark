"""
User input handling
"""

from enum import Enum, auto
import tcod


class Commands(Enum):
    MOVE_N = auto()
    MOVE_S = auto()
    MOVE_E = auto()
    MOVE_W = auto()
    MOVE_NW = auto()
    MOVE_NE = auto()
    MOVE_SW = auto()
    MOVE_SE = auto()

    INTERACT_N = auto()
    INTERACT_S = auto()
    INTERACT_E = auto()
    INTERACT_W = auto()
    INTERACT_NW = auto()
    INTERACT_NE = auto()
    INTERACT_SW = auto()
    INTERACT_SE = auto()

    ATTACK_N = auto()
    ATTACK_S = auto()
    ATTACK_E = auto()
    ATTACK_W = auto()
    ATTACK_NW = auto()
    ATTACK_NE = auto()
    ATTACK_SW = auto()
    ATTACK_SE = auto()

    WAIT = auto()

    PICK_UP = auto()

    INTERACT = auto()

    INVENTORY_OPEN = auto()
    INVENTORY_USE = auto()
    INVENTORY_DROP = auto()
    INVENTORY_EQUIP_TOGGLE = auto()

    MESSAGES_SCROLL_UP = auto()
    MESSAGES_SCROLL_DOWN = auto()

    CHARINFO_OPEN = auto()

    EXIT = auto()
    CONFIRM = auto()

    PROMPT_CONFIRM = auto()
    PROMPT_DENY = auto()

    MENU_NEXT = auto()
    MENU_PREV = auto()

    HELP_TOGGLE = auto()

    BACKSPACE = auto()
    DELETE = auto()

    DELETE_SAVE = auto()

    ENABLE_TEXT_COMMAND = auto()

    NEXT_TARGET = auto()

    LANTERN_TOGGLE = auto()

    INSPECT = auto()

    LEVEL_UP = auto()


class CommandContexts(Enum):
    MENU_HELP = auto()
    MENU_INVENTORY = auto()
    MENU_MAIN = auto()
    MENU_LOAD_GAME = auto()
    MENU_CHAR_STATS = auto()

    PROMPT = auto()

    PLAYER = auto()
    PLAYER_INTERACT = auto()
    PLAYER_ATTACK = auto()
    TARGETING = auto()
    TEXT_COMMAND_INPUT = auto()

    TEXT_INPUT = auto()


key_bindings = {
    CommandContexts.MENU_HELP: {
        tcod.KEY_UP: Commands.MENU_PREV,
        tcod.KEY_DOWN: Commands.MENU_NEXT,
        tcod.KEY_F1: Commands.HELP_TOGGLE,
        tcod.KEY_ESCAPE: Commands.HELP_TOGGLE
    },
    CommandContexts.MENU_INVENTORY: {
        tcod.KEY_UP: Commands.MENU_PREV,
        tcod.KEY_DOWN: Commands.MENU_NEXT,
        '[': Commands.MENU_PREV,
        ']': Commands.MENU_NEXT,
        tcod.KEY_KP8: Commands.MENU_PREV,
        tcod.KEY_KP2: Commands.MENU_NEXT,
        'u': Commands.INVENTORY_USE,
        'd': Commands.INVENTORY_DROP,
        'e': Commands.INVENTORY_EQUIP_TOGGLE,
        tcod.KEY_ESCAPE: Commands.EXIT,
        tcod.KEY_F1: Commands.HELP_TOGGLE
    },
    CommandContexts.MENU_CHAR_STATS: {
        tcod.KEY_UP: Commands.MENU_PREV,
        tcod.KEY_DOWN: Commands.MENU_NEXT,
        '[': Commands.MENU_PREV,
        ']': Commands.MENU_NEXT,
        tcod.KEY_KP8: Commands.MENU_PREV,
        tcod.KEY_KP2: Commands.MENU_NEXT,
        'e': Commands.INVENTORY_EQUIP_TOGGLE,
        'l': Commands.LEVEL_UP,
        tcod.KEY_ESCAPE: Commands.EXIT,
        tcod.KEY_F1: Commands.HELP_TOGGLE
    },
    CommandContexts.MENU_MAIN: {
        tcod.KEY_UP: Commands.MENU_PREV,
        tcod.KEY_DOWN: Commands.MENU_NEXT,
        tcod.KEY_ENTER: Commands.CONFIRM,
        tcod.KEY_ESCAPE: Commands.EXIT,
        'd': Commands.DELETE_SAVE
    },
    CommandContexts.PROMPT: {
        'y': Commands.PROMPT_CONFIRM,
        tcod.KEY_ENTER: Commands.PROMPT_CONFIRM,
        'n': Commands.PROMPT_DENY,
        tcod.KEY_ESCAPE: Commands.PROMPT_DENY
    },
    CommandContexts.TEXT_COMMAND_INPUT: {
        tcod.KEY_ESCAPE: Commands.EXIT,
        tcod.KEY_ENTER: Commands.CONFIRM
    },
    CommandContexts.PLAYER: {
        tcod.KEY_UP: Commands.MOVE_N,
        tcod.KEY_KP8: Commands.MOVE_N,
        'w': Commands.MOVE_N,
        tcod.KEY_DOWN: Commands.MOVE_S,
        tcod.KEY_KP2: Commands.MOVE_S,
        'x': Commands.MOVE_S,
        tcod.KEY_LEFT: Commands.MOVE_W,
        tcod.KEY_KP4: Commands.MOVE_W,
        'a': Commands.MOVE_W,
        tcod.KEY_RIGHT: Commands.MOVE_E,
        tcod.KEY_KP6: Commands.MOVE_E,
        'd': Commands.MOVE_E,
        tcod.KEY_KP7: Commands.MOVE_NW,
        'q': Commands.MOVE_NW,
        tcod.KEY_KP9: Commands.MOVE_NE,
        'e': Commands.MOVE_NE,
        tcod.KEY_KP1: Commands.MOVE_SW,
        'z': Commands.MOVE_SW,
        tcod.KEY_KP3: Commands.MOVE_SE,
        'c': Commands.MOVE_SE,
        tcod.KEY_SPACE: Commands.WAIT,
        'g': Commands.PICK_UP,
        'i': Commands.INVENTORY_OPEN,
        's': Commands.CHARINFO_OPEN,
        tcod.KEY_ESCAPE: Commands.EXIT,
        tcod.KEY_F1: Commands.HELP_TOGGLE,
        tcod.KEY_ENTER: Commands.INTERACT,
        tcod.KEY_KP5: Commands.INTERACT,
        '\\': Commands.ENABLE_TEXT_COMMAND,
        '[': Commands.MESSAGES_SCROLL_UP,
        ']': Commands.MESSAGES_SCROLL_DOWN,
        'l': Commands.LANTERN_TOGGLE,
        ';': Commands.INSPECT
    },
    CommandContexts.PLAYER_INTERACT: {
        tcod.KEY_UP: Commands.INTERACT_N,
        tcod.KEY_KP8: Commands.INTERACT_N,
        tcod.KEY_DOWN: Commands.INTERACT_S,
        tcod.KEY_KP2: Commands.INTERACT_S,
        tcod.KEY_LEFT: Commands.INTERACT_W,
        tcod.KEY_KP4: Commands.INTERACT_W,
        tcod.KEY_RIGHT: Commands.INTERACT_E,
        tcod.KEY_KP6: Commands.INTERACT_E,
        tcod.KEY_KP7: Commands.INTERACT_NW,
        tcod.KEY_KP9: Commands.INTERACT_NE,
        tcod.KEY_KP1: Commands.INTERACT_SW,
        tcod.KEY_KP3: Commands.INTERACT_SE,
    },
    CommandContexts.PLAYER_ATTACK: {
        tcod.KEY_UP: Commands.ATTACK_N,
        tcod.KEY_KP8: Commands.ATTACK_N,
        tcod.KEY_DOWN: Commands.ATTACK_S,
        tcod.KEY_KP2: Commands.ATTACK_S,
        tcod.KEY_LEFT: Commands.ATTACK_W,
        tcod.KEY_KP4: Commands.ATTACK_W,
        tcod.KEY_RIGHT: Commands.ATTACK_E,
        tcod.KEY_KP6: Commands.ATTACK_E,
        tcod.KEY_KP7: Commands.ATTACK_NW,
        tcod.KEY_KP9: Commands.ATTACK_NE,
        tcod.KEY_KP1: Commands.ATTACK_SW,
        tcod.KEY_KP3: Commands.ATTACK_SE,
    },
    CommandContexts.TARGETING: {
        tcod.KEY_UP: Commands.MOVE_N,
        tcod.KEY_KP8: Commands.MOVE_N,
        tcod.KEY_DOWN: Commands.MOVE_S,
        tcod.KEY_KP2: Commands.MOVE_S,
        tcod.KEY_LEFT: Commands.MOVE_W,
        tcod.KEY_KP4: Commands.MOVE_W,
        tcod.KEY_RIGHT: Commands.MOVE_E,
        tcod.KEY_KP6: Commands.MOVE_E,
        tcod.KEY_KP7: Commands.MOVE_NW,
        tcod.KEY_KP9: Commands.MOVE_NE,
        tcod.KEY_KP1: Commands.MOVE_SW,
        tcod.KEY_KP3: Commands.MOVE_SE,
        tcod.KEY_TAB: Commands.NEXT_TARGET,
        tcod.KEY_ESCAPE: Commands.EXIT,
        tcod.KEY_ENTER: Commands.CONFIRM,
        tcod.KEY_F1: Commands.HELP_TOGGLE
    },
    CommandContexts.TEXT_INPUT: {
        tcod.KEY_BACKSPACE: Commands.BACKSPACE
    }
}

__MOD_KEY_BINDINGS = {
    CommandContexts.PLAYER: {
        'lalt': CommandContexts.PLAYER_INTERACT,
        'lctrl': CommandContexts.PLAYER_ATTACK
    }
}


def map_key(keyin, context):
    """
    Translate keyboard event into an input command
    :param keyin: tcod Key device
    :param context: keybinding context
    :returns: Commands code
    """
    ctx = key_bindings.get(context)
    mod_ctx = __MOD_KEY_BINDINGS.get(context)

    if mod_ctx:
        for mod_key, new_ctx in mod_ctx.items():
            if getattr(keyin, mod_key, False):
                ctx = key_bindings.get(new_ctx)
                break

    return ctx.get(chr(keyin.c), ctx.get(keyin.vk))


def map_alpha_key(keyin):
    """
    Translate keyboard event into an alphanumeric key
    """

    return keyin.text if keyin.vk == tcod.KEY_TEXT else None

    # if keyin.vk == tcod.KEY_TEXT:
    #     return keyin.text

    # code = keyin.c
    # if code < ord(' ') or code > ord('z'):
    #     return None

    # return chr(code)


def map_text_input(keyin, cur_str):
    out_str = cur_str

    command = map_key(keyin, CommandContexts.TEXT_INPUT)

    if command == Commands.BACKSPACE and len(cur_str) > 0:
        out_str = cur_str[:-1]

    else:
        key = map_alpha_key(keyin)
        if key is not None:
            out_str = cur_str + key

    return out_str
