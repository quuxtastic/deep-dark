from world_map import WorldMap, PASSABLE_BIT, TRANSPARENT_BIT, DIGGABLE_BIT
from geom import Coord, Rect, line_bresenham, intersect, random_polygon_from_rect
import constants

MAX_ROOMS = 40
MIN_ROOM_DIM = 8
MAX_ROOM_DIM = 12
ROOM_PADDING = 3
MAX_POINTS_PER_SIDE = 3

MOB_DISTRIBUTION = [0, 0, 1, 1, 2, 2, 2, 3]
ITEM_DISTRIBUTION = [0, 0, 0, 1, 1, 2]
SHROOM_DISTRIBUTION = [0, 1, 1, 1, 2, 2, 3, 4]


def generate(map_level, ctx):
    world_map = WorldMap(map_level, constants.MAP_WIDTH, constants.MAP_HEIGHT,
                         ctx)
    world_rect = Rect(
        Coord(1, 1), Coord(constants.MAP_WIDTH - 2, constants.MAP_HEIGHT - 2))

    entities = []

    player_start_pos = Coord(-1, -1)
    stairs_up_pos = Coord(-1, -1)
    stairs_down_pos = Coord(-1, -1)

    def set_floor(pos):
        world_map.tile_map[pos.x, pos.y] |= (PASSABLE_BIT | TRANSPARENT_BIT)

    def set_wall(pos):
        world_map.tile_map[pos.x, pos.y] = 0

    def is_passable(pos):
        if pos.x < 0 or pos.x >= world_rect.dim.x:
            return False
        if pos.y < 0 or pos.y >= world_rect.dim.y:
            return False

        return world_map.tile_map[pos.x, pos.y] & PASSABLE_BIT

    def is_occupied(pos):
        return (pos in (player_start_pos, stairs_up_pos,
                        stairs_down_pos)) or any([
                            ent for ent in entities
                            if ent.get_component('world').position == pos
                            and ent.get_component('world').block_mov
                        ])

    def can_pillar(pos):
        faces = [
            pos + Coord(0, 1), pos + Coord(1, 0), pos + Coord(-1, 0),
            pos + Coord(0, -1)
        ]
        impassable_faces = 0
        for face_pos in faces:
            impassable_faces += int(not is_passable(face_pos))

        return impassable_faces <= 1

    def carve_room(room_rect):

        poly = random_polygon_from_rect(ctx.rand, room_rect, ROOM_PADDING,
                                        MAX_POINTS_PER_SIDE)

        centroid = poly.centroid()

        for coord in room_rect.coords():
            if poly.contains(coord):
                set_floor(coord)

        for coord in room_rect.random_coords(
                ctx.rand, max(1, int(room_rect.dim.x * room_rect.dim.y / 5))):
            if coord != centroid and can_pillar(coord):
                set_wall(coord)

        return poly

    def carve_tunnel(start, end):
        for coord in line_bresenham(start, end):
            set_floor(coord)

    def place_entities(room_rect, count, template):
        cur_count = 0
        while cur_count < count:
            pos = room_rect.random_coords(ctx.rand, 1)[0]
            if pos != player_start_pos \
               and is_passable(pos) \
               and not is_occupied(pos):
                entities.append(
                    world_map.create(template, [], pos, defer_placement=True))
                cur_count += 1

    rooms = list()
    shrunk_rooms = list()
    prev_center = None
    for _ in range(MAX_ROOMS):
        rw = ctx.rand.randint(MIN_ROOM_DIM, MAX_ROOM_DIM)
        rh = ctx.rand.randint(MIN_ROOM_DIM, MAX_ROOM_DIM)
        rx = ctx.rand.randint(0, world_rect.dim.x - rw - 1)
        ry = ctx.rand.randint(0, world_rect.dim.y - rh - 1)

        new_room_rect = Rect(Coord(rx, ry), Coord(rw, rh))

        shrunk_rect = Rect(new_room_rect.pos + Coord(2, 2),
                           new_room_rect.dim - Coord(4, 4))

        for other_room in shrunk_rooms:
            if intersect(shrunk_rect, other_room):
                break
        else:
            new_poly = carve_room(new_room_rect)
            new_center = new_poly.centroid()

            if len(rooms) == 0:
                player_start_pos = world_map.player_spawn_pos = new_center

                if map_level > 0:
                    entities.append(
                        world_map.create(
                            'stairs_up', [],
                            new_center,
                            defer_placement=True,
                            overrides={
                                'stairs': {
                                    'dest_level': map_level - 1
                                }
                            }))
                    stairs_up_pos = world_map.stairs_up_pos = new_center
            else:
                carve_tunnel(prev_center, new_center)

            prev_center = new_center

            shrunk_rooms.append(shrunk_rect)
            rooms.append(new_room_rect)

    entities.append(
        world_map.create(
            'stairs_down', [],
            new_center,
            defer_placement=True,
            overrides={'stairs': {
                'dest_level': map_level + 1
            }}))
    stairs_down_pos = world_map.stairs_down_pos = new_center

    for rect in rooms[(0 if map_level > 0 else 1):]:
        place_entities(rect, ctx.rand.choice(MOB_DISTRIBUTION), 'gen_monster')
        place_entities(rect, ctx.rand.choice(ITEM_DISTRIBUTION), 'gen_item')
        place_entities(rect, ctx.rand.choice(SHROOM_DISTRIBUTION),
                       'gen_mushroom')

    for coord in world_rect.coords():
        if not is_passable(coord) and ctx.rand.perc(30):
            world_map.tile_map[coord.x, coord.y] |= DIGGABLE_BIT
            world_map.dig_map[coord.x, coord.y] = 100

    world_map._fill_map(entities)

    return world_map
