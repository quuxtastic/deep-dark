"""
Pre-designed rooms that can be dynamically placed in maps
"""

from geom import Coord
from world_map import PASSABLE_BIT, TRANSPARENT_BIT


class Blueprint:
    def __init__(self, bounds):
        pass


def get_dimensions(blueprint):
    x_max = 0
    for line in blueprint:
        if len(line) > x_max:
            x_max = len(line)

    return Coord(x_max, len(blueprint))


def apply_to_map(new_map, offset, blueprint, ent_list):
    for y_index in range(len(blueprint)):
        for x_index in range(len(blueprint[y_index])):
            room_pos = Coord(x_index, y_index) + offset
            char = blueprint[y_index][x_index]

            __CHAR_MAP[char](char, new_map, room_pos, ent_list)


def __set_wall(char, out_map, pos, ent_list):
    pass


def __set_floor(_, out_map, pos, ent_list):
    out_map.tile_map[pos.x, pos.y] = PASSABLE_BIT | TRANSPARENT_BIT


def __set_monster(char, out_map, pos, ent_list):
    __set_floor(char, out_map, pos, ent_list)

    ent_list.append(
        out_map.generate('gen_monster', [], pos, defer_placement=True))


def __set_item(char, out_map, pos, ent_list):
    __set_floor(char, out_map, pos, ent_list)

    ent_list.append(
        out_map.generate('gen_item', [], pos, defer_placement=True))


__CHAR_MAP = {
    '#': __set_wall,
    ' ': __set_floor,
    'M': __set_monster,
    'I': __set_item
}

#yapf:disable
blueprints = [
    [
        '#######',
        '#M   M#',
        '#  I  #',
        '# I#I #',
        '#  I  #',
        '#M   M#',
        '#######'
    ],
    [
        '###########',
        '# # M # M #',
        '# M # M # #',
        '###########'
    ]
]
#yapf:enable
