"""
Generate world with simple random rectangular rooms and corridors
"""

from world_map import WorldMap, PASSABLE_BIT, TRANSPARENT_BIT, DIGGABLE_BIT
from geom import Coord, Rect, intersect
import constants

MAX_ROOMS = 40
MAX_ROOM_SIZE = 10
MIN_ROOM_SIZE = 6

MOB_DISTRIBUTION = [0, 0, 1, 1, 2, 2, 2, 3]
ITEM_DISTRIBUTION = [0, 0, 0, 1, 1, 2]
SHROOM_DISTRIBUTION = [0, 1, 1, 1, 2, 2, 3, 4]


def generate(map_level, ctx):
    """
    Fill a world map with tiles and entities
    """
    world_map = WorldMap(map_level, constants.MAP_WIDTH, constants.MAP_HEIGHT,
                         ctx)
    entities = []

    player_start_pos = Coord(-1, -1)

    def is_passable(tiles, pos):
        if pos.x < 0 or pos.x > constants.MAP_WIDTH - 1:
            return False
        if pos.y < 0 or pos.y > constants.MAP_HEIGHT - 1:
            return False

        return tiles[pos.x, pos.y] & PASSABLE_BIT

    def is_occupied(pos):
        return pos == player_start_pos or any([
            ent
            for ent in entities if ent.get_component('world').position == pos
        ])

    def carve_room(tiles, rect):
        for x in range(rect.pos.x + 1, rect.pos.x + rect.dim.x - 1):
            for y in range(rect.pos.y + 1, rect.pos.y + rect.dim.y - 1):
                tiles[x, y] |= PASSABLE_BIT
                tiles[x, y] |= TRANSPARENT_BIT
                tiles[x, y] &= ~DIGGABLE_BIT

    def place_door(tiles, room):
        def _valid_door(pos):
            # door can only exist if at least two of its faces have walls
            impassable_faces = 0
            for face_pos in [
                    Coord(pos.x, pos.y + 1),
                    Coord(pos.x, pos.y - 1),
                    Coord(pos.x - 1, pos.y),
                    Coord(pos.x + 1, pos.y)
            ]:
                impassable_faces += int(not is_passable(tiles, face_pos))

            return impassable_faces > 1

        door_positions = [
            p
            for p in filter(lambda x: is_passable(tiles, x) and _valid_door(x),
                            room.edges())
        ]

        for i in range(ctx.rand.randint(0, len(door_positions))):
            ent = world_map.create(
                'gen_door', [], door_positions[i], defer_placement=True)
            entities.append(ent)

    def carve_h_tunnel(tiles, x1, x2, y):
        for x in range(min(x1, x2), max(x1, x2) + 1):
            tiles[x, y] |= PASSABLE_BIT
            tiles[x, y] |= TRANSPARENT_BIT
            tiles[x, y] &= ~DIGGABLE_BIT

    def carve_v_tunnel(tiles, y1, y2, x):
        for y in range(min(y1, y2), max(y1, y2) + 1):
            tiles[x, y] |= PASSABLE_BIT
            tiles[x, y] |= TRANSPARENT_BIT
            tiles[x, y] &= ~DIGGABLE_BIT

    def place_entities(room):
        n_monsters = ctx.rand.choice(MOB_DISTRIBUTION)
        n_items = ctx.rand.choice(ITEM_DISTRIBUTION)

        for _ in range(n_monsters):
            pos = Coord(
                ctx.rand.randint(room.pos.x + 1, room.pos.x + room.dim.x - 2),
                ctx.rand.randint(room.pos.y + 1, room.pos.y + room.dim.y - 2))

            if not is_occupied(pos):
                new_ent = world_map.create(
                    'gen_monster', [], pos, defer_placement=True)

                entities.append(new_ent)

        for i in range(n_items):
            pos = Coord(
                ctx.rand.randint(room.pos.x + 1, room.pos.x + room.dim.x - 2),
                ctx.rand.randint(room.pos.y + 1, room.pos.y + room.dim.y - 2))

            if not is_occupied(pos):
                ent = world_map.create(
                    'gen_item', [], pos, defer_placement=True)
                entities.append(ent)

    def place_mushrooms(room):
        n_shrooms = ctx.rand.choice(SHROOM_DISTRIBUTION)

        for _ in range(n_shrooms):
            pos = Rect(room.pos + Coord(1, 1),
                       room.dim - Coord(2, 2)).random_coords(ctx.rand, 1)[0]
            entities.append(
                world_map.create(
                    'gen_mushroom', [], pos, defer_placement=True))

    for y in range(constants.MAP_HEIGHT):
        for x in range(constants.MAP_WIDTH):
            diggable = ctx.rand.randint(1, 100) > 70
            if diggable:
                world_map.tile_map[x, y] |= DIGGABLE_BIT
                world_map.dig_map[x, y] = 100

    rooms = list()
    for _ in range(MAX_ROOMS):
        rw = ctx.rand.randint(MIN_ROOM_SIZE, MAX_ROOM_SIZE)
        rh = ctx.rand.randint(MIN_ROOM_SIZE, MAX_ROOM_SIZE)
        rx = ctx.rand.randint(0, world_map.width - rw - 1)
        ry = ctx.rand.randint(0, world_map.height - rh - 1)

        new_room = Rect(Coord(rx, ry), Coord(rw, rh))
        for other_room in rooms:
            if intersect(new_room, other_room):
                break
        else:
            carve_room(world_map.tile_map, new_room)
            new_center = new_room.center()

            if len(rooms) == 0:
                player_start_pos = new_center
                world_map.player_spawn_pos = player_start_pos

                if map_level > 0:
                    ent = world_map.create(
                        'stairs_up', [],
                        player_start_pos,
                        defer_placement=True,
                        overrides={
                            'entity': {
                                'name': 'stairs up to {}'.format(map_level - 1)
                            },
                            'stairs': {
                                'dest_level': map_level - 1
                            }
                        })
                    world_map.stairs_up_pos = player_start_pos
                    entities.append(ent)
            else:
                prev_center = rooms[-1].center()
                if ctx.rand.randint(0, 1) == 1:
                    carve_h_tunnel(world_map.tile_map, prev_center.x,
                                   new_center.x, prev_center.y)
                    carve_v_tunnel(world_map.tile_map, prev_center.y,
                                   new_center.y, new_center.x)
                else:
                    carve_v_tunnel(world_map.tile_map, prev_center.y,
                                   new_center.y, prev_center.x)
                    carve_h_tunnel(world_map.tile_map, prev_center.x,
                                   new_center.x, new_center.y)

            if map_level > 0 or len(rooms) > 0:
                place_entities(new_room)
                place_mushrooms(new_room)

            rooms.append(new_room)

    for room in rooms:
        place_door(world_map.tile_map, room)

    # create down stairs in the last room
    ent = world_map.create(
        'stairs_down', [],
        new_center,
        defer_placement=True,
        overrides={
            'stairs': {
                'dest_level': map_level + 1
            },
            'entity': {
                'name': 'stairs down to {}'.format(map_level + 1)
            }
        })
    world_map.stairs_down_pos = new_center
    entities.append(ent)

    world_map._fill_map(entities)

    return world_map
