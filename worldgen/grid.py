import numpy

from world_map import WorldMap, PASSABLE_BIT, TRANSPARENT_BIT, EXPLORED_BIT
from geom import Coord, Rect, random_polygon_from_rect
from randtools import choice


def __set_floor(the_map, pos):
    the_map.tile_map[pos.x, pos.
                     y] = PASSABLE_BIT | TRANSPARENT_BIT | EXPLORED_BIT


def __set_wall(the_map, pos):
    the_map.tile_map[pos.x, pos.y] = 0


def generate(map_level, ctx):
    grid_width = 6
    grid_height = 3
    cell_width = 11
    cell_height = 11
    padding = 3
    map_width = grid_width * cell_width
    map_height = grid_height * cell_height
    grid = numpy.zeros([grid_width, grid_height], numpy.uint8)

    world_map = WorldMap(map_level, map_width, map_height, ctx)

    room_dims = [1, 1, 1, 1, 2, 2, 3]

    def check_grid_space(rect):
        for gc in rect.coords():
            if gc.x < 0 or gc.x >= grid_width:
                return False
            if gc.y < 0 or gc.y >= grid_height:
                return False
            if grid[gc.x, gc.y]:
                return False

        return True

    rooms = []

    def make_room(grid_rect):
        map_rect = Rect(
            Coord(grid_rect.pos.x * cell_width, grid_rect.pos.y * cell_height),
            Coord(grid_rect.dim.x * cell_width, grid_rect.dim.y * cell_height))
        room_poly = random_polygon_from_rect(map_rect, padding, 6)
        for coord in room_poly.points:
            __set_floor(world_map, coord)

        for coord in map_rect.coords():
            if room_poly.contains(coord):
                __set_floor(world_map, coord)

        for gc in grid_rect.coords():
            grid[gc.x, gc.y] = 1

        rooms.append(room_poly)

        # TODO save the outermost points of each padding rect. After all rooms have
        # been created, we should make at least one connection to each room
        # by drawing lines between these outlying points

    for gy in range(grid_height):
        for gx in range(grid_width):
            grid_rect = Rect(
                Coord(gx, gy), Coord(choice(room_dims), choice(room_dims)))
            if check_grid_space(grid_rect):
                make_room(grid_rect)
            else:
                grid_rect = Rect(Coord(gx, gy), Coord(1, 1))
                if check_grid_space(grid_rect):
                    make_room(grid_rect)

    world_map.player_spawn_pos = choice(rooms).centroid()

    world_map._fill_map([])

    return world_map
