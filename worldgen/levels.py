"""
Level definitions
"""

import sys
import importlib


def get_generator_for_level(index):
    for min_level, max_level, generator_name in levels:
        if (index >= min_level) and (index <= max_level):
            module_name = 'worldgen.' + generator_name
            if module_name not in sys.modules:
                importlib.import_module(module_name, 'worldgen')
            return sys.modules[module_name].__dict__['generate']

    return None


#yapf: disable
levels = [
    [0, 4, 'random_caves'],
    [5, 10, 'random_rect']
]
#yapf: enable
