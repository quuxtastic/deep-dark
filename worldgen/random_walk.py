from world_map import WorldMap, PASSABLE_BIT, TRANSPARENT_BIT, EXPLORED_BIT
from geom import Coord, Rect
from constants import MAP_WIDTH, MAP_HEIGHT

MIN_OPEN_TILES = MAP_WIDTH * MAP_HEIGHT * 0.2


def __set_floor(the_map, pos):
    the_map.tile_map[pos.x, pos.
                     y] = PASSABLE_BIT | TRANSPARENT_BIT | EXPLORED_BIT


def __set_wall(the_map, pos):
    the_map.tile_map[pos.x, pos.y] = 0


def __is_floor(the_map, pos):
    return the_map.tile_map[pos.x, pos.y] & (PASSABLE_BIT | TRANSPARENT_BIT)


def generate(map_level, ctx):
    world_map = WorldMap(map_level, MAP_WIDTH, MAP_HEIGHT, ctx)
    map_rect = Rect(Coord(1, 1), Coord(MAP_WIDTH - 2, MAP_HEIGHT - 2))

    start_pos = Coord(randint(1, MAP_WIDTH - 2), randint(1, MAP_HEIGHT - 2))
    __set_floor(world_map, start_pos)

    world_map.player_spawn_pos = start_pos

    moves = [
        Coord(1, 0),
        Coord(0, 1),
        Coord(1, 1),
        Coord(-1, 0),
        Coord(0, -1),
        Coord(-1, -1),
        Coord(-1, 1),
        Coord(1, -1)
    ]

    entities = []
    templates = ['gen_item', 'gen_monster']

    floor_tiles = 1
    cur_pos = start_pos
    while floor_tiles < MIN_OPEN_TILES:
        cur_pos = choice([
            c for c in filter(map_rect.contains, [m + cur_pos for m in moves])
        ])

        if not __is_floor(world_map, cur_pos):
            __set_floor(world_map, cur_pos)
            floor_tiles += 1

            if randint(1, 100) > 80:
                ent = world_map.generate(
                    choice(templates), [], cur_pos, defer_placement=True)
                entities.append(ent)

    world_map._fill_map(entities)

    return world_map
