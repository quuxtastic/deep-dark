"""
Generate an empty world for testing
"""

from geom import Rect, Coord
from world_map import WorldMap, PASSABLE_BIT, TRANSPARENT_BIT


def generate(map_level, ctx):
    width = 10
    height = 10
    walls = Rect(Coord(0, 0), Coord(width, height))

    world_map = WorldMap(map_level, width, height, ctx)
    world_map.player_spawn_pos = walls.center()

    for wall_coord in walls.interior_coords():
        world_map.tile_map[wall_coord.x, wall_coord.y] |= PASSABLE_BIT
        world_map.tile_map[wall_coord.x, wall_coord.y] |= TRANSPARENT_BIT

    world_map._fill_map([])

    return world_map
