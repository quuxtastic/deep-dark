"""
Help text
"""

import collections

import tcod

from input import Commands, CommandContexts, key_bindings

command_help = {
    Commands.MOVE_N: 'Move North',
    Commands.MOVE_S: 'Move South',
    Commands.MOVE_E: 'Move East',
    Commands.MOVE_W: 'Move West',
    Commands.MOVE_NW: 'Move Northwest',
    Commands.MOVE_NE: 'Move Northeast',
    Commands.MOVE_SW: 'Move Southwest',
    Commands.MOVE_SE: 'Move Southeast',
    Commands.WAIT: 'Wait',
    Commands.PICK_UP: 'Pick up an item',
    Commands.INTERACT: 'Interact with something',
    Commands.INVENTORY_OPEN: 'View player inventory',
    Commands.INVENTORY_USE: 'Use item',
    Commands.INVENTORY_DROP: 'Drop item',
    Commands.INVENTORY_EQUIP_TOGGLE: 'Equip/unequip item',
    Commands.EXIT: 'Exit current screen',
    Commands.CONFIRM: 'Confirm choice',
    Commands.PROMPT_CONFIRM: 'Confirm choice',
    Commands.PROMPT_DENY: 'Cancel',
    Commands.MENU_NEXT: 'Select next option',
    Commands.MENU_PREV: 'Select previous option',
    Commands.HELP_TOGGLE: 'Show/Hide help',
    Commands.DELETE_SAVE: 'Delete selected save',
    Commands.CHARINFO_OPEN: 'Show character stats',
    Commands.ENABLE_TEXT_COMMAND: 'Open console',
    Commands.NEXT_TARGET: 'Select next target',
    Commands.MESSAGES_SCROLL_UP: 'Next message',
    Commands.MESSAGES_SCROLL_DOWN: 'Prev message',
    Commands.LANTERN_TOGGLE: 'Toggle lantern',
    Commands.LEVEL_UP: 'Level up',
    Commands.INSPECT: 'Inspect',
    'Movement+LALT': 'Interact',
    'Movement+LCTRL': 'Attack'
}

key_help = {
    tcod.KEY_UP: chr(tcod.CHAR_ARROW_N),
    tcod.KEY_DOWN: chr(tcod.CHAR_ARROW_S),
    tcod.KEY_LEFT: chr(tcod.CHAR_ARROW_W),
    tcod.KEY_RIGHT: chr(tcod.CHAR_ARROW_E),
    tcod.KEY_KP0: 'NP0',
    tcod.KEY_KP1: 'NP1',
    tcod.KEY_KP2: 'NP2',
    tcod.KEY_KP3: 'NP3',
    tcod.KEY_KP4: 'NP4',
    tcod.KEY_KP5: 'NP5',
    tcod.KEY_KP6: 'NP6',
    tcod.KEY_KP7: 'NP7',
    tcod.KEY_KP8: 'NP8',
    tcod.KEY_KP9: 'NP9',
    tcod.KEY_F1: 'F1',
    tcod.KEY_ESCAPE: 'ESC',
    tcod.KEY_ENTER: 'Ent',
    tcod.KEY_TAB: 'Tab',
    tcod.KEY_SPACE: 'Space'
}


def generate_help_text(command_ctx, extras=None):
    cmd_keys = collections.defaultdict(list)

    for k, v in key_bindings[command_ctx].items():
        cmd_keys[v].append(key_help.get(k, k))

    out_lines = []

    for k, v in cmd_keys.items():
        out_lines.append('{} - {}'.format(','.join(v).ljust(7),
                                          command_help[k]))

    if extras:
        for extra in extras:
            out_lines.append('{} - {}'.format(
                extra.ljust(7), command_help[extra]))

    return '\n'.join(out_lines) + '\n'


inventory_menu = generate_help_text(CommandContexts.MENU_INVENTORY)
player_turn = generate_help_text(CommandContexts.PLAYER,
                                 ['Movement+LALT', 'Movement+LCTRL'])
targeting = generate_help_text(CommandContexts.TARGETING)
char_stats = generate_help_text(CommandContexts.MENU_CHAR_STATS)
