"""
Lore screens
"""


def __find_lore(level_index):
    for level_range, lore in lore_text.items():
        rng = range(level_range[0], level_range[1] + 1)
        if level_index in rng:
            return (rng, lore[0], lore[1])

    return None


def get_lore_for_level(level_index):
    return __find_lore(level_index)


def lore_available(level_index):
    return __find_lore(level_index) is not None


def get_lore_intro():
    return intro_text


#yapf: disable
intro_text = 'In an age now lost to time, your people lived in a great realm ' \
             'deep underground: the legendary city of SARKOMON. But the ' \
             'wondrous city fell, and since living memory your people have ' \
             'had to scratch out a living beneath the sun.' \
             '\n\n' \
             'The time has come to brave the depths and recover the ancient ' \
             'glory of your ancestors!'
lore_text = {
    (0, 4): ('UPPER TUNNELS (0-4)',
             'Once, these twisting passages were kept clear and allowed trade '
             'between the surface and the realms below. Now, all manner of '
             'vermin dwell here.'
             '\n\n'
             'You may find a few meagre scraps of treasure in these tunnels, '
             'but you must delve deeper to find the real prizes.')
}
#yapf: enable
