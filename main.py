#!/usr/bin/env python3
"""
demo using tcod
"""

import time

import tcod

from message import MessageLog
from render.renderer import Renderer
from factory import EntityFactory
from state.stateman import StateMachine, GameStates
from state import init, menu, monster, player

from randtools import Generator


class GameSettings:
    def __init__(self):
        self.auto_eat = True
        self.auto_pickup = True
        self.auto_fuel = True
        self.mouselook = True


class StateContext:
    def __init__(self, message_log, renderer, keyin, mousein):
        self.message_log = message_log
        self.renderer = renderer
        self.keyin = keyin
        self.mousein = mousein

        self.rand = Generator()
        self.world_seed = 0

        self.entity_factory = EntityFactory(self.rand)

        self.world = None
        self.player = None

        self.map_seeds = dict()
        self.maps = dict()
        self.map_index = -1

        self.menu = None

        self.player_dead = False

        self.settings = GameSettings()

        self.lore_levels_shown = set()


def main():
    """
    Main loop
    """

    message_log = MessageLog()

    renderer = Renderer()

    keyin = tcod.Key()
    mousein = tcod.Mouse()

    tcod.sys_set_fps(60)

    state_ctx = StateContext(message_log, renderer, keyin, mousein)

    state_mgr = StateMachine(GameStates.MAIN_MENU, state_ctx)
    init.register_states(state_mgr)
    menu.register_states(state_mgr)
    monster.register_states(state_mgr)
    player.register_states(state_mgr)

    while not tcod.console_is_window_closed():
        cur_time = time.time()
        renderer.do_render(cur_time)

        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS | tcod.EVENT_MOUSE,
                                 keyin, mousein)

        next_state, _ = state_mgr.run()
        if next_state == GameStates.EXIT:
            break


if __name__ == '__main__':
    main()
