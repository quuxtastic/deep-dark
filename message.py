"""
Game user messages
"""

from enum import Enum, auto


class Priority(Enum):
    LOW = auto()
    NORMAL = auto()
    HIGH = auto()
    DEBUG = auto()


class Message:
    def __init__(self, text, priority=Priority.NORMAL):
        self.text = text
        self.priority = priority


class MessageLog:
    def __init__(self):
        self.messages = []
        self.__listeners = []

    def add(self, msg, priority=Priority.NORMAL):
        self.messages.append(msg)

        for listener in self.__listeners:
            listener.add(msg, priority)

    def clear(self):
        self.messages.clear()
        for listener in self.__listeners:
            listener.clear()

    def attach(self, listener):
        self.__listeners.append(listener)

    def detach(self, listener):
        self.__listeners.remove(listener)
