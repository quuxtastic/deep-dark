import tcod

#yapf: disable
templates = {
    'meta_monster': {
        'include': ['meta_actor'],
        'components': [
            ['monsterSteering'],
            ['monsterThink']
        ]
    },

    'orc': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'orc',
                'clsname': 'orc',
                'description': 'A smelly, dull-witted pig-faced biped'
            }],
            ['hp', {'max_hp': 5}],
            ['combat', {
                'attack': 1,
                'dmg_die': 6,
                'defence': 10
            }],
            ['renderable', {
                'glyph': 'o',
                'color': tcod.desaturated_green
            }],
            ['death', {
                'nutrition': 30
            }]
        ]
    },

    'troll': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'troll',
                'clsname': 'troll',
                'description':
                    'A lumbering, mossy brute with a terrible stench'
            }],
            ['hp', {'max_hp': 12}],
            ['combat', {
                'attack': 2,
                'dmg_die': 6,
                'defence': 8,
                'damage_bonus': 2
            }],
            ['renderable', {
                'glyph': 'T',
                'color': tcod.darker_green
            }],
            ['death', {
                'nutrition': 50
            }]
        ]
    }
}

genlists = {
    'gen_monster': [
        [5, [
            [20, 'troll'],
            [40, 'orc'],
            [20, 'kobold'],
            [20, 'slime']
        ]],

        [6, [
            [35, 'troll'],
            [40, 'orc'],
            [25, 'slime']
        ]]
    ]
}
#yapf:enable
