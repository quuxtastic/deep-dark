"""
Templates that define moving actors
"""

import tcod

from render.map_view import RenderOrder
from components.renderStatusEffect import RenderEffects

#yapf: disable
templates = {
    'meta_actor': {
        'components': [
            ['death'],
            ['renderable', {
                'render_order': RenderOrder.ACTOR,
                'frames': {
                    0: [{}],
                    RenderEffects.CONFUSED: [
                        {},
                        {'glyph': '?'}
                    ],
                    RenderEffects.INVISIBLE: [
                        {},
                        {'color': tcod.dark_grey}
                    ]
                }
            }],
            ['renderStatusEffect'],
            ['spellReceiver'],
            ['hp'],
            ['combat'],
            ['world', {
                'block_mov': True,
                'block_los': False,
            }],
            ['vision', {
                'view_distance': 10,
                'fov_algorithm': 0
            }]
        ]
    },

    'meta_spawner': {
        'components': [
            ['renderable', {
                'render_order': RenderOrder.ACTOR,
                'frames': {0: [{}]}
            }],
            ['hp'],
            ['world', {
                'block_mov': True,
                'block_los': False
            }],
            ['spawner']
        ]
    },

    'player': {
        'include': ['meta_actor'],
        'components': [
            ['entity', {
                'name': '<unnamed>',
                'clsname': 'player',
                'description': 'A foolhardy adventurer'
            }],
            ['hp', {
                'max_hp': 20
            }],
            ['combat', {
                'attack': 0,
                'dmg_die': 6,
                'defence': 10
            }],
            ['renderable', {
                'glyph': '@',
                'color': tcod.white
            }],
            ['inventory', {'base_capacity': 80}],
            ['equipSlots'],
            ['playerThink'],
            ['hunger', {'max_hunger': 200}],
            ['fuelTank', {'capacity': 200}],
            ['vision', {
                'view_distance': 3,
                'fov_algorithm': tcod.FOV_RESTRICTIVE
            }],
            ['identifier'],
            ['level'],
            ['stats']
        ]
    }
}
#yapf: enable
