"""
Entities that define world objects
"""

import tcod

from render.map_view import RenderOrder
from components.door import DoorRenderStates

#yapf: disable
templates = {
    'meta_stairs': {
        'components': [
            ['entity', {'clsname': 'stairs'}],
            ['world', {
                'block_mov': False,
                'block_los': False
            }],
            ['renderable', {
                'render_order': RenderOrder.FLOOR,
                'color': tcod.dark_grey
            }],
            ['interactable'],
            ['stairs']
        ]
    },
    'stairs_down': {
        'include': ['meta_stairs'],
        'components': [
            ['entity', {
                'name': 'stairs down',
                'description': 'Stairs spiral down into the darkness'
            }],
            ['renderable', {
                'glyph': chr(tcod.CHAR_ARROW2_S)
            }]
        ]
    },
    'stairs_up': {
        'include': ['meta_stairs'],
        'components': [
            ['entity', {
                'name': 'stairs up',
                'description': 'Stairs spiral up into the darkness'
            }],
            ['renderable', {
                'glyph': chr(tcod.CHAR_ARROW2_N)
            }]
        ]
    },

    'meta_door': {
        'components': [
            ['entity', {
                'name': 'door',
                'clsname': 'door',
                'description': 'A sturdy wooden door'
            }],
            ['interactable'],
            ['door'],
            ['hp', {'max_hp': 10}],
            ['combat', {
                'defence': 10,
                'attack': 0,
                'dmg_die': 1
            }],
            ['renderable', {
                'render_order': RenderOrder.FLOOR,
                'color': tcod.lighter_yellow,

                'frames': {
                    DoorRenderStates.OPEN: [
                        {'glyph': chr(tcod.CHAR_BLOCK1)}
                    ],
                    DoorRenderStates.CLOSED: [
                        {'glyph': chr(tcod.CHAR_BLOCK3)}
                    ]
                }
            }],
            ['world']
        ]
    },

    'door_closed': {
        'include': ['meta_door'],
        'components': [
            ['door', {'closed': True}],
            ['renderable', {'start_frame': DoorRenderStates.CLOSED}],
            ['world', {'block_mov': True, 'block_los': True}]
        ]
    },
    'door_open': {
        'include': ['meta_door'],
        'components': [
            ['door', {'closed': False}],
            ['world', {'block_los': False, 'block_mov': False}],
            ['renderable', {'start_frame': DoorRenderStates.OPEN}],
            ['world', {'block_mov': False, 'block_los': False}]
        ]
    },
    'door_locked': {
        'include': ['meta_door'],
        'components': [
            ['door', {'closed': True, 'locked': True}],
            ['renderable', {'start_frame': DoorRenderStates.CLOSED}],
            ['world', {'block_mov': True, 'block_los': True}]
        ]
    }
}

genlists = {
    'gen_door': [
        [0, [
            [20, 'door_locked'],
            [40, 'door_open'],
            [40, 'door_closed']
        ]]
    ]
}
#yapf: enable
