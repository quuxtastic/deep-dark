import tcod

#yapf: disable
templates = {
    'meta_relic': {
        'include': ['meta_item'],
        'components': [
            ['entity', {
                'name': 'relic',
                'clsname': 'relic',
                'description': 'A relic of lost Sarkomond'
            }],
            ['renderable', {
                'glyph': '$'
            }],
            ['item', {'weight': -1}],
            ['world', {
                'block_mov': False,
                'block_los': False
            }],
            ['xp']
        ]
    },

    'minor_relic': {
        'include': ['meta_relic'],
        'components': [
            ['renderable', {'color': tcod.silver}],
            ['xp', {'amount': 2}]
        ]
    },
    'major_relic': {
        'include': ['meta_relic'],
        'components': [
            ['renderable', {'color': tcod.gold}],
            ['xp', {'amount': 5}]
        ]
    }
}

genlists = {
    'gen_relic': [
        [0, [
            [75, 'minor_relic'],
            [25, 'major_relic']
        ]]
    ]
}
#yapf: enable
