"""
Templates for items (anything that can be picked up or stored)
"""

import tcod

from render.map_view import RenderOrder

#yapf: disable
templates = {
    'meta_item': {
        'components': [
            ['renderable', {
                'render_order': RenderOrder.ITEM,
                'speed': float(0.2),
                'frames': {
                    0: [{}, {'show': False}]
                }
            }],
            ['item'],
            ['world', {
                'block_mov': False,
                'block_los': False
            }]
        ]
    },

    'meta_usable_item': {
        'include': ['meta_item'],
        'components': [['usable']]
    },

    'meta_item_singleuse': {
        'include': ['meta_usable_item'],
        'components': [
            ['consumable', {'uses': 1}]
        ]
    },

    'healing_potion': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'name': 'healing potion',
                'clsname': 'potion',
                'description': 'A small vial of glowing purple fluid (heal 8)'
            }],
            ['renderable', {
                'glyph': '!',
                'color': tcod.violet
            }],
            ['healing', {'heal_amount': 8}]
        ]
    },

    'dungeon_map': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'name': 'magic map',
                'clsname': 'map',
                'description': 'A hastily scrawled map on worn parchment (explores map)'
            }],
            ['renderable', {
                'glyph': 'i',
                'color': tcod.brass
            }],
            ['mapExploreSpell']
        ]
    },

    'food': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'name': 'food',
                'clsname': 'food',
                'description': 'Hardtack and tough jerky (75 food, heal 2)'
            }],
            ['renderable', {
                'glyph': '+',
                'color': tcod.dark_red
            }],
            ['food', {'nutrition': 75}],
            ['healing', {'heal_amount': 2}],
            ['item', {'weight': 3}]
        ]
    },

    'corpse': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'name': '<unnamed>',
                'clsname': 'corpse',
                'description': 'A corpse'
            }],
            ['renderable', {
                'glyph': '%',
                'color': tcod.darker_red,
                'render_order': RenderOrder.CORPSE
            }],
            ['food', {'nutrition': 0}],
            ['item', {'weight': 5}]
        ]
    },

    'oil_flask': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'name': 'oil flask',
                'clsname': 'light_fuel',
                'description': 'Leather flask of oil'
            }],
            ['renderable', {'glyph': 'o', 'color': tcod.black}],
            ['fuel', {'amount': 25}]
        ]
    }
}

genlists = {
    'gen_item': [
        [0, [
            [37, 'gen_rune'],
            [41, 'gen_equipment'],
            [2, 'dungeon_map'],
            [20, 'gen_relic']
        ]]
    ]
}
#yapf: enable
