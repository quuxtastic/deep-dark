import tcod

from components.usable import TargetTypes
from components.damage import DamageTypes

#yapf: disable
templates = {
    'meta_rune': {
        'include': ['meta_item_singleuse'],
        'components': [
            ['entity', {
                'clsname': 'rune',
                'description': 'A stone etched with strange runes'
            }],
            ['renderable', {
                'glyph': '#'
            }]
        ]
    },

    'lightning_rune': {
        'include': ['meta_rune'],
        'components': [
            ['entity', {'name': 'lightning rune'}],
            ['renderable', {
                'color': tcod.light_turquoise
            }],
            ['damage', {'damage': 10, 'tags': [DamageTypes.SHOCK]}],
            ['usable', {
                'target_type': TargetTypes.LIST_IN_FOV,
                'target_selector': {
                    'components': {
                        'combat': None
                    }}
            }]
        ]
    },
    'fireball_rune': {
        'include': ['meta_rune'],
        'components': [
            ['entity', {'name': 'fireball rune'}],
            ['renderable', {
                'color': tcod.flame
            }],
            ['damage', {'damage': 10, 'tags': [DamageTypes.FIRE]}],
            ['usable', {
                'target_type': TargetTypes.SQUARE,
                'target_radius': 1,
                'target_selector': {
                    'components': {
                        'combat': None
                    }
                }}]
        ]
    },
    'confusion_rune': {
        'include': ['meta_rune'],
        'components': [
            ['entity', {'name': 'confusion rune'}],
            ['renderable', {
                'color': tcod.lime
            }],
            ['usable', {
                'target_type': TargetTypes.SINGLE,
                'target_selector': {
                    'components': {
                        'spellReceiver': None
                    }}
            }],
            ['spellCaster', {
                'effect': 'confusionEffect',
                'effect_args': {'turns': 5}
            }]
        ]
    },
    'earthwall_rune': {
        'include': ['meta_rune'],
        'components': [
            ['entity', {'name': 'earthwall rune'}],
            ['renderable', {
                'color': tcod.brass
            }],
            ['summonEarthSpell'],
            ['usable', {'target_type': TargetTypes.SINGLE_EMPTY_TILE}]
        ]
    },
    'invisibility_rune': {
        'include': ['meta_rune'],
        'components': [
            ['entity', {'name': 'invisibilty rune'}],
            ['renderable', {
                'color': tcod.magenta
            }],
            ['spellCaster', {
                'effect': 'invisibleEffect',
                'effect_args': {'turns': 5}
            }],
            ['usable']
        ]
    }
}

genlists = {
    'gen_rune': [
        [0, [
            [25, 'fireball_rune'],
            [25, 'lightning_rune'],
            [25, 'confusion_rune'],
            [25, 'earthwall_rune']
        ]]
    ]
}
#yapf:enable
