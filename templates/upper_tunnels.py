import tcod

from render.map_view import RenderOrder
from components.damage import DamageTypes

#yapf: disable
templates = {
    'kobold': {
        'include': ['meta_actor'],
        'components': [
            ['entity', {
                'name': 'kobold',
                'clsname': 'kobold',
                'description': 'A tiny, mischevious doglike creature'
            }],
            ['hp', {'max_hp': 2}],
            ['combat', {
                'attack': 1,
                'dmg_die': 4,
                'defence': 14
            }],
            ['renderable', {
                'glyph': 'k',
                'color': tcod.dark_crimson
            }],
            ['death', {
                'nutrition': 15
            }],
            ['stealth'],
            ['monsterSteering'],
            ['koboldThink'],
            ['inventory', {'base_capacity': 5}]
        ]
    },

    'giant_rat': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'giant rat',
                'clsname': 'vermin',
                'description': 'Filthy, disease-ridden rodent'
            }],
            ['hp', {'max_hp': 2}],
            ['combat', {
                'attack': 1,
                'dmg_die': 4,
                'defence': 12
            }],
            ['renderable', {
                'glyph': 'r',
                'color': tcod.copper
            }],
            ['death', {
                'nutrition': 10
            }]
        ]
    },

    'giant_maggot': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'giant maggot',
                'clsname': 'vermin',
                'description': 'Freakish, corpulent grub'
            }],
            ['hp', {'max_hp': 1}],
            ['combat', {
                'attack': 0,
                'dmg_die': 3,
                'defence': 8
            }],
            ['renderable', {
                'glyph': 'm',
                'color': tcod.lighter_yellow
            }],
            ['death', {'spawn_corpse': False}]
        ]
    },
    'maggot_sac': {
        'include': ['meta_spawner'],
        'components': [
            ['entity', {
                'name': 'maggot sac',
                'clsname': 'vermin',
                'description': 'Slimy sac that births freakish maggots'
            }],
            ['renderable', {
                'glyph': 'S',
                'color': tcod.lighter_yellow
            }],
            ['combat', {
                'defence': 9,
                'attack': 0,
                'dmg_die': 1
            }],
            ['hp', {
                'max_hp': 20,
                'resist': {
                    DamageTypes.FIRE: -50
                }
            }],
            ['spawner', {
                'spawn_template': 'giant_maggot',
                'radius': 2,
                'spawn_chance': 15,
                'max_spawns': 8
            }],
            ['death', {'spawn_corpse': False}]
        ]
    },

    'slime': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'slime',
                'clsname': 'slime',
                'description': 'A blob of acidic goo'
            }],
            ['hp', {
                'max_hp': 20,
                'resist': {
                    DamageTypes.ACID: 100
                }
            }],
            ['combat', {
                'attack': 1,
                'dmg_die': 4,
                'defence': 8
            }],
            ['renderable', {
                'glyph': 'O',
                'color': tcod.lime
            }],
            ['death', {'spawn_corpse': False}],
            ['slimeTrail', {'spawn_template': 'slime_puddle'}]
        ]
    },
    'slime_puddle': {
        'components': [
            ['entity', {
                'name': 'slime trail',
                'description': 'trail of acidic slime'
            }],
            ['renderable', {
                'render_order': RenderOrder.FLOOR,
                'glyph': chr(tcod.CHAR_BLOCK1),
                'color': tcod.lime
            }],
            ['world', {'block_mov': False, 'block_los': False}],
            ['areaEffect', {'selector': {'components': {'hp': None}}}],
            ['damage', {'damage': 5, 'tags': [DamageTypes.ACID]}],
            ['decompose', {'duration': 5}]
        ]
    },

    'looter': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'looter',
                'clsname': 'looter',
                'description': 'A ruthless tomb-robber'
            }],
            ['hp', {'max_hp': 5}],
            ['combat', {
                'attack': 1,
                'dmg_die': 6,
                'defence': 12
            }],
            ['renderable', {
                'glyph': 'l',
                'color': tcod.lightest_gray
            }],
            ['death', {
                'nutrition': 15
            }]
        ]
    },

    'cave_troll': {
        'include': ['meta_monster'],
        'components': [
            ['entity', {
                'name': 'cave troll',
                'clsname': 'troll',
                'description':
                    'A lumbering, mossy brute with a terrible stench'
            }],
            ['hp', {
                'max_hp': 12,
                'resist': {
                    DamageTypes.FIRE: -50
                }
            }],
            ['combat', {
                'attack': 2,
                'dmg_die': 6,
                'defence': 8,
                'damage_bonus': 2
            }],
            ['renderable', {
                'glyph': 'T',
                'color': tcod.darker_green
            }],
            ['death', {
                'nutrition': 50
            }]
        ]
    }
}

genlists = {
    'gen_monster': [
        [1, [
            [30, 'giant_rat'],
            [30, 'kobold'],
            [35, 'giant_maggot'],
            [5, 'maggot_sac']
        ]],
        [3, [
            [25, 'giant_rat'],
            [25, 'kobold'],
            [40, 'giant_maggot'],
            [10, 'maggot_sac']
        ]],
        [4, [
            [33, 'giant_rat'],
            [32, 'kobold'],
            [20, 'looter'],
            [10, 'slime'],
            [5, 'cave_troll']
        ]]
    ]
}
#yapf: enable
