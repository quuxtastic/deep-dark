import tcod

from render.map_view import RenderOrder
from components.damage import DamageTypes

#yapf: disable
templates = {
    'meta_mushroom': {
        'components': [
            ['entity', {
                'genstr:name': '{type} mushroom',
                'genstr:description': '{type} mushroom',
                'clsname': 'mushroom'
            }],
            ['renderable', {
                'render_order': RenderOrder.FLOOR,
                'glyph': ',',
                'gen:color': 'color'
            }],
            ['identifiable'],
            ['item'],
            ['usable'],
            ['world', {
                'block_mov': False,
                'block_los': False
            }],
            ['consumable', {'uses': 1}]
        ]
    },

    'healing_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['healing', {'heal_amount': 10}],
            ['identifiable', {
                'real_name': 'healing mushroom',
                'real_description': 'Healing mushroom (heal 10)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'poison_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['damage', {'damage': 10, 'tags': [DamageTypes.POISON]}],
            ['identifiable', {
                'real_name': 'poison mushroom',
                'real_description': 'Poisonous mushroom (10 poison damage)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'confusing_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['spellCaster', {
                'effect': 'confusionEffect',
                'effect_args': {'turns': 5}
            }],
            ['identifiable', {
                'real_name': 'confusing mushroom',
                'real_description': 'Confusing mushroom (confusion for 5 turns)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'invisibility_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['spellCaster', {
                'effect': 'invisibleEffect',
                'effect_args': {'turns': 8}
            }],
            ['identifiable', {
                'real_name': 'invisibility mushroom',
                'real_description':
                    'Invisibility mushroom (invisible for 8 turns)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'tasty_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['food', {'nutrition': 75}],
            ['healing', {'heal_amount': 2}],
            ['identifiable', {
                'real_name': 'tasty mushroom',
                'real_description': 'Tasty mushroom (75 food, heal 2)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'esp_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['spellCaster', {
                'effect': 'mapRevealEffect',
                'effect_args': {'turns': 3}
            }],
            ['identifiable', {
                'real_name': 'ESP mushroom',
                'real_description': 'ESP mushroom (reveal map for 3 turns)'
            }]
        ],
        'randomizer': 'mushroom'
    },
    'oily_mushroom': {
        'include': ['meta_mushroom'],
        'components': [
            ['fuel', {'amount': 25}],
            ['identifiable', {
                'real_name': 'oily mushroom',
                'real_description': 'Oily mushroom (25 lumes)'
            }]
        ],
        'randomizer': 'mushroom'
    }
}
genlists = {
    'gen_mushroom': [
        [0, [
            [30, 'tasty_mushroom'],
            [20, 'oily_mushroom'],
            [15, 'healing_mushroom'],
            [15, 'confusing_mushroom'],
            [15, 'poison_mushroom'],
            [3, 'invisibility_mushroom'],
            [2, 'esp_mushroom']
        ]]
    ]
}
randomizers = {
    'mushroom': [
        {
            'entity': {'type': 'blue'},
            'renderable': {'color': tcod.blue}
        },
        {
            'entity': {'type': 'green'},
            'renderable': {'color': tcod.green}
        },
        {
            'entity': {'type': 'purple'},
            'renderable': {'color': tcod.purple}
        },
        {
            'entity': {'type': 'red'},
            'renderable': {'color': tcod.red}
        },
        {
            'entity': {'type': 'white'},
            'renderable': {'color': tcod.white}
        },
        {
            'entity': {'type': 'yellow'},
            'renderable': {'color': tcod.yellow}
        },
        {
            'entity': {'type': 'orange'},
            'renderable': {'color': tcod.orange}
        },
        {
            'entity': {'type': 'cyan'},
            'renderable': {'color': tcod.cyan}
        }
    ]
}
#yapf: enable
