import tcod

from components.usable import TargetTypes
from components.equipSlots import EquipSlots
from components.damage import DamageTypes

#yapf: disable
templates = {
    'meta_equipment': {
        'include': ['meta_item'],
        'components': [
            ['renderable', {
                'color': tcod.gray
            }],
            ['equipment']
        ]
    },

    'shield': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'shield',
                'clsname': 'armor',
                'description': 'A battered shield'
            }],
            ['renderable', {'glyph': '('}],
            ['equipment', {'slots': [[EquipSlots.OFF_HAND]]}],
            ['armor', {'defence_mod': 1}],
            ['item', {'weight': 5}]
        ]
    },
    'chainmail': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'chainmail',
                'clsname': 'armor',
                'description': 'Rusty chainmail armor'
            }],
            ['renderable', {'glyph': 'A'}],
            ['equipment', {'slots': [[EquipSlots.BODY]]}],
            ['armor', {'defence_mod': 2}],
            ['item', {'weight': 10}]
        ]
    },
    'helm': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'helm',
                'clsname': 'armor',
                'description': 'Dented helmet'
            }],
            ['renderable', {'glyph': '^'}],
            ['equipment', {'slots': [[EquipSlots.HEAD]]}],
            ['armor', {'defence_mod': 1}],
            ['item', {'weight': 5}]
        ]
    },

    'sword': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'sword',
                'clsname': 'weapon',
                'description': 'Notched but servicable sword'
            }],
            ['renderable', {'glyph': '/'}],
            ['equipment', {'slots': [[EquipSlots.MAIN_HAND]]}],
            ['weapon', {
                'attack_mod': 2,
                'damage_mod': 0,
                'crit_mod': 0
            }],
            ['item', {'weight': 5}]
        ]
    },
    'dagger': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'dagger',
                'clsname': 'weapon',
                'description': 'Razor-sharp dagger'
            }],
            ['renderable', {'glyph': '-'}],
            ['equipment', {'slots': [[EquipSlots.MAIN_HAND]]}],
            ['weapon', {
                'attack_mod': 1,
                'damage_mod': -1,
                'crit_mod': 2
            }],
            ['item', {'weight': 2}]
        ]
    },
    'hammer': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'hammer',
                'clsname': 'weapon',
                'description': 'Unwieldy hammer'
            }],
            ['renderable', {'glyph': '\\'}],
            ['equipment', {'slots': [[EquipSlots.MAIN_HAND]]}],
            ['weapon', {
                'attack_mod': 0,
                'damage_mod': 3,
                'crit_mod': 0
            }],
            ['item', {'weight': 8}]
        ]
    },

    'flamethrower': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'flamethrower',
                'clsname': 'weapon',
                'description': 'Kerosene-scented mess of tubes and gears (10 fire damage)'
            }],
            ['renderable', {'glyph': '&'}],
            ['equipment', {
                'slots': [[EquipSlots.MAIN_HAND, EquipSlots.OFF_HAND]]
            }],
            ['usable', {
                'target_type': TargetTypes.LINE,
                'target_cone_distance': 2,
                'requires_equip': True,
                'target_selector': {'components': {'hp': None}}
            }],
            ['fuelConsumer', {'use_cost': 5}],
            ['damage', {'damage': 10, 'tags': [DamageTypes.FIRE]}],
            ['item', {'weight': 8}]
        ]
    },

    'lantern': {
        'include': ['meta_equipment'],
        'components': [
            ['entity', {
                'name': 'lantern',
                'clsname': 'lantern',
                'description': 'Flickering lantern (radius 10)'
            }],
            ['renderable', {'glyph': '*'}],
            ['usable', {
                'requires_equip': True,
                'toggle': True
            }],
            ['fuelConsumer', {'periodic_cost': 1}],
            ['lightSource', {
                'view_distance': 10,
                'fov_algorithm': 0
            }],
            ['equipment', {'slots': [[EquipSlots.LIGHT_SOURCE]]}],
            ['item', {'weight': 2}]
        ]
    }
}

genlists = {
    'gen_equipment': [
        [0, [
            [45, 'gen_armor'],
            [55, 'gen_weapon']
        ]]
    ],

    'gen_armor': [
        [0, [
            [33, 'helm'],
            [33, 'chainmail'],
            [34, 'shield']
        ]]
    ],

    'gen_weapon': [
        [0, [
            [40, 'sword'],
            [40, 'hammer'],
            [20, 'flamethrower']
        ]]
    ]
}
#yapf: enable
