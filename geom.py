"""
Geometry primitives

The coordinate system uses the top-left corner of the screen as the origin.
X increases to the right and Y increases downward
"""

import math


class Coord:
    """
    A 2D Coordinate
    """

    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def contain(self, rect):
        """
        Adjust this coordinate to be within a given bounding rectangle.
        :param rect: Bounding rectangle
        :returns: True if the coordinate was outside the bounding rectangle
        """
        collide = False

        if self.x < rect.pos.x:
            self.x = rect.pos.x
            collide = True
        elif self.x >= rect.dim.x:
            self.x = rect.dim.x - 1
            collide = True

        if self.y < rect.pos.y:
            self.y = rect.pos.y
            collide = True
        elif self.y >= rect.dim.y:
            self.y = rect.dim.y - 1
            collide = True

        return collide

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        if other is None:
            return False

        return self.x == other.x and self.y == other.y

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Coord(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        return Coord(self.x * other, self.y * other)

    def __div__(self, other):
        return Coord(self.x / other, self.y / other)


def distance(coord_a, coord_b):
    """
    :returns: the distance between coord a and b
    """
    return math.sqrt((coord_a.x - coord_b.x)**2 + (coord_a.y - coord_b.y)**2)


def sort_by_x(coord_list, reverse=False):
    """
    Sorts coordinate list by x component
    :param coord_list: Coords to sort
    :param reverse: If True, sorts high-to-low, else low-to-high
    :returns: List of sorted Coords
    """
    return sorted(coord_list, key=lambda cur: cur.x, reverse=reverse)


def sort_by_y(coord_list, reverse=False):
    """
    Sorts coordinate list by y component
    :param coord_list: Coords to sort
    :param reverse: If True, sorts high-to-low, else low-to-high
    :returns: List of sorted Coords
    """
    return sorted(coord_list, key=lambda cur: cur.y, reverse=reverse)


def line_bresenham(start, end):
    """
    Calculate coords that make up a line using Bresenham line-drawing algorithm
    :param start: line start Coord
    :param end: line end Coord
    :returns: List of line Coords
    """

    x1 = start.x
    y1 = start.y
    x2 = end.x
    y2 = end.y

    dx = x2 - x1
    dy = y2 - y1

    is_steep = abs(dy) > abs(dx)
    if is_steep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2

    swapped = False
    if x1 > x2:
        x1, x2, = x2, x1
        y1, y2 = y2, y1
        swapped = True

    dx = x2 - x1
    dy = y2 - y1

    error = int(dx / 2.0)
    ystep = 1 if y1 < y2 else -1

    y = y1
    points = []
    for x in range(x1, x2 + 1):
        coord = Coord(y, x) if is_steep else Coord(x, y)
        points.append(coord)
        error -= abs(dy)
        if error < 0:
            y += ystep
            error += dx

    if swapped:
        points.reverse()

    return points


class Rect:
    """
    A rectangle defined by a top-left coord and a width/height
    """

    def __init__(self, pos, dim):
        """
        Create a new Rect
        :param pos: Top-left Coord
        :param dim: Width and height as Coord
        """
        assert dim.x >= 0
        assert dim.y >= 0

        self.pos = pos
        self.dim = dim

    def center(self):
        """
        :returns: absolute position of the center tile of this rect
        """

        return Coord(self.pos.x + int(self.dim.x / 2),
                     self.pos.y + int(self.dim.y / 2))

    def __str__(self):
        return "pos=({}) dim=({})".format(self.pos, self.dim)

    def __eq__(self, other):
        return self.pos == other.pos and self.dim == other.dim

    def contains(self, pos):
        """
        :param pos: Coord to test
        :returns: True if pos is inside this rect
        """

        return pos.x >= self.pos.x and pos.x < self.pos.x + self.dim.x and \
               pos.y >= self.pos.y and pos.y < self.pos.y + self.dim.y

    def constrain(self, pos):
        """
        Move the given position to be just inside this rectangle
        :param pos: Given position
        :returns: New adjusted position
        """

        npos = Coord(pos.x, pos.y)
        adjust = False

        if npos.x < self.pos.x:
            npos.x = self.pos.x
            adjust = True
        if npos.y < self.pos.y:
            npos.y = self.pos.y
            adjust = True

        if npos.x >= self.dim.x:
            npos.x = self.dim.x - 1
            adjust = True
        if npos.y >= self.dim.y:
            npos.y = self.dim.y - 1
            adjust = True

        return (npos, adjust)

    def recenter(self, new_center):
        """
        Creates a new rectangle of the same size centered on a new position
        :param pos: New position
        """

        pos_off = self.center() - self.pos

        return Rect(new_center - pos_off, self.dim)

    def corners(self):
        """
        Generate the coordinates of the rectangle's corners
        If either dimension is 1, will return only a single coord for
        that dimension
        :yields: Coords for each corner
        """
        yield Coord(self.pos.x, self.pos.y)
        if self.dim.y > 1:
            yield Coord(self.pos.x, self.pos.y + self.dim.y - 1)
        if self.dim.x > 1:
            yield Coord(self.pos.x + self.dim.x - 1, self.pos.y)

        if self.dim.x > 1 or self.dim.y > 1:
            yield Coord(self.pos.x + self.dim.x - 1,
                        self.pos.y + self.dim.y - 1)

    def edges(self):
        """
        Generate coordinates of the rectangles edges
        :yields: Coords for each edge
        """
        yield from self.corners()
        if self.dim.y > 2:
            for cx in range(self.pos.x + 1, self.pos.x + self.dim.x - 1):
                yield Coord(cx, self.pos.y)
                yield Coord(cx, self.pos.y + self.dim.y - 1)

        if self.dim.x > 2:
            for cy in range(self.pos.y + 1, self.pos.y + self.dim.y - 1):
                yield Coord(self.pos.x, cy)
                yield Coord(self.pos.x + self.dim.x - 1, cy)

    def interior_coords(self):
        """
        Generate coordinates of the rectangle's interior
        :yields: Coords for all interior positions
        """
        for cy in range(self.pos.y + 1, self.pos.y + self.dim.y - 1):
            for cx in range(self.pos.x + 1, self.pos.x + self.dim.x - 1):
                yield Coord(cx, cy)

    def coords(self):
        """
        Generate coordinates of all rectangle positions
        :yields: Coords for each position in the rectangle
        """
        yield from self.edges()
        yield from self.interior_coords()

    def random_coords(self, gen, count, distinct=True):
        """
        Generate random coordinates in this rectangle
        :param count: Number of random coordinates to generate
        :param distinct: If True, each returned position will be unique
        :returns: List of random Coords
        """
        out = []
        while len(out) < count:
            pos = Coord(
                gen.randint(self.pos.x, self.pos.x + self.dim.x - 1),
                gen.randint(self.pos.y, self.pos.y + self.dim.y - 1))
            if not distinct:
                out.append(pos)
            elif pos not in out:
                out.append(pos)

        return out


def intersect(rect_a, rect_b):
    """
    Rectangle intersection test
    :params rect_a, rect_b: Two rectangles to compare
    :returns: Intersection rectangle if a and b intersect, or None if they
        do not intersect
    """

    left_x = max(rect_a.pos.x, rect_b.pos.x)
    right_x = min(rect_a.pos.x + rect_a.dim.x, rect_b.pos.x + rect_b.dim.x)
    top_y = max(rect_a.pos.y, rect_b.pos.y)
    bottom_y = min(rect_a.pos.y + rect_a.dim.y, rect_b.pos.y + rect_b.dim.y)

    if left_x <= right_x and top_y <= bottom_y:
        # intersection rectangle
        return Rect(
            Coord(left_x, top_y), Coord(right_x - left_x, bottom_y - top_y))

    # no intersection
    return None


def centered_rect(center_pos, dim):
    """
    Creates a new rectangle centered at a particular location insted of
    setting the top-left coord
    :param center_pos: Coord to center rectangle on
    :param dim: Coord containing rectangle dimensions
    :returns: New, centered Rect
    """

    x_center = center_pos.x - int(dim.x / 2) if dim.x > 1 else center_pos.x
    y_center = center_pos.y - int(dim.y / 2) if dim.y > 1 else center_pos.y

    return Rect(Coord(x_center, y_center), Coord(dim.x, dim.y))


class Polygon:
    """
    N-sided polygon, defined by a list of points
    """

    def __init__(self, points):
        self.points = points

    def centroid(self):
        """
        Get centroid of this polygon - the 'center of mass'
        :returns: Coord of polygon centroid
        """

        cross_sum = 0
        v_sum = Coord(0, 0)

        for i in range(len(self.points)):
            v1 = self.points[i]
            v2 = self.points[(i + 1) % len(self.points)]

            cross = v1.x * v2.y - v1.y * v2.x
            cross_sum += cross

            v_sum = Coord(((v1.x + v2.x) * cross) + v_sum.x,
                          ((v1.y + v2.y) * cross) + v_sum.y)

        z = float(1) / float(3 * float(cross_sum))
        return Coord(float(v_sum.x) * z, float(v_sum.y) * z)

    def bounding_rect(self):
        """
        Get bounding rectangle of this polygon
        :returns: Rect large enough to contain this polygon
        """

        max_p = Coord(0, 0)
        min_p = Coord(1000, 1000)

        for point in self.points:
            if point.x > max_p.x:
                max_p.x = point.x
            elif point.x < min_p.x:
                min_p.x = point.x

            if point.y > max_p.y:
                max_p.y = point.y
            elif point.y < min_p.y:
                min_p.y = point.y

        return Rect(min_p, max_p + Coord(1, 1))

    def contains(self, coord):
        """
        Test if this polygon contains the given position
        :param coord: Position to test
        :returns: True if the given Coord is contained in this polygon
        """

        # Implementation of PNPOLY - Point Inclusion in Polygon Test
        # by W. Randolph Franklin
        # https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html
        # Copyright (c) 1970-2003, Wm. Randolph Franklin
        # Permission is hereby granted, free of charge, to any person obtaining
        # a copy of this software and associated documentation files
        # (the "Software"), to deal in the Software without restriction,
        # including without limitation the rights to use, copy, modify, merge,
        # publish, distribute, sublicense, and/or sell copies of the Software,
        # and to permit persons to whom the Software is furnished to do so,
        # subject to the following conditions:
        #     * Redistributions of source code must retain the above copyright
        #       notice, this list of conditions and the following disclaimers.
        #     * Redistributions in binary form must reproduce the above
        #       copyright notice in the documentation and/or other materials
        #       provided with the distribution.
        #     * The name of W. Randolph Franklin may not be used to endorse or
        #       promote products derived from this Software without specific
        #       prior written permission.
        # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        # EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
        # NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
        # BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
        # ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        # CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        # SOFTWARE.
        i = 0
        j = len(self.points) - 1
        contains = False
        cx = float(coord.x)
        cy = float(coord.y)
        while i < len(self.points):
            ix = float(self.points[i].x)
            iy = float(self.points[i].y)
            jx = float(self.points[j].x)
            jy = float(self.points[j].y)

            if (iy > cy) != (jy > cy):
                if cx < ((jx - ix) * (cy - iy) / (jy - iy) + ix):
                    contains = not contains

            j = i
            i += 1

        return contains


def random_polygon_from_rect(gen, rect, padding, max_side_points):
    """
    Generate a random polygon contained in a rectangle
    :param rect: Bounding rectangle
    :param padding: width of the four padding rectangles within which the
                    polygon's points will be generated
    :param max_side_points: Maximum number of points per side of the bounding
                            rectangle (actual # will vary between 1 and this)
    :returns: New random Polygon
    """

    top_rect = Rect(
        Coord(rect.pos.x + padding, rect.pos.y),
        Coord(rect.dim.x - (padding * 2), padding))
    bottom_rect = Rect(
        Coord(rect.pos.x + padding, rect.pos.y + rect.dim.y - padding),
        Coord(rect.dim.x - (padding * 2), padding))
    left_rect = Rect(
        Coord(rect.pos.x, rect.pos.y + padding),
        Coord(padding, rect.dim.y - (padding * 2)))
    right_rect = Rect(
        Coord(rect.pos.x + rect.dim.x - padding, rect.pos.y + padding),
        Coord(padding, rect.dim.y - (padding * 2)))

    # pick random points going clockwise from top
    points = []
    points.extend(
        sort_by_x(
            top_rect.random_coords(gen, gen.randint(1, max_side_points))))
    points.extend(
        sort_by_y(
            right_rect.random_coords(gen, gen.randint(1, max_side_points))))
    points.extend(
        sort_by_x(
            bottom_rect.random_coords(gen, gen.randint(1, max_side_points)),
            reverse=True))
    points.extend(
        sort_by_y(
            left_rect.random_coords(gen, gen.randint(1, max_side_points)),
            reverse=True))

    return Polygon(points)
