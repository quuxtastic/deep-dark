"""
Random number generation helpers
"""

import time
import tcod


class Generator:
    def __init__(self, seed=None):
        self.seed(seed)

    def seed(self, seed=None):
        if seed is None:
            seed = time.time()

        self.__gen = tcod.random.Random(0, seed)
        self.__seed = seed

    def get_seed(self):
        return self.__seed

    def randint(self, low, high):
        return self.__gen.randint(low, high)

    def gauss(self, mu, sigma):
        return self.__gen.guass(mu, sigma)

    def inverse_gauss(self, mu, sigma):
        return self.__gen.inverse_guass(mu, sigma)

    def uniform(self, low, high):
        return self.__gen.uniform(low, high)

    def choice(self, seq):
        return seq[self.randint(0, len(seq) - 1)]

    def perc(self, chance):
        return self.randint(1, 100) <= chance

    def roll(self, die, count=1, mod=0):
        roll_sum = 0
        for _ in range(count):
            roll_sum += self.randint(1, die)

        return roll_sum + mod

    def draw(self, cards, count=1):
        if len(cards) < count:
            raise ValueError(
                "Can't draw {} cards from list with {} items".format(
                    count, len(cards)))

        indexes = [i for i in range(len(cards))]

        out = list()
        for _ in range(count):
            index = self.choice(indexes)

            out.append(cards[index])

            indexes.remove(index)
            del cards[index]

        if count == 1:
            return out[0]

        return out
