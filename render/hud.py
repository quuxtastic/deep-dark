"""
UI elements in the main game view
"""

import tcod

from render.renderer import Surface, Layers
from render.ui_components import Bar, ScrollableList
from components.equipSlots import EquipSlots
from constants import SCREEN_WIDTH, SCREEN_HEIGHT, HUD_WIDTH, HUD_HEIGHT
from geom import Rect, Coord
from message import Priority
import colors


class HUD(Surface):
    def __init__(self, ctx):
        super().__init__(
            Rect(
                Coord(0, SCREEN_HEIGHT - HUD_HEIGHT),
                Coord(HUD_WIDTH, SCREEN_HEIGHT)), Layers.MAP,
            colors.hud['bars'])

        self.ctx = ctx

        self.__hp_bar = Bar(
            rect=Rect(Coord(1, 1), Coord(self.rect.dim.x - 2, 1)),
            color_scheme=self.colors['hp'],
            name='HP',
            value=0,
            max_value=0)
        self.__hunger_bar = Bar(
            rect=Rect(Coord(1, 2), Coord(self.rect.dim.x - 2, 1)),
            color_scheme=self.colors['hunger'],
            name='Hunger',
            value=0,
            max_value=0)
        self.__fuel_bar = Bar(
            rect=Rect(Coord(1, 3), Coord(self.rect.dim.x - 2, 1)),
            color_scheme=self.colors['fuel'],
            name='Lumes',
            value=0,
            max_value=0)
        self.__inventory_bar = Bar(
            rect=Rect(Coord(1, 4), Coord(self.rect.dim.x - 2, 1)),
            color_scheme=self.colors['inventory'],
            name='Encumbrance',
            value=0,
            max_value=0)

    def _draw(self, console, cur_time):
        console.set_fg(colors.ui['frame'])
        console.draw_hline(Coord(1, 0), HUD_WIDTH - 1)
        console.draw_vline(Coord(0, 1), HUD_HEIGHT - 1)
        console.draw_vline(Coord(HUD_WIDTH - 1, 1), HUD_HEIGHT - 1)
        console.draw_char(Coord(0, 0), tcod.CHAR_NW)
        console.draw_char(Coord(HUD_WIDTH - 1, 0), tcod.CHAR_TEES)

        self.__hp_bar.set_value(self.ctx.player.get_component('hp').hp)
        self.__hp_bar.set_max_value(self.ctx.player.get_component('hp').max_hp)
        self.__hunger_bar.set_value(
            self.ctx.player.get_component('hunger').cur_hunger)
        self.__hunger_bar.set_max_value(
            self.ctx.player.get_component('hunger').max_hunger)
        self.__fuel_bar.set_value(
            self.ctx.player.get_component('fuelTank').current)
        self.__fuel_bar.set_max_value(
            self.ctx.player.get_component('fuelTank').capacity)
        self.__inventory_bar.set_value(
            self.ctx.player.get_component('inventory').cur_weight)
        self.__inventory_bar.set_max_value(
            self.ctx.player.get_component('inventory').capacity)

        self.__hp_bar.draw(console)
        self.__hunger_bar.draw(console)
        self.__fuel_bar.draw(console)
        self.__inventory_bar.draw(console)

        console.set_bg(self.colors['bg'])
        console.set_fg(self.colors['text'])
        console.draw_text(
            Coord(1, 5), "Steps taken: {}".format(
                self.ctx.player.get_component('playerThink').steps))
        console.draw_text(
            Coord(1, 6), "Dungeon level: {}".format(self.ctx.world.index))

        all_tiles, explored_tiles = self.ctx.world.calc_explored()

        console.draw_text(
            Coord(1, 7), "Explored: {}%".format(
                int(float(explored_tiles) / float(all_tiles) * 100)))

        cur_lantern = self.ctx.player.get_component('equipSlots').get_at_slot(
            EquipSlots.LIGHT_SOURCE)
        if not cur_lantern:
            cur_lantern = 'No lantern'
        else:
            cur_lantern = '{}: {}'.format(
                cur_lantern.name,
                'On' if cur_lantern.get_component('usable').active else 'Off')
        console.draw_text(Coord(1, 8), cur_lantern)

        lvl = self.ctx.player.get_component('level')
        if lvl.can_level_up():
            console.set_bg(self.colors['alt_bg'])
            console.set_fg(self.colors['alt_text'])
        console.draw_text(
            Coord(1, 9),
            'Level {}, {}/{} XP'.format(lvl.level, lvl.xp,
                                        lvl.next_level_xp()),
            bg_blend=tcod.BKGND_SET)


class Messages(Surface):
    def __init__(self, ctx):
        super().__init__(
            Rect(
                Coord(HUD_WIDTH, SCREEN_HEIGHT - HUD_HEIGHT),
                Coord(SCREEN_WIDTH - HUD_WIDTH, HUD_HEIGHT)), Layers.MAP,
            colors.hud['messages'])
        self.ctx = ctx
        ctx.message_log.attach(self)

        self.__message_area = ScrollableList(
            rect=Rect(
                Coord(0, 1), Coord(self.rect.dim.x, self.rect.dim.y - 1)),
            color_scheme=colors.ui,
            show_selection=False,
            show_scrollbar=True)

    def add(self, message, priority):
        color = self.colors['text']
        if priority == Priority.LOW:
            color = self.colors['low_text']
        elif priority == Priority.HIGH:
            color = self.colors['high_text']
        elif priority == Priority.DEBUG:
            color = self.colors['debug_text']

        self.__message_area.prepend(
            message,
            wrap=True,
            scroll=False,
            color=color,
            wrap_indent=chr(tcod.CHAR_ARROW_E) + ' ')
        self.__message_area.set_index(0)

    def clear(self):
        self.__message_area.clear()

    def scroll_up(self):
        self.__message_area.scroll_up()

    def scroll_down(self):
        self.__message_area.scroll_down()

    def _draw(self, console, cur_time):
        console.set_fg(colors.ui['frame'])
        console.draw_hline(Coord(0, 0), SCREEN_WIDTH - HUD_WIDTH)

        self.__message_area.draw(console)


class TextCommandInputPrompt(Surface):
    def __init__(self):
        super().__init__(
            Rect(Coord(0, 0), Coord(SCREEN_WIDTH, 1)), Layers.TEXT_COMMAND,
            colors.hud['command'])

        self.input = ''

    def _draw(self, console, cur_time):
        console.set_fg(self.colors['text'])
        console.draw_text(Coord(0, 0), self.input)
