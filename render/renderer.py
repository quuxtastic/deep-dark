"""
Rendering
"""

from enum import Enum, auto

import tcod

from render.console import Console
from constants import SCREEN_WIDTH, SCREEN_HEIGHT


class Layers(Enum):
    """
    Z-ordering of surfaces on screen
    """
    MAP = auto()
    MENU = auto()
    PROMPT = auto()
    HELP = auto()
    TEXT_COMMAND = auto()


class Surface:
    def __init__(self, rect, layer, color_scheme):
        self.__console = Console(rect)
        self.rect = rect
        self.layer = layer
        self.colors = color_scheme

    def draw(self, root, cur_time):
        self.__console.set_bg(self.colors['bg'])
        self.__console.clear()

        self._draw(self.__console, cur_time)

        self.__console.blit(root)

    def _draw(self, console, cur_time):
        pass


class Renderer:
    def __init__(self):
        """
        Create a Renderer
        :param screen_width, screen_height: Dimensions of whole console screen
        :param hud_width,hud_height: Dimensions of hud bar section of screen
        """

        tcod.console_set_custom_font(
            'dejavu16x16_gs_tc.png',
            tcod.FONT_TYPE_GREYSCALE | tcod.FONT_LAYOUT_TCOD)
        self.__root = tcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT,
                                             'DEEP DARK', False)

        self.__surfaces = []

        self.__map_view = None
        self.__hud_bars = None
        self.__hud_messages = None
        self.__menu = None

    def add_surface(self, surface):
        self.__surfaces.append(surface)
        self.__surfaces = sorted(self.__surfaces, key=lambda x: x.layer.value)

    def remove_surface(self, surface):
        self.__surfaces.remove(surface)

    def set_map_view(self, map_view):
        if self.__map_view:
            self.remove_surface(self.__map_view)
            self.__map_view = None

        if map_view:
            self.__map_view = map_view
            self.add_surface(map_view)

    def get_map_view(self):
        return self.__map_view

    def set_hud_bars(self, hud_bars):
        if self.__hud_bars:
            self.remove_surface(self.__hud_bars)
            self.__hud_bars = None

        if hud_bars:
            self.__hud_bars = hud_bars
            self.add_surface(hud_bars)

    def get_hud_bars(self):
        return self.__hud_bars

    def set_hud_messages(self, hud_messages):
        if self.__hud_messages:
            self.remove_surface(self.__hud_messages)
            self.__hud_messages = None

        if hud_messages:
            self.__hud_messages = hud_messages
            self.add_surface(hud_messages)

    def get_hud_messages(self):
        return self.__hud_messages

    def set_menu(self, menu):
        if self.__menu:
            self.remove_surface(self.__menu)
            self.__menu = None

        if menu:
            self.__menu = menu
            self.add_surface(menu)

    def get_menu(self):
        return self.__menu

    def do_render(self, cur_time):
        self.__root.clear()

        for surface in self.__surfaces:
            surface.draw(self.__root, cur_time)

        tcod.console_flush()
