"""
Base rendering elements
"""

import tcod

from geom import Rect


class Console:
    """
    A Console is the basic drawing surface
    """

    def __init__(self, rect):
        """
        Create a new Console
        :param rect: Dimensions of console
        :param root: Parent console (drawn to on blit)
        """
        self.rect = rect

        self.__console = tcod.console_new(rect.dim.x, rect.dim.y)

    def draw_char(self, pos, ch_code, bg_blend=tcod.BKGND_NONE):
        self.__console.put_char(pos.x, pos.y, ch_code, bg_blend)

    def draw_bg(self, pos, color):
        self.__console.bg[pos.y, pos.x, 0] = color[0]
        self.__console.bg[pos.y, pos.x, 1] = color[1]
        self.__console.bg[pos.y, pos.x, 2] = color[2]

    def draw_fg(self, pos, color):
        self.__console.fg[pos.y, pos.x, 0] = color[0]
        self.__console.fg[pos.y, pos.x, 1] = color[1]
        self.__console.fg[pos.y, pos.x, 2] = color[2]

    def set_bg(self, color):
        self.__console.default_bg = color

    def set_fg(self, color):
        self.__console.default_fg = color

    def draw_text(self,
                  pos,
                  message,
                  bg_blend=tcod.BKGND_NONE,
                  align=tcod.LEFT):
        self.__console.print_(pos.x, pos.y, message, bg_blend, align)

    def draw_text_wrap(self,
                       pos,
                       message,
                       dim=None,
                       bg_blend=tcod.BKGND_NONE,
                       align=tcod.LEFT):
        if not dim:
            dim = self.rect.dim
        self.__console.print_rect(pos.x, pos.y, dim.x, dim.y, message,
                                  bg_blend, align)

    def draw_rect(self, rect, clear=True, bg_blend=tcod.BKGND_NONE):
        self.__console.rect(rect.pos.x, rect.pos.y, rect.dim.x, rect.dim.y,
                            clear, bg_blend)

    def draw_framed_rect(self, rect, clear=True, bg_blend=tcod.BKGND_NONE):
        self.__console.print_frame(
            rect.pos.x,
            rect.pos.y,
            rect.dim.x,
            rect.dim.y,
            string='',
            clear=clear,
            bg_blend=bg_blend)

    def draw_hline(self, start_pos, length, bg_blend=tcod.BKGND_NONE):
        self.__console.hline(
            start_pos.x, start_pos.y, length, bg_blend=bg_blend)

    def draw_vline(self, start_pos, length, bg_blend=tcod.BKGND_NONE):
        self.__console.vline(start_pos.x, start_pos.y, length, bg_blend)

    def blit(self, root, fg_alpha=1.0, bg_alpha=1.0, key=None):
        self.__console.blit(root, self.rect.pos.x, self.rect.pos.y, 0, 0,
                            self.rect.dim.x, self.rect.dim.y, fg_alpha,
                            bg_alpha, key)

    def clear(self):
        self.__console.clear()

    def calc_needed_height(self, text, rect=None):
        if rect is None:
            rect = self.rect
        return self.__console.get_height_rect(rect.pos.x, rect.pos.y,
                                              rect.dim.x, rect.dim.y, text)


class ConsoleView:
    def __init__(self, console, offset_rect):
        self.__console = console
        self.rect = offset_rect

    def draw_char(self, pos, ch_code, bg_blend=tcod.BKGND_NONE):
        self.__console.draw_char(self.rect.pos + pos, ch_code, bg_blend)

    def draw_bg(self, pos, color):
        self.__console.draw_bg(self.rect.pos + pos, color)

    def draw_fg(self, pos, color):
        self.__console.draw_fg(self.rect.pos + pos, color)

    def set_bg(self, color):
        self.__console.set_bg(color)

    def set_fg(self, color):
        self.__console.set_fg(color)

    def draw_text(self,
                  pos,
                  message,
                  bg_blend=tcod.BKGND_NONE,
                  align=tcod.LEFT):
        self.__console.draw_text(self.rect.pos + pos, message, bg_blend, align)

    def draw_text_wrap(self,
                       pos,
                       message,
                       dim=None,
                       bg_blend=tcod.BKGND_NONE,
                       align=tcod.LEFT):
        if not dim:
            dim = self.rect.dim
        self.__console.draw_text_wrap(self, self.rect.pos + pos, dim, message,
                                      bg_blend, align)

    def draw_rect(self, rect, clear=True, bg_blend=tcod.BKGND_NONE):
        self.__console.draw_rect(
            Rect(self.rect.pos + rect.pos, rect.dim), clear, bg_blend)

    def draw_framed_rect(self, rect, clear=True, bg_blend=tcod.BKGND_NONE):
        self.__console.draw_framed_rect(
            self, Rect(self.rect.pos + rect.pos, rect.dim), clear, bg_blend)

    def draw_hline(self, start_pos, length, bg_blend=tcod.BKGND_NONE):
        self.__console.draw_hline(start_pos, length, bg_blend)

    def draw_vline(self, start_pos, length, bg_blend=tcod.BKGND_NONE):
        self.__console.draw_vline(start_pos, length, bg_blend)

    def blit(self, root, fg_alpha=1.0, bg_alpha=1.0, key=None):
        self.__console.blit(root, fg_alpha, bg_alpha, key)

    def clear(self):
        self.__console.draw_rect(self.rect)

    def calc_needed_height(self, rect, text):
        return self.__console.calc_needed_height(
            Rect(self.rect.pos + rect.pos, rect.dim), text)
