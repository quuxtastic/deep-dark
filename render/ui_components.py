"""
Common sub-components used by various UI elements
"""

import textwrap
import tcod

from render.console import ConsoleView
from render.renderer import Surface
from components.usable import TargetTypes
from geom import Coord, Rect, line_bresenham, centered_rect


class TargetIndicator:
    def __init__(self, color_scheme):
        self.__type = None

        self.__show = True
        self.__blink_last = float(0)
        self.__blink_speed = float(0.1)

        self.__colors = color_scheme

    def __draw_single(self, console):
        console.set_fg(self.__colors['active_fg'])
        console.draw_char(self.__target, tcod.CHAR_BLOCK1)

    def __draw_square(self, console):
        console.set_fg(self.__colors['active_fg'])
        for screen_coord in self.__square_rect.coords():
            console.draw_char(screen_coord, tcod.CHAR_BLOCK1)

    def __draw_list(self, console):
        console.set_fg(self.__colors['active_fg'])
        console.draw_char(self.__target, tcod.CHAR_BLOCK1)

        console.set_fg(self.__colors['inactive_fg'])
        for coord in self.__target_list:
            if coord != self.__target:
                console.draw_char(coord, tcod.CHAR_BLOCK1)

    def __draw_line(self, console):
        console.set_fg(self.__colors['active_fg'])
        for coord in self.__target_list:
            console.draw_char(coord, tcod.CHAR_BLOCK1)

    def disable(self):
        self.__type = None

    def set_single(self, target):
        self.__type = TargetTypes.SINGLE
        self.__target = target

    def set_square(self, center, radius):
        self.__type = TargetTypes.SQUARE
        self.__square_rect = centered_rect(
            center, Coord(radius * 2 + 1, radius * 2 + 1))

    def set_list(self, coords, active_coord):
        self.__type = TargetTypes.LIST_IN_FOV
        self.__target_list = coords
        self.__target = active_coord

    def set_line(self, start, end):
        self.__type = TargetTypes.LINE
        self.__target_list = line_bresenham(start, end)

    def draw(self, console, cur_time):
        if cur_time - self.__blink_last > self.__blink_speed:
            self.__show = not self.__show
            self.__blink_last = cur_time

        if self.__show:
            if self.__type == TargetTypes.SINGLE:
                self.__draw_single(console)

            elif self.__type == TargetTypes.SQUARE:
                self.__draw_square(console)

            elif self.__type == TargetTypes.LIST_IN_FOV:
                self.__draw_list(console)

            elif self.__type == TargetTypes.LINE:
                self.__draw_line(console)


class FramedWindow(Surface):
    def __init__(self, rect, title, layer, color_scheme):
        super().__init__(rect, layer, color_scheme)

        self.title = title
        self.inner_rect = Rect(
            Coord(2, 2), Coord(rect.dim.x - 4, rect.dim.y - 4))

    def _draw(self, console, cur_time):
        console.set_fg(self.colors['frame'])
        console.draw_framed_rect(
            Rect(Coord(0, 0), Coord(console.rect.dim.x, console.rect.dim.y)))

        console.set_fg(self.colors['title_text'])
        console.set_bg(self.colors['title_bg'])
        console.draw_text(
            Coord(console.rect.dim.x / 2, 0),
            self.title,
            align=tcod.CENTER,
            bg_blend=tcod.BKGND_SET)

        self._draw_inner(ConsoleView(console, self.inner_rect), cur_time)

    def _draw_inner(self, console, cur_time):
        pass


class Bar:
    def __init__(self, rect, color_scheme, name, value, max_value):
        self.__rect = rect
        self.__name = name
        self.__value = value
        self.__max_value = max_value
        self.__colors = color_scheme

    def draw(self, console):
        bar_width = int(
            float(self.__value) / self.__max_value * self.__rect.dim.x)

        console.set_bg(self.__colors['bg'])
        console.draw_rect(self.__rect, False, tcod.BKGND_SCREEN)

        console.set_bg(self.__colors['fg'])
        if bar_width > 0:
            console.draw_rect(
                Rect(self.__rect.pos, Coord(bar_width, self.__rect.dim.y)),
                False, tcod.BKGND_SCREEN)

        console.set_fg(self.__colors['text'])
        console.draw_text(
            Coord(
                int(self.__rect.pos.x + self.__rect.dim.x / 2),
                self.__rect.pos.y),
            '{} {}/{}'.format(self.__name, self.__value,
                              self.__max_value), tcod.BKGND_NONE, tcod.CENTER)

    def set_value(self, value):
        self.__value = max(min(value, self.__max_value), 0)

    def set_max_value(self, value):
        self.__max_value = max(value, 0)


class ScrollableList:
    def __init__(self,
                 rect,
                 color_scheme,
                 show_selection=True,
                 show_scrollbar=True,
                 align=tcod.LEFT):
        self.__rect = rect
        self.__colors = color_scheme
        self.__show_selection = show_selection
        self.__show_scrollbar = show_scrollbar
        self.__item_align = align

        self.__lines = []
        self.__index = 0
        self.__selected_index = 0

    def draw(self, console):
        console.set_bg(self.__colors['bg'])
        console.draw_rect(self.__rect, clear=True)

        show_scrollbar = self.__show_scrollbar and self.__rect.dim.y < len(
            self.__lines)
        item_width = self.__rect.dim.x
        if show_scrollbar:
            item_width = self.__rect.dim.x - 3

        y_pos = self.__rect.pos.y
        for i in range(
                self.__index,
                min(len(self.__lines), self.__index + self.__rect.dim.y)):

            text, color, bg_color = self.__lines[i]

            console.set_fg(color)

            if self.__item_align == tcod.LEFT:
                x_pos = self.__rect.pos.x
            elif self.__item_align == tcod.RIGHT:
                x_pos = item_width - 2
            elif self.__item_align == tcod.CENTER:
                x_pos = item_width / 2

            if i == self.__selected_index and self.__show_selection:
                console.set_bg(self.__colors['selected_bg'])
            else:
                console.set_bg(bg_color)

            console.draw_rect(
                Rect(
                    Coord(self.__rect.pos.x, y_pos), Coord(item_width - 1, 1)),
                clear=False,
                bg_blend=tcod.BKGND_SCREEN)
            console.set_bg(self.__colors['bg'])

            console.draw_text(
                Coord(x_pos, y_pos), text, align=self.__item_align)

            y_pos += 1

        if self.__show_scrollbar and self.__rect.dim.y < len(self.__lines):
            console.set_bg(self.__colors['scrollbar_bg'])
            console.draw_rect(
                Rect(
                    Coord(self.__rect.pos.x + self.__rect.dim.x - 1,
                          self.__rect.pos.y), Coord(1, self.__rect.dim.y)),
                False, tcod.BKGND_SCREEN)

            console.set_bg(self.__colors['scrollbar'])

            # let's do some ratios!
            # visible_lines / lines == scrollbar_height / rect_height
            scrollbar_height = int(
                max((self.__rect.dim.y**2) / len(self.__lines), 1))
            # __index / lines == scrollbar_pos / rect_height
            scrollbar_pos = min(
                round((self.__index * self.__rect.dim.y) / len(self.__lines)),
                self.__rect.dim.y - scrollbar_height)
            console.draw_rect(
                Rect(
                    Coord(self.__rect.pos.x + self.__rect.dim.x - 1,
                          self.__rect.pos.y + scrollbar_pos),
                    Coord(1, scrollbar_height)), False, tcod.BKGND_SCREEN)

    def __split_text(self, text, indent):
        out = []
        for line in text.split('\n'):
            out.extend(
                textwrap.wrap(
                    line, self.__rect.dim.x - 1, subsequent_indent=indent))

        return out

    def __truncate_text(self, text):
        if len(text) > self.__rect.dim.x:
            return text[0:self.__rect.dim.x - 3] + '...'

        return text

    def append(self,
               text,
               wrap=True,
               color=None,
               bg_color=None,
               wrap_indent=''):
        if color is None:
            color = self.__colors['text']
        if bg_color is None:
            bg_color = self.__colors['list_entry_bg']

        if wrap:
            for line in self.__split_text(text, indent=wrap_indent):
                self.__lines.append((line, color, bg_color))
        else:
            line = self.__truncate_text(text)
            self.__lines.append((line, color, bg_color))

    def prepend(self,
                text,
                scroll=True,
                wrap=True,
                color=None,
                bg_color=None,
                wrap_indent=''):
        if color is None:
            color = self.__colors['text']
        if bg_color is None:
            bg_color = self.__colors['list_entry_bg']

        index = 0

        if wrap:
            for line in self.__split_text(text, indent=wrap_indent):
                self.__lines.insert(index, (line, color, bg_color))
                index += 1
        else:
            self.__lines.insert(index,
                                (self.__truncate_text(text), color, bg_color))
            index = 1

        if scroll:
            self.set_index(self.__index + index)
            self.set_selected_index(self.__selected_index + index)

    def remove(self, index, scroll=True):
        del self.__lines[index]

        if scroll:
            if self.__index >= index:
                self.set_index(self.__index - 1)
            if self.__selected_index >= index:
                self.set_selected_index(self.__selected_index - 1)

    def set_index(self, index):
        if len(self.__lines) < self.__rect.dim.y:
            return

        index = min(index, len(self.__lines))
        index = max(index, 0)

        self.__index = min(index, len(self.__lines) - self.__rect.dim.y)

    def get_index(self):
        return self.__index

    def set_selected_index(self, index, scroll=True):
        index = min(index, len(self.__lines))
        index = max(index, 0)

        self.__selected_index = index

        if scroll:
            if index < self.__index:
                self.set_index(self.__selected_index)
            elif index > self.__index + self.__rect.dim.y - 1:
                self.set_index(self.__selected_index - self.__rect.dim.y + 1)

    def get_selected_index(self):
        return self.__selected_index

    def select_up(self, scroll=True, wrap=True):
        index = self.__selected_index - 1
        if wrap:
            index = index % len(self.__lines)
        self.set_selected_index(index, scroll)

    def select_down(self, scroll=True, wrap=True):
        index = self.__selected_index + 1
        if wrap:
            index = index % len(self.__lines)
        self.set_selected_index(index, scroll)

    def scroll_up(self):
        self.set_index(self.__index - 1)

    def scroll_down(self):
        self.set_index(self.__index + 1)

    def clear(self):
        self.__index = 0
        self.__selected_index = 0
        self.__lines.clear()


class ColumnList(ScrollableList):
    def __init__(self,
                 rect,
                 color_scheme,
                 columns,
                 show_selection=True,
                 show_scrollbar=True):
        self.header_pos = rect.pos
        self.__colors = color_scheme
        super().__init__(
            Rect(
                Coord(rect.pos.x, rect.pos.y + 1),
                Coord(rect.dim.x, rect.dim.y - 1)),
            color_scheme,
            show_selection=show_selection,
            show_scrollbar=show_scrollbar)

        # assume scrollbar exists
        width = rect.dim.x - 3

        self.columns = []
        for name, col_width, col_align in columns:
            if col_align == tcod.LEFT:
                col_align = lambda s, w: s.ljust(w)
            elif col_align == tcod.CENTER:
                col_align = lambda s, w: s.center(w)
            elif col_align == tcod.RIGHT:
                col_align = lambda s, w: s.rjust(w)
            self.columns.append([name, col_width, col_align])
            width -= col_width + 1
        width += 1  # remove trailing separator

        for entry in self.columns:
            if entry[1] == 0:
                entry[1] = width
                break

        self.header_str = self.__make_row([entry[0] for entry in self.columns])

    def __truncate_entry(self, index, text):
        adjust = self.columns[index][2]
        width = self.columns[index][1]

        if len(text) > width:
            text = text[0:width]

        return adjust(text, width)

    def __make_row(self, entries):
        assert len(entries) == len(self.columns)

        cols = []

        for i, entry in enumerate(entries):
            cols.append(self.__truncate_entry(i, str(entry)))

        return ' '.join(cols)

    def append(self, entries, color=None, bg_color=None):
        super().append(
            self.__make_row(entries),
            color=color,
            bg_color=bg_color,
            wrap=False)

    def prepend(self, entries, color=None, bg_color=None, scroll=True):
        super().prepend(
            self.__make_row(entries),
            color=color,
            bg_color=bg_color,
            wrap=False,
            scroll=scroll)

    def draw(self, console):
        console.set_bg(self.__colors['col_header_bg'])
        console.set_fg(self.__colors['col_header_text'])
        console.draw_text(
            self.header_pos, self.header_str, bg_blend=tcod.BKGND_SET)

        super().draw(console)
