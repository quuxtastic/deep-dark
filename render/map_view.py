"""
Drawing the world map
"""

from enum import IntEnum, auto

from render.renderer import Surface, Layers
from render.ui_components import TargetIndicator
from world_map import TRANSPARENT_BIT, DIGGABLE_BIT, EXPLORED_BIT
from events import DrawEvent
from constants import SCREEN_WIDTH, SCREEN_HEIGHT, HUD_HEIGHT
from geom import Coord, Rect
import colors


class RenderOrder(IntEnum):
    """
    Z-ordering of entities drawn on map
    """
    FLOOR = auto()
    CORPSE = auto()
    ITEM = auto()
    ACTOR = auto()


class MapView(Surface):
    """
    Console surface that displays the world map
    """

    def __init__(self, ctx):
        super().__init__(
            Rect(Coord(0, 0), Coord(SCREEN_WIDTH, SCREEN_HEIGHT - HUD_HEIGHT)),
            Layers.MAP, colors.world_map)

        self.ctx = ctx

        self.force_visible = False

        self.__map_offset = Coord(0, 0)
        self.__map_view_min = Coord(0, 0)
        self.__map_view_max = Coord(0, 0)

        self.targeting = TargetIndicator(colors.hud['targeting'])

    def __update_map_view(self):
        # Keep the map centered on the player, but dont scroll past the
        # boundaries of the map

        map_view_rect = self.rect.recenter(
            self.ctx.player.get_component('world').position)

        if self.ctx.world.width > self.rect.dim.x:
            self.__map_offset.x = max(0, map_view_rect.pos.x)
            self.__map_offset.x = min(
                self.__map_offset.x,
                self.ctx.world.width - map_view_rect.dim.x)
            self.__map_view_min.x = 0
            self.__map_view_max.x = self.rect.dim.x
        else:
            self.__map_offset.x = int(
                (self.ctx.world.width - self.rect.dim.x) / 2)
            self.__map_view_min.x = -self.__map_offset.x
            self.__map_view_max.x = -self.__map_offset.x + self.ctx.world.width

        if self.ctx.world.height > self.rect.dim.y:
            self.__map_offset.y = max(0, map_view_rect.pos.y)
            self.__map_offset.y = min(
                self.__map_offset.y,
                self.ctx.world.height - map_view_rect.dim.y)
            self.__map_view_min.y = 0
            self.__map_view_max.y = self.rect.dim.y
        else:
            self.__map_offset.y = int(
                (self.ctx.world.height - self.rect.dim.y) / 2)
            self.__map_view_min.y = -self.__map_offset.y
            self.__map_view_max.y = -self.__map_offset.y + self.ctx.world.height

    def __draw_tile(self, console, screen_coord, map_coord, cur_time, visible):
        tile_data = self.ctx.world.tile_map[map_coord.x, map_coord.y]

        is_wall = not tile_data & TRANSPARENT_BIT

        if visible:
            if is_wall:
                if tile_data & DIGGABLE_BIT:
                    color = colors.world_map['wall_light_diggable']
                else:
                    color = colors.world_map['wall_light']
            else:
                color = colors.world_map['ground_light']

            console.draw_bg(screen_coord, color)

        elif tile_data & EXPLORED_BIT:
            if is_wall:
                if tile_data & DIGGABLE_BIT:
                    color = colors.world_map['wall_dark_diggable']
                else:
                    color = colors.world_map['wall_dark']
            else:
                color = colors.world_map['ground_dark']

            console.draw_bg(screen_coord, color)

        if map_coord in (self.ctx.world.stairs_up_pos,
                         self.ctx.world.stairs_down_pos):
            if not visible and tile_data & EXPLORED_BIT:
                stairs = self.ctx.world.get_ents_at(
                    map_coord, {'components': {
                        'stairs': None
                    }})
                stairs[0].get_component('renderable').send(
                    DrawEvent(self.ctx, console, screen_coord, cur_time), [])

    def __draw_ents(self, console, screen_coord, map_coord, cur_time):
        renderables = [
            x.get_component('renderable') for x in self.ctx.world.get_ents_at(
                map_coord, {'components': {
                    'renderable': None
                }})
        ]
        for comp in sorted(
                renderables, key=lambda x: int(x.get_render_order())):
            comp.send(DrawEvent(self.ctx, console, screen_coord, cur_time), [])

    def _draw(self, console, cur_time):
        self.__update_map_view()

        vision = self.ctx.player.get_component('vision')
        vision.compute_fov()

        for sy in range(self.__map_view_min.y, self.__map_view_max.y):
            for sx in range(self.__map_view_min.x, self.__map_view_max.x):
                screen_coord = Coord(sx, sy)
                map_coord = self.screen_coord_to_map(screen_coord)

                visible = self.force_visible \
                          or vision.in_fov(map_coord, recompute=False)

                self.__draw_tile(console, screen_coord, map_coord, cur_time,
                                 visible)

                if visible:
                    self.__draw_ents(console, screen_coord, map_coord,
                                     cur_time)

        self.targeting.draw(console, cur_time)

    def screen_coord_to_map(self, screen_coord):
        return screen_coord + self.__map_offset

    def map_coord_to_screen(self, map_coord):
        return map_coord - self.__map_offset
