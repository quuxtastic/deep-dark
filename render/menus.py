"""
UI Menus
"""

import datetime
import textwrap
import tcod

from constants import SCREEN_WIDTH, SCREEN_HEIGHT
from geom import Coord, Rect, centered_rect
from render.renderer import Layers
from render.ui_components import ScrollableList, FramedWindow, ColumnList
from components.equipSlots import EquipSlots
import colors


class Prompt(FramedWindow):
    def __init__(self, title, message, rect=None, centered=False):
        if not rect:
            rect = centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 5))
        super().__init__(rect, title, Layers.PROMPT, colors.ui)
        self.message = message
        self.centered = centered

    def _draw_inner(self, console, cur_time):
        console.set_fg(self.colors['text'])
        if self.centered:
            y_off = 0
            for line in self.message.split('\n'):
                console.draw_text(
                    Coord(self.inner_rect.dim.x / 2, y_off),
                    line,
                    align=tcod.CENTER)
                y_off += 1
        else:
            console.draw_text(Coord(0, 0), self.message)


class TextInputPrompt(FramedWindow):
    def __init__(self, title, message):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 5)), title,
            Layers.PROMPT, colors.ui)
        self.message = message
        self.input = ''
        self.error = ''

    def _draw_inner(self, console, cur_time):
        console.set_fg(self.colors['text'])
        console.draw_text(Coord(0, 0), self.message)

        console.set_bg(self.colors['textinput_bg'])
        console.draw_rect(
            Rect(Coord(0, 1), Coord(console.rect.dim.x - 4, 1)), False,
            tcod.BKGND_SCREEN)

        console.set_fg(self.colors['textinput_text'])
        console.draw_text(
            Coord(int(console.rect.dim.x / 2), 1),
            self.input,
            align=tcod.CENTER)

        console.set_fg(self.colors['error_text'])
        console.draw_text(Coord(0, 3), self.error)


class ChoicePrompt(FramedWindow):
    def __init__(self, title, message, choices):
        super().__init__(
            rect=centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 4)),
            title=title,
            layer=Layers.PROMPT,
            color_scheme=colors.ui)
        self.message = message
        self.choices = choices

        self.__choices_area = ScrollableList(
            rect=Rect(
                Coord(0, 2),
                Coord(self.inner_rect.dim.x, self.inner_rect.dim.y - 2)),
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=True,
            align=tcod.CENTER)
        for choice in self.choices:
            self.__choices_area.append(choice)

    def _draw_inner(self, console, cur_time):
        console.set_bg(self.colors['bg'])
        console.set_fg(self.colors['text'])

        console.draw_text(Coord(0, 0), self.message)
        self.__choices_area.draw(console)

    def next(self):
        self.__choices_area.select_down()

    def prev(self):
        self.__choices_area.select_up()

    def selected(self):
        return self.choices[self.__choices_area.get_selected_index()]


class InventoryMenu(FramedWindow):
    def __init__(self, player, items):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT / 2)), 'INVENTORY',
            Layers.MENU, colors.ui)

        self.__items = self.__merge_items(items, player)

        columns = (
            ('U', 1, tcod.CENTER),  # usable
            ('E', 1, tcod.CENTER),  # equippable/equipped
            ('Item', 0, tcod.LEFT),  # item name
            ('Q', 3, tcod.RIGHT),  # item quantity
            ('W', 3, tcod.RIGHT),  # weight of single item
            ('S', 3, tcod.RIGHT),  # sum of all item weights
            ('Use', 5, tcod.CENTER)  # uses (if consumable)
        )
        self.__inventory_area = ColumnList(
            rect=Rect(
                Coord(0, 0),
                Coord(self.inner_rect.dim.x, self.inner_rect.dim.y - 1)),
            columns=columns,
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=True)

        self.__descriptions = []

        ident = player.get_component('identifier')

        for item_list in self.__items:
            item = item_list[0]
            usable = item.get_component('usable')
            consumable = item.get_component('consumable')
            equipment = item.get_component('equipment')

            item_name = ident.get_name(item)
            item_wt = item.get_component('item').weight
            sum_wt = sum([x.get_component('item').weight for x in item_list])

            consumable_str = ' '
            equipment_str = ' '
            usable_str = ' '
            if consumable:
                consumable_str = '{}/{}'.format(consumable.cur_uses,
                                                consumable.max_uses)
            if equipment:
                equipment_str = '+' if equipment.equipped_by else '-'
            if usable:
                usable_str = '*'

            self.__inventory_area.append((
                usable_str,  # usable
                equipment_str,  # equipment/equipped
                item_name,  # item name
                len(item_list),  # item quantity
                item_wt,  # single item weight
                sum_wt,  # weight of all items
                consumable_str  # uses (if consumable)
            ))

            self.__descriptions.append(
                textwrap.shorten(
                    ident.get_desc(item),
                    width=self.inner_rect.dim.x - 2,
                    placeholder='...'))

    def __merge_items(self, item_list, player):
        out_list = []
        slots = player.get_component('equipSlots')
        ident = player.get_component('identifier')

        names_dict = dict()

        for item in item_list:
            if slots.has_equipped(item):
                out_list.append([item])
                continue

            if not names_dict.get(item.name):
                names_dict[item.name] = [item]
            else:
                names_dict[item.name].append(item)

        out_list.extend(names_dict.values())

        return sorted(out_list, key=lambda x: ident.get_name(x[0]))

    def _draw_inner(self, console, cur_time):
        self.__inventory_area.draw(console)

        console.set_fg(self.colors['desc_text'])
        console.set_bg(self.colors['bg'])
        console.draw_text(
            Coord(0, console.rect.dim.y - 1),
            self.__descriptions[self.__inventory_area.get_selected_index()])

    def next(self):
        self.__inventory_area.select_down()

    def prev(self):
        self.__inventory_area.select_up()

    def selected(self):
        return self.__items[self.__inventory_area.get_selected_index()][0]


class ContextHelpWindow(FramedWindow):
    def __init__(self, title, text):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)), 'HELP: ' + title,
            Layers.HELP, colors.ui)

        self.__text_area = ScrollableList(
            rect=Rect(Coord(0, 0), self.inner_rect.dim),
            color_scheme=self.colors,
            show_selection=False,
            show_scrollbar=True)
        self.__text_area.append(text)

    def _draw_inner(self, console, cur_time):
        self.__text_area.draw(console)

    def scroll_up(self):
        self.__text_area.scroll_up()

    def scroll_down(self):
        self.__text_area.scroll_down()


class LoreTransitionWindow(FramedWindow):
    def __init__(self, title, text):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT / 2)), title,
            Layers.PROMPT, colors.ui)

        self.__text_area = ScrollableList(
            rect=Rect(Coord(0, 0), self.inner_rect.dim),
            color_scheme=self.colors,
            show_selection=False)
        self.__text_area.append(text)

    def _draw_inner(self, console, cur_time):
        self.__text_area.draw(console)

    def scroll_up(self):
        self.__text_area.scroll_up()

    def scroll_down(self):
        self.__text_area.scroll_down()


class OptionMenu(FramedWindow):
    def __init__(self, options, title=None, align=tcod.LEFT):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3)),
            title if title is not None else '', Layers.MENU, colors.ui)

        self.__option_area = ScrollableList(
            rect=Rect(Coord(0, 0), self.inner_rect.dim),
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=True,
            align=align)

        self.__options = options
        for opt in self.__options:
            self.__option_area.append(opt)

    def _draw_inner(self, console, cur_time):
        self.__option_area.draw(console)

    def next(self):
        self.__option_area.select_down()

    def prev(self):
        self.__option_area.select_up()

    def selected(self):
        return self.__options[self.__option_area.get_selected_index()]

    def set_options(self, options):
        self.__options = options
        self.__option_area.clear()
        for opt in options:
            self.__option_area.append(opt)


class ToggleSettingsMenu(FramedWindow):
    def __init__(self, options, title):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3)), title,
            Layers.MENU, colors.ui)

        self.__option_area = ScrollableList(
            rect=Rect(Coord(0, 0), self.inner_rect.dim),
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=True)

        self.__options = options
        self.__fill_options()

    def __fill_options(self):
        old_selected = self.__option_area.get_selected_index()

        self.__option_area.clear()
        for opt, enabled in self.__options:
            toggle_char = tcod.CHAR_CHECKBOX_SET if enabled \
                          else tcod.CHAR_CHECKBOX_UNSET
            self.__option_area.append(
                '{} {}'.format(chr(toggle_char), opt), wrap=False)

        self.__option_area.set_selected_index(old_selected)

    def next(self):
        self.__option_area.select_down()

    def prev(self):
        self.__option_area.select_up()

    def toggle(self, index=None):
        if index is None:
            index = self.__option_area.get_selected_index()

        self.__options[index] = (self.__options[index][0],
                                 not self.__options[index][1])
        self.__fill_options()

    def selected(self):
        return self.__options[self.__option_area.get_selected_index()]

    def set_options(self, options):
        self.__options = options
        self.__fill_options()

    def _draw_inner(self, console, cur_time):
        self.__option_area.draw(console)


class StatsMenu(FramedWindow):
    def __init__(self, player):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT / 2)), 'CHARACTER',
            Layers.MENU, colors.ui)

        self.player = player

        col_width = (self.inner_rect.dim.x / 2) - 1

        self.__stats = ScrollableList(
            rect=Rect(
                Coord(0, 0), Coord(col_width, self.inner_rect.dim.y - 1)),
            color_scheme=self.colors,
            show_selection=False,
            show_scrollbar=False,
            align=tcod.LEFT)

        self.__equipslots = ColumnList(
            columns=(('Equipment', 12, tcod.LEFT), ('Item', 0, tcod.LEFT)),
            rect=Rect(
                Coord(col_width + 1, 0), Coord(col_width,
                                               self.inner_rect.dim.y)),
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=False)

        self.refresh()

    def _draw_inner(self, console, cur_time):
        self.__stats.draw(console)
        self.__equipslots.draw(console)

        if self.player.get_component('level').can_level_up():
            console.set_bg(self.colors['alt_bg'])
            console.set_fg(self.colors['alt_text'])
            console.draw_text(
                Coord(0, self.inner_rect.dim.y - 1), 'Level up available!',
                tcod.BKGND_SET)

    def next_slot(self):
        self.__equipslots.select_down()

    def prev_slot(self):
        self.__equipslots.select_up()

    def selected_slot(self):
        return [slot
                for slot in EquipSlots][self.__equipslots.get_selected_index()]

    def __refresh_stats(self):
        self.__stats.clear()

        stats = self.player.get_component('stats')
        combat = self.player.get_component('combat')
        hp = self.player.get_component('hp')
        level = self.player.get_component('level')
        inventory = self.player.get_component('inventory')
        hunger = self.player.get_component('hunger')

        stats = [('Level', '{}'.format(level.level)),
                 ('XP', '{}/{}'.format(level.xp, level.next_level_xp())),
                 ('Dmg die', 'd{}'.format(combat.dmg_die)),
                 ('STR', '{:+d}'.format(stats.str)),
                 (' Dmg bonus', '{:+d}'.format(combat.damage_bonus)),
                 (' Carry', '{}/{}'.format(inventory.cur_weight,
                                           inventory.capacity)),
                 ('DEX', '{:+d}'.format(stats.dex)),
                 (' Atk', '{:+d}'.format(combat.attack)),
                 (' Def', '{}'.format(combat.defence)),
                 ('CON', '{:+d}'.format(stats.con)),
                 (' HP', '{}/{}'.format(hp.hp, hp.max_hp)),
                 (' Hunger', '{}/{}'.format(hunger.cur_hunger,
                                            hunger.max_hunger)),
                 ('WIL', '{:+d}'.format(stats.wil)),
                 ('PER', '{:+d}'.format(stats.per)),
                 (' Crit', '{}% x{}'.format(combat.crit_chance,
                                            combat.crit_mult)),
                 ('CHA', '{:+d}'.format(stats.cha))]

        max_width = 0
        for name, _ in stats:
            if len(name) > max_width:
                max_width = len(name)

        for index, (name, info) in enumerate(stats):
            if index % 2 != 0:
                color = self.colors['list_entry_alt_bg']
            else:
                color = self.colors['list_entry_bg']
            self.__stats.append(
                '{} {}'.format(name.ljust(max_width), info), bg_color=color)

    def __refresh_slots(self):
        self.__equipslots.clear()

        slots = self.player.get_component('equipSlots')

        for slot_id in EquipSlots:
            cur_item = slots.get_at_slot(slot_id)
            if cur_item:
                item_name = cur_item.name
            else:
                item_name = ''

            self.__equipslots.append((slot_id.name, item_name))

    def refresh(self):
        self.__refresh_stats()
        self.__refresh_slots()


class DeathsMenu(FramedWindow):
    def __init__(self, entries):
        super().__init__(
            centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT / 2)), 'CATACOMBS',
            Layers.MENU, colors.ui)

        self.__text_area = ScrollableList(
            rect=Rect(Coord(0, 0), self.inner_rect.dim),
            color_scheme=self.colors,
            show_selection=False,
            show_scrollbar=True,
            align=tcod.CENTER)

        ind = 0
        for entry in reversed(entries):
            bg_color = self.colors['list_entry_bg']
            if ind % 2 != 0:
                bg_color = self.colors['list_entry_alt_bg']

            self.__text_area.append(
                '{}: Level {} ({} XP, {} steps)\n'
                'Died {}\n'
                'COD: {}\n'
                'Deepest level: {} Explored: {}%'.format(
                    entry['name'], entry['level'], entry['xp'], entry['steps'],
                    datetime.datetime.fromtimestamp(
                        entry['death_time']).strftime('%b %d %Y %H:%M:%S'),
                    entry['cod'], entry['map_level'], entry['explore_avg']),
                bg_color=bg_color)

            ind += 1

    def _draw_inner(self, console, cur_time):
        self.__text_area.draw(console)

    def scroll_up(self):
        self.__text_area.scroll_up()

    def scroll_down(self):
        self.__text_area.scroll_down()


class ListViewMenu(FramedWindow):
    def __init__(self, entries, title, rect=None, layer=Layers.MENU):
        if rect is None:
            rect = centered_rect(
                Coord(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2),
                Coord(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT / 2))

        super().__init__(rect, title, layer, colors.ui)

        self.__entries = entries

        self.__list_area = ScrollableList(
            rect=Rect(Coord(0, 0), Coord(self.inner_rect.dim.x, 4)),
            color_scheme=self.colors,
            show_selection=True,
            show_scrollbar=True,
            align=tcod.CENTER)

        self.__desc_area = ScrollableList(
            rect=Rect(
                Coord(0, 4),
                Coord(self.inner_rect.dim.x, self.inner_rect.dim.y - 4)),
            color_scheme=self.colors,
            show_selection=False,
            show_scrollbar=True)

        for name, _ in entries:
            self.__list_area.append(name)

        self.__update_desc()

    def __update_desc(self):
        selected = self.__entries[self.__list_area.get_selected_index()]
        self.__desc_area.clear()
        self.__desc_area.append(selected[1])

    def _draw_inner(self, console, cur_time):
        self.__list_area.draw(console)
        self.__desc_area.draw(console)

    def next(self):
        self.__list_area.select_down()
        self.__update_desc()

    def prev(self):
        self.__list_area.select_up()
        self.__update_desc()

    def selected(self):
        return self.__entries[self.__list_area.get_selected_index()]

    def selected_index(self):
        return self.__list_area.get_selected_index()


class ItemListViewMenu(ListViewMenu):
    def __init__(self, ent_list, player, title, layer=Layers.MENU):
        self.__ent_list = ent_list
        ident = player.get_component('identifier')

        entries = list()
        for ent in ent_list:
            entries.append((ident.get_name(ent), ident.get_desc(ent)))

        super().__init__(entries, title=title, layer=layer)

    def selected(self):
        return self.__ent_list[self.selected_index()]
