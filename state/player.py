import copy

from state.stateman import GameStates, BaseSubStateMachine, BaseState
from render.menus import InventoryMenu, ContextHelpWindow, StatsMenu, Prompt, \
                         ChoicePrompt, ItemListViewMenu
from render.hud import TextCommandInputPrompt
from render.renderer import Layers
from components.usable import TargetTypes
from input import map_key, CommandContexts, Commands, map_text_input
from events import find_event, EventID, PlayerThinkEvent, \
                   PlayerTurnDoneEvent, DropEvent, ItemActivateEvent, \
                   EquipItemEvent, UnequipItemEvent, ItemToggleEvent, \
                   PickUpEvent
from geom import Coord, Rect, centered_rect
from message import Priority
from save import delete_save, save_death
from text_commands import run_text_command
import help_text
import constants


def register_states(mgr):
    mgr.add(GameStates.PLAYER_TURN, PlayerTurnState)
    mgr.add(GameStates.PLAYER_DEAD, DeadState)


class PlayerTurnState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN,
                         GameStates.PLAYER_TURN_INPUT)

        self.sub.add(GameStates.PLAYER_TURN_INPUT, InputState)
        self.sub.add(GameStates.PLAYER_TURN_INVENTORY, InventoryState)
        self.sub.add(GameStates.PLAYER_TURN_TARGETING, TargetingState)
        self.sub.add(GameStates.PLAYER_TURN_HELP, HelpState)
        self.sub.add(GameStates.PLAYER_TURN_CHARINFO, CharInfoState)
        self.sub.add(GameStates.PLAYER_TURN_COMMAND_INPUT, CommandInputState)
        self.sub.add(GameStates.PLAYER_TURN_INSPECT_TARGETING,
                     InspectTargetingState)
        self.sub.add(GameStates.PLAYER_TURN_INSPECT_VIEW, InspectViewState)
        self.sub.add(GameStates.PLAYER_TURN_PICKUP_LIST, ItemPickUpListState)


class InputState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INPUT)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.PLAYER)

        if command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_HELP, None)

        elif command == Commands.MESSAGES_SCROLL_UP:
            self.ctx.renderer.get_hud_messages().scroll_up()
        elif command == Commands.MESSAGES_SCROLL_DOWN:
            self.ctx.renderer.get_hud_messages().scroll_down()

        elif command == Commands.INVENTORY_OPEN:
            if len(self.ctx.player.get_component('inventory').items) > 0:
                next_state = (GameStates.PLAYER_TURN_INVENTORY, None)
            else:
                self.ctx.message_log.add('Inventory empty', Priority.LOW)

        elif command == Commands.CHARINFO_OPEN:
            next_state = (GameStates.PLAYER_TURN_CHARINFO, None)

        elif command == Commands.INSPECT:
            next_state = (GameStates.PLAYER_TURN_INSPECT_TARGETING, None)

        elif command == Commands.EXIT:
            next_state = (GameStates.GAME_MENU, None)

        elif command == Commands.ENABLE_TEXT_COMMAND:
            next_state = (GameStates.PLAYER_TURN_COMMAND_INPUT, None)

        else:
            events = []
            self.ctx.player.send(PlayerThinkEvent(self.ctx, command), events)

            state_change_ev = find_event(events, EventID.STATE_CHANGE)
            if state_change_ev:
                next_state = (state_change_ev.next_state,
                              state_change_ev.state_args)

            else:
                evt = find_event(events, EventID.PLAYER_DEAD)
                if evt:
                    next_state = (GameStates.PLAYER_DEAD, [evt.cod])

                elif find_event(events, EventID.PLAYER_TURN_DONE):
                    next_state = (GameStates.ENEMY_TURN, None)

        return next_state


class TargetingState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_TARGETING)

        self.screen_target = Coord(0, 0)

        self.current_target_index = 0
        self.target_list = []
        self.screen_target_list = []

        self.help = None

    def enter(self, last_state, item):
        self.item = item
        self.item_use = item.get_component('usable')

        self.__set_target(True)

    def exit(self):
        self.ctx.renderer.get_map_view().targeting.disable()

    def __clip_to_map(self, in_coord, radius):
        screen_coord = copy.copy(in_coord)
        screen_coord.contain(
            Rect(
                Coord(radius, radius),
                Coord(self.ctx.renderer.get_map_view().rect.dim.x - radius,
                      self.ctx.renderer.get_map_view().rect.dim.y - radius)))

        map_coord = self.ctx.renderer.get_map_view().screen_coord_to_map(
            screen_coord)

        map_coord.contain(
            Rect(
                Coord(radius, radius),
                Coord(self.ctx.world.width - radius,
                      self.ctx.world.height - radius)))

        return self.ctx.renderer.get_map_view().map_coord_to_screen(map_coord)

    def __set_target(self, first=False):
        if self.item_use.target_type in (TargetTypes.SINGLE,
                                         TargetTypes.SINGLE_EMPTY_TILE):
            if first:
                self.screen_target = self.ctx.renderer.get_map_view(
                ).map_coord_to_screen(
                    self.ctx.player.get_component('world').position)
            else:
                self.screen_target = self.__clip_to_map(self.screen_target, 0)
                self.ctx.renderer.get_map_view().targeting.set_single(
                    self.screen_target)

        elif self.item_use.target_type == TargetTypes.SQUARE:
            if first:
                self.screen_target = self.ctx.renderer.get_map_view(
                ).map_coord_to_screen(
                    self.ctx.player.get_component('world').position)
            else:
                self.screen_target = self.__clip_to_map(
                    self.screen_target, self.item_use.target_radius)
                self.ctx.renderer.get_map_view().targeting.set_square(
                    self.screen_target, self.item_use.target_radius)

        elif self.item_use.target_type == TargetTypes.LIST_IN_FOV:
            if first:
                self.target_list = self.item_use.get_targets(
                    self.ctx.player, self.ctx.message_log)
                if self.target_list != False:
                    self.screen_target_list = [
                        self.ctx.renderer.get_map_view().map_coord_to_screen(
                            ent.get_component('world').position)
                        for ent in self.target_list
                    ]

            if self.target_list:
                self.screen_target = self.screen_target_list[
                    self.current_target_index]
                self.ctx.renderer.get_map_view().targeting.set_list(
                    self.screen_target_list, self.screen_target)

        elif self.item_use.target_type == TargetTypes.LINE:
            if first:
                self.current_target_index = 0
                self.target_list = [
                    Coord(0, 1),
                    Coord(-1, 1),
                    Coord(-1, 0),
                    Coord(-1, -1),
                    Coord(0, -1),
                    Coord(1, -1),
                    Coord(1, 0),
                    Coord(1, 1)
                ]

            self.screen_target = self.ctx.renderer.get_map_view(
            ).map_coord_to_screen(
                self.ctx.player.get_component('world').position)
            self.ctx.renderer.get_map_view().targeting.set_line(
                self.screen_target +
                self.target_list[self.current_target_index], self.screen_target
                + (self.target_list[self.current_target_index] *
                   self.item_use.target_cone_distance))

    def __use_item(self):
        next_state = (self.id(), None)
        events = []

        if self.item_use.target_type == TargetTypes.LIST_IN_FOV:
            target_ents = [self.target_list[self.current_target_index]]

        elif self.item_use.target_type == TargetTypes.LINE:
            target_ents = self.item_use.get_targets(
                self.ctx.player,
                self.ctx.message_log,
                target_pos=self.ctx.renderer.get_map_view().
                screen_coord_to_map(self.screen_target),
                target_dir=self.target_list[self.current_target_index])
        else:
            target_ents = self.item_use.get_targets(
                self.ctx.player,
                self.ctx.message_log,
                target_pos=self.ctx.renderer.get_map_view().
                screen_coord_to_map(self.screen_target))

        if target_ents != False:
            if self.item_use.toggle:
                self.item.send(
                    ItemToggleEvent(
                        self, self.ctx, self.ctx.player, targets=target_ents),
                    events)
            else:
                self.item.send(
                    ItemActivateEvent(
                        self, self.ctx, self.ctx.player, targets=target_ents),
                    events)

        if find_event(events, EventID.ITEM_SUCCESS):
            self.ctx.player.send(PlayerTurnDoneEvent(self, self.ctx), events)

        evt = find_event(events, EventID.PLAYER_DEAD)
        if evt:
            next_state = (GameStates.PLAYER_DEAD, [evt.cod])

        elif find_event(events, EventID.PLAYER_TURN_DONE):
            next_state = (GameStates.ENEMY_TURN, None)

        return next_state

    def run(self, last_state):

        if self.ctx.settings.mouselook:
            self.screen_target = Coord(self.ctx.mousein.cx,
                                       self.ctx.mousein.cy)

        self.__set_target()

        next_state = (self.id(), None)

        if self.ctx.settings.mouselook:
            if self.ctx.mousein.lbutton:
                next_state = self.__use_item()

            elif self.ctx.mousein.rbutton:
                next_state = (GameStates.PLAYER_TURN_INPUT, None)

        command = map_key(self.ctx.keyin, CommandContexts.TARGETING)

        if self.item_use.target_type == TargetTypes.LIST_IN_FOV \
           and not self.target_list:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.CONFIRM:
            next_state = self.__use_item()

        elif command == Commands.NEXT_TARGET:
            if len(self.target_list) > 0:
                self.current_target_index = (
                    self.current_target_index + 1) % len(self.target_list)

        elif command == Commands.HELP_TOGGLE:
            if self.help:
                self.ctx.renderer.remove_surface(self.help)
                self.help = None
            else:
                self.help = ContextHelpWindow('TARGETING', help_text.targeting)
                self.ctx.renderer.add_surface(self.help)

        if command == Commands.MOVE_N:
            self.screen_target += Coord(0, -1)
        elif command == Commands.MOVE_S:
            self.screen_target += Coord(0, 1)
        elif command == Commands.MOVE_E:
            self.screen_target += Coord(1, 0)
        elif command == Commands.MOVE_W:
            self.screen_target += Coord(-1, 0)
        elif command == Commands.MOVE_NE:
            self.screen_target += Coord(1, -1)
        elif command == Commands.MOVE_NW:
            self.screen_target += Coord(-1, -1)
        elif command == Commands.MOVE_SE:
            self.screen_target += Coord(1, 1)
        elif command == Commands.MOVE_SW:
            self.screen_target += Coord(-1, 1)

        return next_state


class HelpState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_HELP)

    def enter(self, last_state):
        self.window = ContextHelpWindow('CONTROLS', help_text.player_turn)
        self.ctx.renderer.add_surface(self.window)

    def exit(self):
        self.ctx.renderer.remove_surface(self.window)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_HELP)

        if command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.MENU_NEXT:
            self.window.scroll_up()
        elif command == Commands.MENU_PREV:
            self.window.scroll_down()

        return next_state


class DeadState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_DEAD)

    def enter(self, last_state, cod):
        tile_total = 0
        explore_total = 0
        for cur_map in self.ctx.maps.values():
            tiles, explored = cur_map.calc_explored()
            tile_total += tiles
            explore_total += explored
        explore_avg = int(float(explore_total) / float(tile_total) * 100)

        self.prompt = Prompt(
            'YOU DIED',
            '{} ({} steps)\nCOD: {}\nDeepest level: {} Explored: {}%'.format(
                self.ctx.player.name,
                self.ctx.player.get_component('playerThink').steps, cod,
                len(self.ctx.maps) - 1, explore_avg),
            centered_rect(
                Coord(constants.SCREEN_WIDTH / 2, 5),
                Coord(constants.SCREEN_WIDTH * 0.75, 8)),
            centered=True)
        self.ctx.renderer.add_surface(self.prompt)

        save_death(self.ctx, cod)
        delete_save(self.ctx.player.name)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command in (Commands.EXIT, Commands.CONFIRM):
            next_state = (GameStates.CLEANUP, None)

        return next_state


class CharInfoState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_CHARINFO,
                         GameStates.PLAYER_TURN_CHARINFO_VIEW)

        self.sub.add(GameStates.PLAYER_TURN_CHARINFO_VIEW, CharInfoViewState)
        self.sub.add(GameStates.PLAYER_TURN_LEVEL_UP, LevelUpState)
        self.sub.add(GameStates.PLAYER_TURN_CHARINFO_HELP, CharInfoHelpState)
        self.sub.add(GameStates.PLAYER_TURN_CHARINFO_EQUIPLIST,
                     CharInfoEquipListState)

    def enter(self, last_state):
        self.ctx.menu = StatsMenu(self.ctx.player)

        self.ctx.renderer.set_menu(self.ctx.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)


class CharInfoViewState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_CHARINFO_VIEW)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_CHAR_STATS)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.MENU_NEXT:
            self.ctx.menu.next_slot()
        elif command == Commands.MENU_PREV:
            self.ctx.menu.prev_slot()

        elif command == Commands.INVENTORY_EQUIP_TOGGLE:
            cur_slot = self.ctx.menu.selected_slot()
            cur_item = self.ctx.player.get_component('equipSlots').get_at_slot(
                cur_slot)
            if cur_item:
                events = []
                self.ctx.player.send(
                    UnequipItemEvent(self, self.ctx, cur_item), events)
                if find_event(events, EventID.ITEM_UNEQUIP_SUCCESS):
                    self.ctx.player.send(
                        PlayerTurnDoneEvent(self, self.ctx), events)
                    next_state = (GameStates.ENEMY_TURN, None)
            else:

                def _equippable_filter(ent):
                    equip = ent.get_component('equipment')
                    return equip and equip.can_equip_at(cur_slot)

                equippable = [
                    item for item in filter(
                        _equippable_filter,
                        self.ctx.player.get_component('inventory').items)
                ]
                if len(equippable) > 0:
                    next_state = (GameStates.PLAYER_TURN_CHARINFO_EQUIPLIST,
                                  [equippable, cur_slot])
                else:
                    self.ctx.message_log.add(
                        'No items for {} slot'.format(cur_slot.name),
                        Priority.LOW)

        elif command == Commands.LEVEL_UP:
            if self.ctx.player.get_component('level').can_level_up():
                next_state = (GameStates.PLAYER_TURN_LEVEL_UP, None)

        elif command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_CHARINFO_HELP, None)

        return next_state


class LevelUpState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_LEVEL_UP)

    def enter(self, last_state):
        self.prompt = ChoicePrompt(
            title='LEVEL UP',
            message='Choose a stat to increase',
            choices=['STR', 'DEX', 'CON', 'WIL', 'PER', 'CHA'])
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_CHARINFO_VIEW, None)

        elif command == Commands.CONFIRM:
            option = self.prompt.selected()
            stats = self.ctx.player.get_component('stats')

            if option == 'STR':
                stats.str += 1
            elif option == 'DEX':
                stats.dex += 1
            elif option == 'CON':
                stats.con += 1
            elif option == 'WIL':
                stats.wil += 1
            elif option == 'PER':
                stats.per += 1
            elif option == 'CHA':
                stats.cha += 1

            self.ctx.message_log.add(
                '{} levels up: +1 {}'.format(self.ctx.player.name, option),
                Priority.HIGH)

            self.ctx.player.get_component('level').level_up()
            self.ctx.menu.refresh()

            next_state = (GameStates.PLAYER_TURN_CHARINFO_VIEW, None)

        elif command == Commands.MENU_NEXT:
            self.prompt.next()
        elif command == Commands.MENU_PREV:
            self.prompt.prev()

        return next_state


class CharInfoHelpState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_CHARINFO_HELP)

    def enter(self, last_state):
        self.window = ContextHelpWindow('CHARACTER', help_text.char_stats)
        self.ctx.renderer.add_surface(self.window)

    def exit(self):
        self.ctx.renderer.remove_surface(self.window)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_HELP)

        if command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_CHARINFO_VIEW, None)

        elif command == Commands.MENU_NEXT:
            self.window.scroll_up()
        elif command == Commands.MENU_PREV:
            self.window.scroll_down()

        return next_state


class CharInfoEquipListState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_CHARINFO_EQUIPLIST)

    def enter(self, last_state, ent_list, slot_id):
        self.slot_id = slot_id
        self.menu = ItemListViewMenu(
            ent_list,
            self.ctx.player,
            title='EQUIP - {}'.format(slot_id.name),
            layer=Layers.PROMPT)
        self.ctx.renderer.add_surface(self.menu)

    def exit(self):
        self.ctx.renderer.remove_surface(self.menu)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_CHARINFO_VIEW, None)

        if command == Commands.CONFIRM:
            events = []
            self.ctx.player.send(
                EquipItemEvent(
                    self, self.ctx, self.menu.selected(),
                    self.menu.selected().get_component(
                        'equipment').can_equip_at(self.slot_id)), events)
            if find_event(events, EventID.ITEM_EQUIP_SUCCESS):
                self.ctx.player.send(
                    PlayerTurnDoneEvent(self, self.ctx), events)
                next_state = (GameStates.ENEMY_TURN, None)
            else:
                self.ctx.menu.refresh()
                next_state = (GameStates.PLAYER_TURN_CHARINFO_VIEW, None)

        elif command == Commands.MENU_PREV:
            self.menu.prev()
        elif command == Commands.MENU_NEXT:
            self.menu.next()

        return next_state


class CommandInputState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_COMMAND_INPUT)
        self.prompt = TextCommandInputPrompt()

    def enter(self, last_state):
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.TEXT_COMMAND_INPUT)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.CONFIRM:
            next_state = run_text_command(self.prompt.input, self.ctx)

        else:
            self.prompt.input = map_text_input(self.ctx.keyin,
                                               self.prompt.input)

        return next_state


class InspectTargetingState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INSPECT_TARGETING)
        self.screen_target = Coord(0, 0)

    def enter(self, last_state):
        self.__set_target(True)

    def exit(self):
        self.ctx.renderer.get_map_view().targeting.disable()

    def __clip_to_map(self, in_coord):
        screen_coord = copy.copy(in_coord)
        screen_coord.contain(
            Rect(
                Coord(0, 0),
                Coord(self.ctx.renderer.get_map_view().rect.dim.x,
                      self.ctx.renderer.get_map_view().rect.dim.y)))

        map_coord = self.ctx.renderer.get_map_view().screen_coord_to_map(
            screen_coord)

        map_coord.contain(
            Rect(
                Coord(0, 0), Coord(self.ctx.world.width,
                                   self.ctx.world.height)))

        return self.ctx.renderer.get_map_view().map_coord_to_screen(map_coord)

    def __set_target(self, first=False):
        if first:
            self.screen_target = self.ctx.renderer.get_map_view(
            ).map_coord_to_screen(
                self.ctx.player.get_component('world').position)
        else:
            self.screen_target = self.__clip_to_map(self.screen_target)

        self.ctx.renderer.get_map_view().targeting.set_single(
            self.screen_target)

    def __inspect(self):
        next_state = (self.id(), None)

        map_coord = self.ctx.renderer.get_map_view().screen_coord_to_map(
            self.screen_target)
        if self.ctx.player.get_component('vision').in_fov(map_coord):
            ents = self.ctx.world.get_ents_at(
                map_coord,
                selector={'components': {
                    'world': {
                        'visible': True
                    }
                }})
            if ents:
                next_state = (GameStates.PLAYER_TURN_INSPECT_VIEW, [ents])

        return next_state

    def run(self, last_state):

        if self.ctx.settings.mouselook:
            self.screen_target = Coord(self.ctx.mousein.cx,
                                       self.ctx.mousein.cy)

        self.__set_target()

        next_state = (self.id(), None)

        if self.ctx.settings.mouselook:
            if self.ctx.mousein.lbutton:
                next_state = self.__inspect()

            elif self.ctx.mousein.rbutton:
                next_state = (GameStates.PLAYER_TURN_INPUT, None)

        command = map_key(self.ctx.keyin, CommandContexts.TARGETING)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.CONFIRM:
            next_state = self.__inspect()

        if command == Commands.MOVE_N:
            self.screen_target += Coord(0, -1)
        elif command == Commands.MOVE_S:
            self.screen_target += Coord(0, 1)
        elif command == Commands.MOVE_E:
            self.screen_target += Coord(1, 0)
        elif command == Commands.MOVE_W:
            self.screen_target += Coord(-1, 0)
        elif command == Commands.MOVE_NE:
            self.screen_target += Coord(1, -1)
        elif command == Commands.MOVE_NW:
            self.screen_target += Coord(-1, -1)
        elif command == Commands.MOVE_SE:
            self.screen_target += Coord(1, 1)
        elif command == Commands.MOVE_SW:
            self.screen_target += Coord(-1, 1)

        return next_state


class InspectViewState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INSPECT_VIEW)

    def enter(self, last_state, ent_list):
        self.menu = ItemListViewMenu(
            ent_list, self.ctx.player, title='INSPECT')
        self.ctx.renderer.set_menu(self.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.MENU_PREV:
            self.menu.prev()
        elif command == Commands.MENU_NEXT:
            self.menu.next()

        return next_state


class ItemPickUpListState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_PICKUP_LIST)

    def enter(self, last_state, ent_list):
        self.menu = ItemListViewMenu(
            ent_list, self.ctx.player, title='PICK UP')
        self.ctx.renderer.set_menu(self.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        if command == Commands.CONFIRM:
            events = []
            self.menu.selected().send(
                PickUpEvent(self, self.ctx, self.ctx.player), events)
            if find_event(events, EventID.STORE_ITEM_SUCCESS):
                self.ctx.player.send(
                    PlayerTurnDoneEvent(self, self.ctx), events)
                next_state = (GameStates.ENEMY_TURN, None)

        elif command == Commands.MENU_PREV:
            self.menu.prev()
        elif command == Commands.MENU_NEXT:
            self.menu.next()

        return next_state


class InventoryState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INVENTORY,
                         GameStates.PLAYER_TURN_INVENTORY_VIEW)

        self.sub.add(GameStates.PLAYER_TURN_INVENTORY_VIEW, InventoryViewState)
        self.sub.add(GameStates.PLAYER_TURN_INVENTORY_HELP, InventoryHelpState)

    def enter(self, last_state):
        self.ctx.menu = InventoryMenu(
            self.ctx.player,
            self.ctx.player.get_component('inventory').items)

        self.ctx.renderer.set_menu(self.ctx.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)


class InventoryViewState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INVENTORY_VIEW)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_INVENTORY)

        if command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_INVENTORY_HELP, None)

        elif command == Commands.EXIT:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)

        elif command == Commands.MENU_PREV:
            self.ctx.menu.prev()
        elif command == Commands.MENU_NEXT:
            self.ctx.menu.next()

        elif command in (Commands.INVENTORY_USE, Commands.INVENTORY_DROP,
                         Commands.INVENTORY_EQUIP_TOGGLE):
            item = self.ctx.menu.selected()
            events = []

            if command == Commands.INVENTORY_DROP:
                slot = self.ctx.player.get_component('equipSlots')
                if slot.has_equipped(item):
                    self.ctx.player.send(
                        UnequipItemEvent(self, self.ctx, item), events)
                    if find_event(events, EventID.ITEM_UNEQUIP_SUCCESS):
                        item.send(
                            DropEvent(self, self.ctx, self.ctx.player), events)
                        self.ctx.player.send(
                            PlayerTurnDoneEvent(self, self.ctx), events)
                else:
                    item.send(
                        DropEvent(self, self.ctx, self.ctx.player), events)
                    self.ctx.player.send(
                        PlayerTurnDoneEvent(self, self.ctx), events)

            elif command == Commands.INVENTORY_USE:
                usable = item.get_component('usable')
                equip = item.get_component('equipment')
                if usable:
                    if usable.requires_equip and equip \
                       and equip.equipped_by is not self.ctx.player:
                        self.ctx.message_log.add('Must be equipped first',
                                                 Priority.LOW)
                    elif usable.target_type == TargetTypes.SELF:
                        targets = usable.get_targets(self.ctx.player,
                                                     self.ctx.message_log)
                        if targets:
                            if usable.toggle:
                                item.send(
                                    ItemToggleEvent(
                                        self,
                                        self.ctx,
                                        self.ctx.player,
                                        targets=targets), events)
                            else:
                                item.send(
                                    ItemActivateEvent(
                                        self,
                                        self.ctx,
                                        self.ctx.player,
                                        targets=targets), events)
                            if find_event(events, EventID.ITEM_SUCCESS):
                                self.ctx.player.send(
                                    PlayerTurnDoneEvent(self, self.ctx),
                                    events)
                    else:
                        next_state = (GameStates.PLAYER_TURN_TARGETING, [item])

            elif command == Commands.INVENTORY_EQUIP_TOGGLE:
                equipment = item.get_component('equipment')
                slot = self.ctx.player.get_component('equipSlots')
                if equipment:
                    if not equipment.equipped_by is self.ctx.player:
                        self.ctx.player.send(
                            EquipItemEvent(self, self.ctx, item,
                                           equipment.slots[0]), events)
                        if find_event(events, EventID.ITEM_EQUIP_SUCCESS):
                            self.ctx.player.send(
                                PlayerTurnDoneEvent(self, self.ctx), events)
                    else:
                        self.ctx.player.send(
                            UnequipItemEvent(self, self.ctx, item), events)
                        if find_event(events, EventID.ITEM_UNEQUIP_SUCCESS):
                            self.ctx.player.send(
                                PlayerTurnDoneEvent(self, self.ctx), events)

            evt = find_event(events, EventID.PLAYER_DEAD)
            if evt:
                next_state = (GameStates.PLAYER_DEAD, [evt.cod])
            elif find_event(events, EventID.PLAYER_TURN_DONE):
                next_state = (GameStates.ENEMY_TURN, None)

        return next_state


class InventoryHelpState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.PLAYER_TURN_INVENTORY_HELP)

    def enter(self, last_state):
        self.window = ContextHelpWindow('INVENTORY', help_text.inventory_menu)
        self.ctx.renderer.add_surface(self.window)

    def exit(self):
        self.ctx.renderer.remove_surface(self.window)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_HELP)

        if command == Commands.HELP_TOGGLE:
            next_state = (GameStates.PLAYER_TURN_INVENTORY_VIEW, None)

        elif command == Commands.MENU_NEXT:
            self.window.scroll_up()
        elif command == Commands.MENU_PREV:
            self.window.scroll_down()

        return next_state
