from state.stateman import BaseState
from render.menus import TextInputPrompt, Prompt
from input import map_key, map_text_input, CommandContexts, Commands


class InputPromptState(BaseState):
    def __init__(self,
                 ctx,
                 state_id,
                 cancel_state,
                 confirm_state,
                 title,
                 prompt,
                 input_text=''):
        super().__init__(ctx, state_id)

        self.title_text = title
        self.prompt_text = prompt
        self.input_text = input_text
        self.cancel_state = cancel_state
        self.confirm_state = confirm_state

    def enter(self, last_state):
        self.prompt = TextInputPrompt(self.title_text, self.prompt_text)
        self.prompt.input = self.input_text
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def _validate(self, input_text):
        if not input_text:
            return (self.id(), 'Must not be blank')

        return True

    def _confirm(self, input_text):
        return (self.confirm_state, [input_text])

    def _cancel(self, _):
        return (self.cancel_state, None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.EXIT:
            next_state = self._cancel(self.prompt.input)

        elif command == Commands.CONFIRM:
            result = self._validate(self.prompt.input)
            if result == True:
                next_state = self._confirm(self.prompt.input)
            elif result[0] == self.id():
                self.prompt.error = result[1]
            else:
                next_state = (result[0], result[1])

        else:
            new_str = map_text_input(self.ctx.keyin, self.prompt.input)
            if new_str != self.prompt.input:
                self.prompt.error = ''
                self.prompt.input = new_str

        return next_state


class ConfirmPromptState(BaseState):
    def __init__(self, ctx, state_id, cancel_state, confirm_state, title,
                 prompt):
        super().__init__(ctx, state_id)

        self.title_text = title
        self.prompt_text = prompt
        self.cancel_state = cancel_state
        self.confirm_state = confirm_state

    def enter(self, last_state):
        self.prompt = Prompt(self.title_text, self.prompt_text)
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def _confirm(self):
        return (self.confirm_state, None)

    def _cancel(self):
        return (self.cancel_state, None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.PROMPT)

        if command == Commands.PROMPT_CONFIRM:
            next_state = self._confirm()

        elif command == Commands.PROMPT_DENY:
            next_state = self._cancel()

        return next_state
