"""
State management
"""

from enum import Enum, auto
import constants


def _debug_print(msg):
    if constants.DEBUG_STATE_MACHINE:
        print(msg)


class GameStates(Enum):
    EXIT = auto()

    INIT = auto()
    LOAD_SAVED = auto()
    CLEANUP = auto()
    MAP_CHANGE = auto()
    LORE_TRANSITION = auto()

    PLAYER_TURN = auto()
    ENEMY_TURN = auto()
    PLAYER_DEAD = auto()

    PLAYER_TURN_INPUT = auto()
    PLAYER_TURN_INVENTORY = auto()
    PLAYER_TURN_TARGETING = auto()
    PLAYER_TURN_HELP = auto()
    PLAYER_TURN_CHARINFO = auto()

    PLAYER_TURN_INVENTORY_VIEW = auto()
    PLAYER_TURN_INVENTORY_HELP = auto()

    PLAYER_TURN_CHARINFO_VIEW = auto()
    PLAYER_TURN_LEVEL_UP = auto()
    PLAYER_TURN_CHARINFO_HELP = auto()
    PLAYER_TURN_CHARINFO_EQUIPLIST = auto()

    PLAYER_TURN_COMMAND_INPUT = auto()

    PLAYER_TURN_INSPECT_TARGETING = auto()
    PLAYER_TURN_INSPECT_VIEW = auto()

    PLAYER_TURN_PICKUP_LIST = auto()

    MAIN_MENU = auto()

    MAIN_MENU_ROOT = auto()
    MAIN_MENU_NEW_NAME_PROMPT = auto()
    MAIN_MENU_NEW_CONFIRM_OVERWRITE = auto()
    MAIN_MENU_LOAD = auto()
    MAIN_MENU_LOAD_ROOT = auto()
    MAIN_MENU_LOAD_CONFIRM_DELETE = auto()
    MAIN_MENU_DEATHS = auto()
    MAIN_MENU_INTRO = auto()

    GAME_MENU = auto()

    GAME_MENU_ROOT = auto()
    GAME_MENU_SETTINGS = auto()


class BaseState:
    def __init__(self, ctx, in_sid):
        self.ctx = ctx
        self.sid = in_sid
        self.sub_state = None

    def run(self, last_state):
        return (self.sid, None)

    def enter(self, last_state):
        pass

    def exit(self):
        pass

    def id(self):
        return self.sid


class BaseSubStateMachine(BaseState):
    def __init__(self, ctx, this_sid, init_id):
        super().__init__(ctx, this_sid)
        self.sub = StateMachine(init_id, ctx)
        self.sub.debug_id = str(this_sid)

    def run(self, last_state):
        next_id, args = self.sub.run()
        if not self.sub.has_state(next_id):
            _debug_print('State {} not in sub state machine {}'.format(
                next_id, self.id()))
            return (next_id, args)

        return (self.id(), None)


class StateMachine:
    def __init__(self, init_id, ctx):
        self.__state_fns = dict()
        self.__cur_state = None
        self.__last_state_id = None
        self.__last_state_args = None
        self.__init_state_id = init_id
        self.__ctx = ctx

        self.debug_id = 'root'

    def add(self, sid, fn):
        self.__state_fns[sid] = fn

    def has_state(self, sid):
        return sid in self.__state_fns.keys()

    def get_last(self):
        return self.__last_state_id

    def run(self):

        if not self.__cur_state:
            self.__cur_state = self.__state_fns[self.__init_state_id](
                self.__ctx)

        if self.__cur_state.id() != self.__last_state_id:
            _debug_print('{}: Entering state: {}'.format(
                self.debug_id, self.__cur_state.id()))
            if self.__last_state_args:
                self.__cur_state.enter(self.__last_state_id,
                                       *self.__last_state_args)
            else:
                self.__cur_state.enter(self.__last_state_id)

        next_id, args = self.__cur_state.run(self.__last_state_id)
        self.__last_state_id = self.__cur_state.id()

        if next_id != self.__cur_state.id():
            _debug_print('{}: Changing state {} to {}'.format(
                self.debug_id, self.__cur_state.id(), next_id))
            self.__cur_state.exit()
            self.__last_state_args = args

            if self.has_state(next_id):
                self.__cur_state = self.__state_fns[next_id](self.__ctx)
            else:
                _debug_print('{}: No state {}'.format(self.debug_id, next_id))

        return (next_id, args)
