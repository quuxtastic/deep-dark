from state.stateman import BaseState, GameStates
from events import find_event, EventID, MonsterThinkEvent


def register_states(mgr):
    mgr.add(GameStates.ENEMY_TURN, EnemyTurnState)


class EnemyTurnState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.ENEMY_TURN)

    def run(self, last_state):
        next_state = (GameStates.PLAYER_TURN, None)

        events = []

        for ent in self.ctx.world.get_all_ents():
            ent.send(MonsterThinkEvent(self.ctx), events)

        evt = find_event(events, EventID.PLAYER_DEAD)
        if evt:
            next_state = (GameStates.PLAYER_DEAD, [evt.cod])

        return next_state
