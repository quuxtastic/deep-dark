"""
Game setup/cleanup states
"""

import time

from state.stateman import GameStates, BaseState
from events import SpawnEvent, DespawnEvent, PickUpEvent, EquipItemEvent, \
                   GainXpEvent
from save import load_from_save
from worldgen.levels import get_generator_for_level
from render.map_view import MapView
from render.hud import HUD, Messages
from lore import lore_available


def register_states(mgr):
    mgr.add(GameStates.INIT, InitState)
    mgr.add(GameStates.CLEANUP, CleanupState)
    mgr.add(GameStates.LOAD_SAVED, LoadSaveState)
    mgr.add(GameStates.MAP_CHANGE, MapChangeState)


class InitState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.INIT)

    def enter(self, last_state, player_name):
        self.player_name = player_name

    def run(self, last_state):
        if 0 not in self.ctx.lore_levels_shown \
           and lore_available(0):
            return (GameStates.LORE_TRANSITION,
                    [0, (GameStates.INIT, [self.player_name])])

        self.ctx.world_seed = time.time()
        self.ctx.rand.seed(self.ctx.world_seed)

        for index in range(100):
            self.ctx.map_seeds[index] = self.ctx.rand.randint(1, 100000000)

        self.ctx.entity_factory.initialize()

        self.ctx.rand.seed(self.ctx.map_seeds[0])
        self.ctx.map_index = 0
        new_map = get_generator_for_level(0)(self.ctx.map_index, self.ctx)

        self.ctx.maps[self.ctx.map_index] = new_map
        self.ctx.world = new_map

        player_ent = new_map.create(
            'player', [],
            new_map.player_spawn_pos,
            overrides={'entity': {
                'name': self.player_name
            }})
        self.ctx.player = player_ent

        self.ctx.renderer.set_map_view(MapView(self.ctx))
        self.ctx.renderer.set_hud_bars(HUD(self.ctx))
        self.ctx.renderer.set_hud_messages(Messages(self.ctx))

        self.ctx.rand.seed(time.time())

        initial_equipment = ['lantern', 'dagger']
        for tmpl in initial_equipment:
            item = self.ctx.entity_factory.create(tmpl)
            equipment = item.get_component('equipment')
            item.send(PickUpEvent(None, self.ctx, player_ent), [])
            player_ent.send(
                EquipItemEvent(None, self.ctx, item, equipment.slots[0]), [])

        initial_items = ['food', 'oil_flask']
        for tmpl in initial_items:
            item = self.ctx.entity_factory.create(tmpl)
            item.send(PickUpEvent(None, self.ctx, player_ent), [])

        return (GameStates.PLAYER_TURN, None)


class LoadSaveState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.LOAD_SAVED)

    def enter(self, last_state, save_name):
        self.save_name = save_name

    def run(self, last_state):
        load_from_save(self.save_name, self.ctx)
        self.ctx.world = self.ctx.maps[self.ctx.map_index]
        self.ctx.player = self.ctx.world.player

        self.ctx.renderer.set_map_view(MapView(self.ctx))
        self.ctx.renderer.set_hud_bars(HUD(self.ctx))
        self.ctx.renderer.set_hud_messages(Messages(self.ctx))

        self.ctx.rand.seed(time.time())

        return (GameStates.PLAYER_TURN, None)


class CleanupState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.CLEANUP)

    def run(self, last_state):
        self.ctx.world = None
        self.ctx.maps.clear()
        self.ctx.map_index = -1
        self.ctx.world_seed = 0
        self.ctx.map_seeds = dict()

        self.ctx.player = None
        self.ctx.player_dead = False

        self.ctx.message_log.clear()
        self.ctx.entity_factory.reset_all()

        self.ctx.lore_levels_shown = set()

        self.ctx.renderer.set_map_view(None)
        self.ctx.renderer.set_hud_bars(None)
        self.ctx.renderer.set_hud_messages(None)

        return (GameStates.MAIN_MENU, None)


class MapChangeState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAP_CHANGE)

    def enter(self, last_state, dest_level):
        self.dest_level = dest_level

    def run(self, last_state):
        if self.dest_level not in self.ctx.lore_levels_shown \
           and lore_available(self.dest_level):
            return (GameStates.LORE_TRANSITION, [
                self.dest_level, (GameStates.MAP_CHANGE, [self.dest_level])
            ])

        new_level = self.ctx.maps.get(self.dest_level)
        if new_level is None:
            self.ctx.rand.seed(self.ctx.map_seeds[self.dest_level])
            generator = get_generator_for_level(self.dest_level)
            new_level = generator(self.dest_level, self.ctx)
            self.ctx.maps[self.dest_level] = new_level

            self.ctx.player.send(
                GainXpEvent(None, self.ctx, 2 * self.dest_level, 'new level'),
                [])

        if self.dest_level > self.ctx.map_index:
            new_pos = new_level.stairs_up_pos
        else:
            new_pos = new_level.stairs_down_pos

        self.ctx.player.send(DespawnEvent(None, self.ctx, self.ctx.world), [])
        self.ctx.player.send(
            SpawnEvent(None, self.ctx, new_level, new_pos), [])

        self.ctx.map_index = self.dest_level
        self.ctx.world = new_level

        self.ctx.rand.seed(time.time())

        return (GameStates.PLAYER_TURN, None)
