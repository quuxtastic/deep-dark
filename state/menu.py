import tcod

from state.stateman import BaseState, GameStates, BaseSubStateMachine
from state.helpers import InputPromptState, ConfirmPromptState
from render.menus import OptionMenu, ToggleSettingsMenu, DeathsMenu, \
                         LoreTransitionWindow
from input import map_key, CommandContexts, Commands
from save import save_game, read_saves, delete_save, read_deaths
from lore import get_lore_for_level, get_lore_intro


def register_states(mgr):
    mgr.add(GameStates.MAIN_MENU, MainMenuState)
    mgr.add(GameStates.GAME_MENU, GameMenuState)
    mgr.add(GameStates.LORE_TRANSITION, LoreTransitionState)


class MainMenuState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU, GameStates.MAIN_MENU_ROOT)

        self.sub.add(GameStates.MAIN_MENU_ROOT, MainRootState)
        self.sub.add(GameStates.MAIN_MENU_NEW_NAME_PROMPT, MainNewGameState)
        self.sub.add(GameStates.MAIN_MENU_NEW_CONFIRM_OVERWRITE,
                     ConfirmOverwriteState)
        self.sub.add(GameStates.MAIN_MENU_LOAD, MainLoadMenuState)
        self.sub.add(GameStates.MAIN_MENU_DEATHS, MainViewDeathsState)
        self.sub.add(GameStates.MAIN_MENU_INTRO, MainIntroState)

    def exit(self):
        self.ctx.renderer.set_menu(None)


class MainRootState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU_ROOT)

    def enter(self, last_state):

        options = ['New Game']
        if read_saves():
            options.append('Load Game')
        if read_deaths():
            options.append('Catacombs')
        options.append('Quit')

        self.menu = OptionMenu(options, align=tcod.CENTER, title='DEEP DARK')
        self.ctx.renderer.set_menu(self.menu)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.menu.next()
        elif command == Commands.MENU_PREV:
            self.menu.prev()

        elif command == Commands.CONFIRM:
            option = self.menu.selected()

            if option == 'Quit':
                next_state = (GameStates.EXIT, None)
            elif option == 'New Game':
                next_state = (GameStates.MAIN_MENU_INTRO, None)
            elif option == 'Load Game':
                next_state = (GameStates.MAIN_MENU_LOAD, None)
            elif option == 'Catacombs':
                next_state = (GameStates.MAIN_MENU_DEATHS, None)

        return next_state


class MainIntroState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU_INTRO)

    def enter(self, last_state):
        self.prompt = LoreTransitionWindow(
            title='DEEP DARK', text=get_lore_intro())
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.prompt.scroll_up()
        elif command == Commands.MENU_PREV:
            self.prompt.scroll_down()

        elif command in (Commands.EXIT, Commands.CONFIRM):
            next_state = (GameStates.MAIN_MENU_NEW_NAME_PROMPT, None)

        return next_state


class MainNewGameState(InputPromptState):
    def __init__(self, ctx):
        super().__init__(
            ctx,
            GameStates.MAIN_MENU_NEW_NAME_PROMPT,
            cancel_state=GameStates.MAIN_MENU_ROOT,
            confirm_state=GameStates.INIT,
            title='NEW GAME',
            prompt='Enter your name:')

    def _validate(self, input_text):
        res = super()._validate(input_text)
        if res != True:
            return res

        if input_text in read_saves():
            return (GameStates.MAIN_MENU_NEW_CONFIRM_OVERWRITE, [input_text])

        return True


class ConfirmOverwriteState(ConfirmPromptState):
    def __init__(self, ctx):
        super().__init__(
            ctx,
            GameStates.MAIN_MENU_NEW_CONFIRM_OVERWRITE,
            cancel_state=GameStates.MAIN_MENU_ROOT,
            confirm_state=GameStates.INIT,
            title='OVERWRITE SAVE',
            prompt=None)

    def enter(self, last_state, save_name):
        self.save_name = save_name
        self.prompt_text = "Overwrite save '{}'?".format(save_name)
        super().enter(last_state)

    def _confirm(self):
        return (GameStates.INIT, [self.save_name])


class MainViewDeathsState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU_DEATHS)

    def enter(self, last_state):
        self.menu = DeathsMenu(read_deaths())
        self.ctx.renderer.set_menu(self.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.menu.scroll_down()
        elif command == Commands.MENU_PREV:
            self.menu.scroll_up()

        elif command == Commands.EXIT:
            next_state = (GameStates.MAIN_MENU_ROOT, None)

        return next_state


class MainLoadMenuState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU_LOAD,
                         GameStates.MAIN_MENU_LOAD_ROOT)

        self.sub.add(GameStates.MAIN_MENU_LOAD_ROOT, MainLoadMenuRootState)
        self.sub.add(GameStates.MAIN_MENU_LOAD_CONFIRM_DELETE,
                     ConfirmDeleteState)


class MainLoadMenuRootState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.MAIN_MENU_LOAD_ROOT)

    def enter(self, last_state):
        self.ctx.menu = OptionMenu(read_saves(), 'LOAD GAME')
        self.ctx.renderer.set_menu(self.ctx.menu)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.ctx.menu.next()
        elif command == Commands.MENU_PREV:
            self.ctx.menu.prev()

        elif command == Commands.EXIT:
            next_state = (GameStates.MAIN_MENU_ROOT, None)

        elif command == Commands.CONFIRM:
            next_state = (GameStates.LOAD_SAVED, [self.ctx.menu.selected()])

        elif command == Commands.DELETE_SAVE:
            next_state = (GameStates.MAIN_MENU_LOAD_CONFIRM_DELETE,
                          [self.ctx.menu.selected()])

        return next_state


class ConfirmDeleteState(ConfirmPromptState):
    def __init__(self, ctx):
        super().__init__(
            ctx,
            GameStates.MAIN_MENU_LOAD_CONFIRM_DELETE,
            cancel_state=GameStates.MAIN_MENU_LOAD_ROOT,
            confirm_state=GameStates.MAIN_MENU_ROOT,
            title='DELETE SAVE',
            prompt=None)

    def enter(self, last_state, save_name):
        self.save_name = save_name
        self.prompt_text = "Delete save '{}'?".format(save_name)
        super().enter(last_state)

    def _confirm(self):
        delete_save(self.save_name)
        if not read_saves():
            return (GameStates.MAIN_MENU_ROOT, None)

        return (GameStates.MAIN_MENU_LOAD_ROOT, None)


class GameMenuState(BaseSubStateMachine):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.GAME_MENU, GameStates.GAME_MENU_ROOT)

        self.sub.add(GameStates.GAME_MENU_ROOT, GameMenuRootState)
        self.sub.add(GameStates.GAME_MENU_SETTINGS, GameMenuSettingsState)


class GameMenuRootState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.GAME_MENU_ROOT)

    def enter(self, last_state):
        self.ctx.menu = OptionMenu(
            ['Back to Game', 'Settings', 'Save and Exit'], align=tcod.CENTER)
        self.ctx.renderer.set_menu(self.ctx.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.ctx.menu.next()
        elif command == Commands.MENU_PREV:
            self.ctx.menu.prev()

        elif command == Commands.CONFIRM:
            option = self.ctx.menu.selected()

            if option == 'Save and Exit':
                save_game(self.ctx.player.name, self.ctx)
                next_state = (GameStates.CLEANUP, None)
            elif option == 'Back to Game':
                next_state = (GameStates.PLAYER_TURN, None)
            elif option == 'Settings':
                next_state = (GameStates.GAME_MENU_SETTINGS, None)

        return next_state


class GameMenuSettingsState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.GAME_MENU_SETTINGS)

    def enter(self, last_state):
        self.menu = ToggleSettingsMenu(
            [('Auto-Eat', self.ctx.settings.auto_eat),
             ('Auto-Pickup', self.ctx.settings.auto_pickup),
             ('Auto-Fuel', self.ctx.settings.auto_fuel),
             ('Mouse-Look', self.ctx.settings.mouselook)], 'SETTINGS')
        self.ctx.renderer.set_menu(self.menu)

    def exit(self):
        self.ctx.renderer.set_menu(None)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.menu.next()
        elif command == Commands.MENU_PREV:
            self.menu.prev()

        elif command == Commands.EXIT:
            next_state = (GameStates.GAME_MENU_ROOT, None)

        elif command == Commands.CONFIRM:
            self.menu.toggle()
            sel = self.menu.selected()
            if sel[0] == 'Auto-Eat':
                self.ctx.settings.auto_eat = sel[1]
            elif sel[0] == 'Auto-Pickup':
                self.ctx.settings.auto_pickup = sel[1]
            elif sel[0] == 'Auto-Fuel':
                self.ctx.settings.auto_fuel = sel[1]
            elif sel[0] == 'Mouse-Look':
                self.ctx.settings.mouselook = sel[1]

        return next_state


class LoreTransitionState(BaseState):
    def __init__(self, ctx):
        super().__init__(ctx, GameStates.LORE_TRANSITION)

    def enter(self, last_state, dest_level, next_state_info):
        self.dest_level = dest_level
        self.next_state_info = next_state_info

        levels, title, lore = get_lore_for_level(dest_level)
        self.ctx.lore_levels_shown.update(levels)

        self.prompt = LoreTransitionWindow(title=title, text=lore)
        self.ctx.renderer.add_surface(self.prompt)

    def exit(self):
        self.ctx.renderer.remove_surface(self.prompt)

    def run(self, last_state):
        next_state = (self.id(), None)

        command = map_key(self.ctx.keyin, CommandContexts.MENU_MAIN)

        if command == Commands.MENU_NEXT:
            self.prompt.scroll_up()
        elif command == Commands.MENU_PREV:
            self.prompt.scroll_down()

        elif command in (Commands.EXIT, Commands.CONFIRM):
            next_state = self.next_state_info

        return next_state
