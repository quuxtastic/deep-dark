"""
Player actions
"""

from component import BaseComponent, component
from components.equipSlots import EquipSlots
from components.world import MoveBlockReasons
from events import EventID, PlayerTurnDoneEvent, AttackEvent, PickUpEvent, \
                   InteractEvent, ExhaustEvent, ItemActivateEvent, \
                   PlayerDeadEvent, PlayerWaitEvent, ItemToggleEvent, \
                   MoveEvent, find_event, StateChangeEvent
from state.stateman import GameStates
from input import Commands
from geom import Coord
from message import Priority

_MOVE_MAP = {
    Commands.MOVE_N: Coord(0, -1),
    Commands.MOVE_E: Coord(1, 0),
    Commands.MOVE_S: Coord(0, 1),
    Commands.MOVE_W: Coord(-1, 0),
    Commands.MOVE_NE: Coord(1, -1),
    Commands.MOVE_NW: Coord(-1, -1),
    Commands.MOVE_SE: Coord(1, 1),
    Commands.MOVE_SW: Coord(-1, 1)
}
_INTERACT_MAP = {
    Commands.INTERACT_N: Coord(0, -1),
    Commands.INTERACT_E: Coord(1, 0),
    Commands.INTERACT_S: Coord(0, 1),
    Commands.INTERACT_W: Coord(-1, 0),
    Commands.INTERACT_NE: Coord(1, -1),
    Commands.INTERACT_NW: Coord(-1, -1),
    Commands.INTERACT_SE: Coord(1, 1),
    Commands.INTERACT_SW: Coord(-1, 1)
}
_ATTACK_MAP = {
    Commands.ATTACK_N: Coord(0, -1),
    Commands.ATTACK_E: Coord(1, 0),
    Commands.ATTACK_S: Coord(0, 1),
    Commands.ATTACK_W: Coord(-1, 0),
    Commands.ATTACK_NE: Coord(1, -1),
    Commands.ATTACK_NW: Coord(-1, -1),
    Commands.ATTACK_SE: Coord(1, 1),
    Commands.ATTACK_SW: Coord(-1, 1)
}


@component('playerThink')
class PlayerThinkComponent(BaseComponent):
    """
    Processes input commands
    """

    def __init__(self, owner):
        super().__init__(owner)

        self.steps = 0

        self._bind_event(EventID.PLAYER_THINK, self._think)
        self._bind_event(EventID.SPAWN, self._spawn)
        self._bind_event(EventID.DESPAWN, self._despawn)
        self._bind_event(EventID.KILLED, self._killed)
        self._bind_event(EventID.PLAYER_TURN_DONE, self._turn_done)

    def _killed(self, event, chain):
        chain.append(PlayerDeadEvent(self, EventID.PLAYER_DEAD, event.cod))
        event.ctx.player_dead = True

        return True

    def __interact_think(self, event, chain, world_comp):
        interact_dir = _INTERACT_MAP.get(event.command)
        if interact_dir is None:
            return

        ents = world_comp.world.get_ents_at(
            world_comp.position + interact_dir,
            {'components': {
                'interactable': None
            }})
        if ents:
            ents[0].send(InteractEvent(self, event.ctx, self.owner), chain)
            self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)
        else:
            event.ctx.message_log.add('Nothing to interact with', Priority.LOW)

    def __attack_think(self, event, chain, world_comp):
        attack_dir = _ATTACK_MAP.get(event.command)
        if attack_dir is None:
            return

        ents = world_comp.world.get_ents_at(world_comp.position + attack_dir,
                                            {'components': {
                                                'combat': None
                                            }})
        if ents:
            self.owner.send(AttackEvent(self, event.ctx, ents[0]), chain)
            self.owner.send(ExhaustEvent(self, event.ctx, 1), chain)
            self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)

    def __move_think(self, event, chain, world_comp):
        move_dir = _MOVE_MAP.get(event.command)
        if move_dir is None:
            return

        self.owner.send(MoveEvent(self, event.ctx, move_dir), chain)
        blocked = find_event(chain, EventID.MOVE_BLOCKED)
        if not blocked:
            # pick up whatever is here if auto-pickup is enabled
            if event.ctx.settings.auto_pickup:
                ents = world_comp.world.get_ents_at(
                    world_comp.position, {'components': {
                        'item': None
                    }})
                if ents:
                    ents[0].send(
                        PickUpEvent(self, event.ctx, self.owner), chain)

            self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)
        else:
            if blocked.reason == MoveBlockReasons.TILE_DIG:
                progress = world_comp.world.dig(world_comp.position + move_dir,
                                                10)
                if progress == 0:
                    event.ctx.message_log.add('You have opened a new passage',
                                              Priority.HIGH)
                else:
                    event.ctx.message_log.add(
                        'You dig away at the stone, but it\'s slow going',
                        Priority.LOW)
                self.owner.send(ExhaustEvent(self, event.ctx, 3), chain)
                self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)

            elif blocked.reason == MoveBlockReasons.TILE:
                event.ctx.message_log.add('The stone is unyielding',
                                          Priority.LOW)

            elif blocked.reason == MoveBlockReasons.ENTITY:
                ent = blocked.blockers[0]

                if ent.get_component('interactable'):
                    ent.send(InteractEvent(self, event.ctx, self.owner), chain)
                    self.owner.send(
                        PlayerTurnDoneEvent(self, event.ctx), chain)

                elif ent.get_component('combat'):
                    self.owner.send(AttackEvent(self, event.ctx, ent), chain)
                    self.owner.send(ExhaustEvent(self, event.ctx, 1), chain)
                    self.owner.send(
                        PlayerTurnDoneEvent(self, event.ctx), chain)

                else:
                    event.ctx.message_log.add('{} is in your way'.format(
                        ent.name))

            elif blocked.reason == MoveBlockReasons.STUCK:
                ent = blocked.blockers[0]
                event.ctx.message_log.add('{} stops you'.format(ent.name))

    def _think(self, event, chain):
        if event.ctx.player_dead:
            return True

        world_comp = self.owner.get_component('world')
        vision = self.owner.get_component('vision')

        # compute the player FOV once, and mark explored tiles
        vision.compute_fov(world_comp.position, set_explored=True)

        if event.command == Commands.WAIT:
            self.owner.send(PlayerWaitEvent(self, event.ctx), chain)
            self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)

        elif event.command == Commands.INTERACT:
            ents = world_comp.world.get_ents_at(
                world_comp.position, {'components': {
                    'interactable': None
                }})
            if not ents:
                event.ctx.message_log.add('Nothing to interact with here',
                                          Priority.LOW)
            else:
                ents[0].send(InteractEvent(self, event.ctx, self.owner), chain)
                self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)

        elif event.command == Commands.PICK_UP:
            ents = world_comp.world.get_ents_at(world_comp.position,
                                                {'components': {
                                                    'item': None
                                                }})
            if not ents:
                event.ctx.message_log.add('Nothing to pick up here',
                                          Priority.LOW)
            elif len(ents) == 1:
                ents[0].send(PickUpEvent(self, event.ctx, self.owner), chain)
                if find_event(chain, EventID.ITEM_SUCCESS):
                    self.owner.send(
                        PlayerTurnDoneEvent(self, event.ctx), chain)
            else:
                chain.append(
                    StateChangeEvent(self, event.ctx,
                                     GameStates.PLAYER_TURN_PICKUP_LIST,
                                     [ents]))

        elif event.command == Commands.LANTERN_TOGGLE:
            cur_lantern = self.owner.get_component('equipSlots').get_at_slot(
                EquipSlots.LIGHT_SOURCE)
            if cur_lantern:
                cur_lantern.send(
                    ItemToggleEvent(
                        self,
                        event.ctx,
                        event.ctx.player,
                        targets=[event.ctx.player]), chain)
                self.owner.send(PlayerTurnDoneEvent(self, event.ctx), chain)
            else:
                event.ctx.message_log.add('No lantern equipped', Priority.LOW)

        else:
            self.__move_think(event, chain, world_comp)
            self.__interact_think(event, chain, world_comp)
            self.__attack_think(event, chain, world_comp)

        return True

    def _spawn(self, event, _):
        event.world.player = self.owner
        return True

    def _despawn(self, event, _):
        event.world.player = None
        return True

    def _turn_done(self, event, chain):
        self.steps += 1
        # every turn costs 1 hunger
        self.owner.send(ExhaustEvent(self, event.ctx, 1), chain)

        ident = self.owner.get_component('identifier')

        def _is_identified(item):
            if not item.get_component('identifiable'):
                return True

            return ident.is_identified(item)

        if event.ctx.settings.auto_eat:
            hunger = self.owner.get_component('hunger')
            inventory = self.owner.get_component('inventory')

            for item in inventory.items:
                food = item.get_component('food')
                if food and food.nutrition <= hunger.cur_hunger \
                   and _is_identified(item):
                    item.send(
                        ItemActivateEvent(self, event.ctx, self.owner,
                                          [self.owner]), chain)
                    break

        if event.ctx.settings.auto_fuel:
            tank = self.owner.get_component('fuelTank')
            inventory = self.owner.get_component('inventory')

            for item in inventory.items:
                fuel = item.get_component('fuel')
                if fuel and fuel.amount <= (tank.capacity - tank.current) \
                   and _is_identified(item):
                    item.send(
                        ItemActivateEvent(self, event.ctx, self.owner,
                                          [self.owner]), chain)
                    break

        return True

    def serialize(self):
        return {'steps': self.steps}

    def deserialize(self, data):
        self.steps = data['steps']
