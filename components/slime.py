"""
Slime AI
"""

from component import BaseComponent, component
from events import EventID
from geom import Coord, centered_rect
from components.monsterThink import ThinkStates


@component('slime', before=['monsterThink'])
class SlimeComponent(BaseComponent):
    """
    Slime monsters have a chance of splitting instead of doing something
    else each turn. Splits halve current/max HP, and cannot happen at 1 HP
    """

    def __init__(self, owner, split_chance=15, spawn_template=None):
        super().__init__(owner)

        self.split_chance = split_chance
        if not spawn_template:
            spawn_template = self.owner.template
        self.spawn_template = spawn_template

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _think(self, event, chain):
        hp = self.owner.get_component('hp')
        world = self.owner.get_component('world')
        think = self.owner.get_component('monsterThink')

        def _calc_spawn_pos():
            for pos in centered_rect(world.position, Coord(3, 3)).coords():
                if world.world.is_passable(pos):
                    return pos

            return None

        if hp.hp > 1 \
           and think.state in (ThinkStates.ATTACK, ThinkStates.HUNT) \
           and event.ctx.rand.perc(self.split_chance):
            spawn_pos = _calc_spawn_pos()
            if spawn_pos:

                new_hp = int(hp.hp / 2)
                new_max_hp = int(hp.base_max_hp / 2)

                hp.hp = new_hp
                hp.base_max_hp = new_max_hp

                world.world.create(
                    self.spawn_template,
                    chain,
                    spawn_pos,
                    overrides={'hp': {
                        'hp': new_hp,
                        'max_hp': new_max_hp
                    }})

                event.ctx.message_log.add('{} divides!'.format(
                    self.owner.name))

                return False

        return True
