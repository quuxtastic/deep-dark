"""
Light source
"""

from component import BaseComponent, component
from events import EventID, ItemSuccessEvent


@component('lightSource', after=['usable', 'fuelConsumer'])
class LightSourceComponent(BaseComponent):
    """
    Modifies equipper's FOV distance and algorithm
    """

    def __init__(self, owner, view_distance, fov_algorithm=0):
        super().__init__(owner)

        self.view_distance = view_distance
        self.fov_algorithm = fov_algorithm

        self.old_view_distance = -1
        self.old_fov_algorithm = -1

        self._bind_event(EventID.USE_ITEM, self._use_item)
        self._bind_event(EventID.ITEM_DEACTIVATE, self._deactivate)
        self._bind_event(EventID.EQUIPPED, self._equipped)

    def _use_item(self, event, chain):
        vision = self.owner.get_component('item').container.get_component(
            'vision')
        vision.view_distance = self.view_distance
        vision.fov_algorithm = self.fov_algorithm

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def _deactivate(self, event, chain):
        vision = self.owner.get_component('item').container.get_component(
            'vision')
        vision.view_distance = self.old_view_distance
        vision.fov_algorithm = self.old_fov_algorithm

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def _equipped(self, _, __):
        vision = self.owner.get_component('item').container.get_component(
            'vision')
        self.old_view_distance = vision.view_distance
        self.old_fov_algorithm = vision.fov_algorithm

        return True

    def serialize(self):
        return {
            'view_distance': self.view_distance,
            'fov_algorithm': self.fov_algorithm,
            'old_view_distance': self.old_view_distance,
            'old_fov_algorithm': self.old_fov_algorithm
        }

    def deserialize(self, data):
        self.view_distance = int(data['view_distance'])
        self.fov_algorithm = int(data['fov_algorithm'])
        self.old_view_distance = int(data['old_view_distance'])
        self.old_fov_algorithm = int(data['old_fov_algorithm'])
