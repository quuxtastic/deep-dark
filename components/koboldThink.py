"""
Really basic monster AI
"""

import copy

from component import BaseComponent, component
from components.monsterSteering import ThinkStates
from events import EventID, DropEvent
from geom import Coord, distance


@component('koboldThink', before=['monsterSteering'])
class KoboldThinkComponent(BaseComponent):
    """
    Kobolds will path towards the player and steal something if possible.
    If at full capacity, they will run to their home location and drop the
    items, then go to idle state.
    If the kobold loses sight of the player, they will try to travel to their
    last known position and wait there, before eventually getting bored and
    going to idle state.
    While idle, the monster will wander around randomly
    """

    def __init__(self, owner, boredom_threshold=5):
        super().__init__(owner)

        self.state = ThinkStates.IDLE_WANDER
        self.turns_since_detected = 0
        self.boredom_threshold = boredom_threshold
        self.last_seen_pos = None
        self.home_pos = None
        self.flee_time = 5
        self.turns_since_fled = 0

        self._bind_event(EventID.MONSTER_THINK, self._think)
        self._bind_event(EventID.SPAWN, self._spawn)

        self.__state_fns = {
            ThinkStates.IDLE_WANDER: self.__idle_wander,
            ThinkStates.HUNT: self.__hunt,
            ThinkStates.ATTACK: self.__attack,
            ThinkStates.FLEE: self.__flee,
            ThinkStates.STEAL: self.__steal
        }

    def __see_player(self, player):
        pos = player.get_component('world').position
        return player and player.get_component('world').visible and pos \
               and self.owner.get_component('vision').in_fov(pos)

    def __idle_wander(self, event, chain):
        if self.__see_player(event.ctx.player):
            event.think_target = event.ctx.player
            self.last_seen_pos = event.ctx.player.get_component(
                'world').position
            self.state = ThinkStates.STEAL

    def __hunt(self, event, chain):
        if self.__see_player(event.ctx.player):
            event.think_target = event.ctx.player
            self.last_seen_pos = event.ctx.player.get_component(
                'world').position
            self.state = ThinkStates.STEAL
        else:
            if self.turns_since_detected > self.boredom_threshold:
                self.turns_since_detected += 1
                self.state = ThinkStates.HUNT
            else:
                self.state = ThinkStates.IDLE_WANDER

    def __flee(self, event, chain):
        event.think_target = self.home_pos

        my_pos = self.owner.get_component('world').position

        if self.owner.get_component('world').position == self.home_pos:
            for item in copy.copy(self.owner.get_component('inventory').items):
                item.send(DropEvent(self, event.ctx, self.owner), chain)

            if self.turns_since_fled < self.flee_time:
                self.turns_since_fled += 1
            else:
                self.state = ThinkStates.IDLE_WANDER

        if distance(my_pos,
                    event.ctx.player.get_component('world').position) < 2:
            self.state = ThinkStates.ATTACK
            event.think_target = event.ctx.player
            self.last_seen_pos = event.ctx.player.get_component(
                'world').position

    def __steal(self, event, chain):
        event.think_target = event.ctx.player
        if not self.__see_player(event.ctx.player):
            self.state = ThinkStates.HUNT
            event.think_target = self.last_seen_pos
        else:
            self.last_seen_pos = event.ctx.player.get_component(
                'world').position
            inventory = self.owner.get_component('inventory')

            def _filter_stealable(ent):
                equipment = ent.get_component('equipment')
                item = ent.get_component('item')
                return item and item.weight > 0 and (
                    not equipment or equipment.equipped_by is None)

            stealable = [
                x for x in filter(
                    _filter_stealable,
                    event.ctx.player.get_component('inventory').items)
            ]
            if inventory.cur_weight >= inventory.capacity or not stealable:
                self.state = ThinkStates.FLEE
                event.think_target = self.home_pos
                self.turns_since_fled = 0
            else:
                self.state = ThinkStates.STEAL
                event.think_target = event.ctx.player

    def __attack(self, event, chain):
        my_pos = self.owner.get_component('world').position
        if self.__see_player(event.ctx.player) and distance(
                my_pos,
                event.ctx.player.get_component('world').position) < 2:
            self.state = ThinkStates.ATTACK
            event.think_target = event.ctx.player
        else:
            self.state = ThinkStates.FLEE
            self.turns_since_fled = 0
            event.think_target = self.home_pos

    def _spawn(self, event, _):
        self.home_pos = event.position

        return True

    def _think(self, event, chain):

        if self.__see_player(event.ctx.player):
            self.last_seen_pos = event.ctx.player.get_component(
                'world').position

        self.__state_fns[self.state](event, chain)

        event.think_state = self.state

        return True

    def serialize(self):
        last_pos = [self.last_seen_pos.x, self.last_seen_pos.y] \
                   if self.last_seen_pos else None
        home_pos = [self.home_pos.x, self.home_pos.y] \
                   if self.home_pos else None
        return {
            'state': int(self.state),
            'turns_since_detected': int(self.turns_since_detected),
            'boredom_threshold': int(self.boredom_threshold),
            'last_seen_pos': last_pos,
            'home_pos': home_pos,
            'turns_since_fled': int(self.turns_since_fled)
        }

    def deserialize(self, data):
        self.state = ThinkStates(int(data['state']))
        self.turns_since_detected = int(data['turns_since_detected'])
        self.boredom_threshold = int(data['boredom_threshold'])
        self.last_seen_pos = None
        last_pos = data.get('last_seen_pos')
        if last_pos:
            self.last_seen_pos = Coord(int(last_pos[0]), int(last_pos[1]))
        home_pos = data.get('home_pos')
        if home_pos:
            self.home_pos = Coord(int(home_pos[0]), int(home_pos[1]))
        self.turns_since_fled = int(data['turns_since_fled'])
