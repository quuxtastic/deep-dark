"""
Really basic monster AI
"""

from component import BaseComponent, component
from components.monsterSteering import ThinkStates
from events import EventID
from geom import Coord


@component('monsterThink', before=['monsterSteering'])
class MonsterThinkComponent(BaseComponent):
    """
    Monster will path towards the player and attack if possible.
    If the monster loses sight of the player, they will try to travel to their
    last known position and wait there, before eventually getting bored and
    going to idle state.
    While idle, the monster will wander around randomly
    """

    def __init__(self, owner, boredom_threshold=5):
        super().__init__(owner)

        self.state = ThinkStates.IDLE_WANDER
        self.turns_since_detected = 0
        self.boredom_threshold = boredom_threshold
        self.last_seen_pos = None

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _think(self, event, _):
        world_comp = self.owner.get_component('world')
        vision_comp = self.owner.get_component('vision')
        player = world_comp.world.player

        if not player:
            self.state = ThinkStates.IDLE_WANDER

        else:
            player_pos = player.get_component('world').position

            if player.get_component('world').visible \
               and player_pos \
               and vision_comp.in_fov(player_pos):
                event.think_target = player
                self.last_seen_pos = player_pos
                self.state = ThinkStates.ATTACK

            elif self.state == ThinkStates.ATTACK:
                self.state = ThinkStates.HUNT
                event.think_target = self.last_seen_pos
                self.turns_since_detected = 0

            elif self.state == ThinkStates.HUNT:
                if self.turns_since_detected >= self.boredom_threshold:
                    self.state = ThinkStates.IDLE_WANDER

                self.turns_since_detected += 1
                event.think_target = self.last_seen_pos

        event.think_state = self.state

        return True

    def serialize(self):
        last_pos = [self.last_seen_pos.x, self.last_seen_pos.y] \
                   if self.last_seen_pos else None
        return {
            'state': int(self.state),
            'turns_since_detected': int(self.turns_since_detected),
            'boredom_threshold': int(self.boredom_threshold),
            'last_seen_pos': last_pos
        }

    def deserialize(self, data):
        self.state = ThinkStates(int(data['state']))
        self.turns_since_detected = int(data['turns_since_detected'])
        self.boredom_threshold = int(data['boredom_threshold'])
        self.last_seen_pos = None
        last_pos = data.get('last_seen_pos')
        if last_pos:
            self.last_seen_pos = Coord(int(last_pos[0]), int(last_pos[1]))
