"""
Pick-uppable component
"""

from component import BaseComponent, component
from events import EventID, StoreItemEvent, SpawnEvent, DespawnEvent, \
                   RemoveItemEvent, find_event, UseItemEvent
from message import Priority


@component('item')
class ItemComponent(BaseComponent):
    """
    Allows an entity to be picked up as an item
    """

    def __init__(self, owner, weight=1):
        super().__init__(owner)

        self.container = None
        self.weight = weight

        self._bind_event(EventID.PICK_UP, self._pick_up)
        self._bind_event(EventID.DROP, self._drop)
        self._bind_event(EventID.TAKE, self._take)

    def __after_pick_up(self, event, chain):
        self.owner.send(DespawnEvent(self, event.ctx, event.ctx.world), chain)
        event.ctx.message_log.add(
            '{} picked up {}'.format(event.picker.name, self.owner.name),
            Priority.LOW)

    def _pick_up(self, event, chain):
        if self.weight == -1:
            # indicates this item despawns after being picked up

            self.__after_pick_up(event, chain)

            self.owner.send(
                UseItemEvent(self, event.ctx, event.picker, [event.picker]),
                chain)

            self.owner.factory.destroy_entity(self.owner)

        else:
            event.picker.send(
                StoreItemEvent(self, event.ctx, self.owner), chain)
            if find_event(chain, EventID.STORE_ITEM_SUCCESS):
                self.container = event.picker

                self.__after_pick_up(event, chain)

        return True

    def _drop(self, event, chain):
        event.dropper.send(RemoveItemEvent(self, event.ctx, self.owner), chain)
        if find_event(chain, EventID.REMOVE_ITEM_SUCCESS):
            self.container = None

            self.owner.send(
                SpawnEvent(self, event.ctx,
                           event.dropper.get_component('world').world,
                           event.dropper.get_component('world').position),
                chain)

            event.ctx.message_log.add(
                '{} dropped {}'.format(event.dropper.name, self.owner.name),
                Priority.LOW)

        return True

    def _take(self, event, chain):
        event.src.send(RemoveItemEvent(self, event.ctx, self.owner), chain)
        if find_event(chain, EventID.REMOVE_ITEM_SUCCESS):
            event.dst.send(StoreItemEvent(self, event.ctx, self.owner), chain)
            if not find_event(chain, EventID.STORE_ITEM_SUCCESS):
                event.src.send(
                    StoreItemEvent(self, event.ctx, self.owner), chain)

        return True

    def serialize(self):
        return {
            'container':
            self.container.id() if self.container is not None else -1,
            'weight': self.weight
        }

    def deserialize(self, data):
        self.container = self.owner.factory.get_ent_by_id(
            data['container']) if data['container'] != -1 else None
        self.weight = int(data['weight'])
