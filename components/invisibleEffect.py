"""
Makes an actor invisible
"""

from component import component
from components.renderStatusEffect import RenderEffects
from components.duration_spell import SpellDurationBaseComponent
from events import EventID, ApplyRenderEffectEvent, RemoveRenderEffectEvent


@component('invisibleEffect', after=['combat'])
class InvisibleEffectComponent(SpellDurationBaseComponent):
    """
    While attached, actor is invisible
    """

    def __init__(self, owner, turns=0):
        super().__init__(owner, 'invisible', turns)

        self._bind_event(EventID.MONSTER_THINK, self._think)
        self._bind_event(EventID.ATTACK, self._attack)

    def _on_apply(self, event, chain):
        event.ctx.message_log.add('{} makes {} invisible'.format(
            event.source.owner.name, self.owner.name))
        self.owner.get_component('world').visible = False
        self.owner.send(
            ApplyRenderEffectEvent(self, event.ctx, RenderEffects.INVISIBLE),
            chain)

    def _on_end(self, event, chain):
        self.owner.get_component('world').visible = True
        self.owner.send(
            RemoveRenderEffectEvent(self, event.ctx, RenderEffects.INVISIBLE),
            chain)
        event.ctx.message_log.add('{} is no longer invisible'.format(
            self.owner.name))

    def _think(self, event, chain):
        self._duration_think(event, chain)

        return True

    def _attack(self, event, chain):
        event.ctx.message_log.add("{}'s invisibility breaks".format(
            self.owner.name))
        self._end(event, chain)

        return True
