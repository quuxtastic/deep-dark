from component import BaseComponent, component
from events import EventID, DespawnEvent


@component('decompose')
class DecomposeComponent(BaseComponent):
    def __init__(self, owner, duration):
        super().__init__(owner)

        self.turns = duration

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _think(self, event, chain):
        self.turns -= 1
        if self.turns <= 0:
            self.owner.send(
                DespawnEvent(self, event.ctx,
                             self.owner.get_component('world').world), chain)
            event.ctx.entity_factory.destroy_entity(self.owner)

        return True

    def serialize(self):
        return {'turns': self.turns}

    def deserialize(self, data):
        self.turns = int(data['turns'])
