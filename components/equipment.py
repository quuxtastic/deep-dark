"""
Equippable items
"""

from component import BaseComponent, component
from events import EventID, ItemEquipSuccessEvent, ItemUnequipSuccessEvent


@component('equipment')
class EquipmentComponent(BaseComponent):
    """
    Items that can be equipped to an entity's equipSlots
    """

    def __init__(self, owner, slots):
        super().__init__(owner)

        self.slots = slots
        self.equipped_by = None

        self._bind_event(EventID.EQUIPPED, self._equipped)
        self._bind_event(EventID.UNEQUIPPED, self._unequipped)

    def can_equip_at(self, slot_id):
        for slot_set in self.slots:
            if slot_id in slot_set:
                return slot_set

        return False

    def _equipped(self, event, chain):
        self.equipped_by = event.user
        chain.append(ItemEquipSuccessEvent(self, event.ctx))

        return True

    def _unequipped(self, event, chain):
        self.equipped_by = None
        chain.append(ItemUnequipSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {
            'equipped_by':
            self.equipped_by.id() if self.equipped_by is not None else -1
        }

    def deserialize(self, data):
        self.equipped_by = self.owner.factory.get_ent_by_id(
            int(data['equipped_by'])) if int(
                data['equipped_by']) != -1 else None
