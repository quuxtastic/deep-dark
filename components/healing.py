"""
Healing spell
"""

from component import BaseComponent, component
from events import EventID, HealDamageEvent, ItemSuccessEvent


@component('healing', after=['usable'])
class HealingComponent(BaseComponent):
    """
    Heals damage
    """

    def __init__(self, owner, heal_amount):
        super().__init__(owner)

        self.heal_amount = heal_amount

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for ent in event.targets:
            ent.send(HealDamageEvent(self, event.ctx, self.heal_amount), chain)

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {'heal_amount': self.heal_amount}

    def deserialize(self, data):
        self.heal_amount = data['heal_amount']
