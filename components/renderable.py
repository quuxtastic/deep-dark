"""
Entity rendering
"""

import copy
import tcod

from component import BaseComponent, component
from events import EventID


@component('renderable')
class RenderableComponent(BaseComponent):
    """
    Allows an entity to be drawn on the screen
    """

    def __init__(self,
                 owner,
                 glyph=None,
                 color=None,
                 render_order=None,
                 bg=None,
                 bg_blend=None,
                 speed=None,
                 frames=None,
                 start_frame=0):
        super().__init__(owner)

        self.__assign_frames(glyph, color, render_order, bg, bg_blend, speed,
                             frames, start_frame)

        self.cur_frames_id = start_frame
        self.show = True

        self.cur_frame_index = 0
        self.last_time = float(0)

        self.__update_render_state()

        self._bind_event(EventID.DRAW, self._draw)

    def __assign_frames(self, glyph, color, render_order, bg, bg_blend, speed,
                        frames, start_frame):
        defaults = {
            'bg_blend': tcod.BKGND_NONE,
            'bg': tcod.black,
            'speed': float(0.2),
            'show': True
        }
        base_new = copy.copy(defaults)
        if glyph is not None:
            base_new['glyph'] = glyph
        if color is not None:
            base_new['color'] = color
        if render_order is not None:
            base_new['render_order'] = render_order
        if bg is not None:
            base_new['bg'] = bg
        if bg_blend is not None:
            base_new['bg_blend'] = bg_blend
        if speed is not None:
            base_new['speed'] = speed

        self.__frames = dict()

        if not frames or not frames.items():
            self.__frames[int(start_frame)] = [copy.copy(base_new)]
        else:
            for index, frame_list in frames.items():
                new_list = []
                for frame in frame_list:
                    new_frame = copy.copy(base_new)
                    new_frame.update(frame)

                    assert new_frame.get('glyph') is not None
                    assert new_frame.get('color') is not None
                    assert new_frame.get('render_order') is not None
                    assert new_frame.get('bg') is not None
                    assert new_frame.get('bg_blend') is not None
                    assert new_frame.get('speed') is not None
                    assert new_frame.get('show') is not None

                    new_list.append(new_frame)

                self.__frames[int(index)] = new_list

    def __update_render_state(self):
        self.__frame_count = len(self.__frames[self.cur_frames_id])

        self.cur_frame = self.__frames[self.cur_frames_id][
            self.cur_frame_index]
        self.__order = self.cur_frame['render_order']
        self.__color = self.cur_frame['color']
        self.__glyph = self.cur_frame['glyph']
        self.__bg_blend = self.cur_frame['bg_blend']
        self.__bg = self.cur_frame['bg']
        self.__speed = self.cur_frame['speed']
        self.__show = self.cur_frame['show']

    def set_frame_id(self, new_id):
        self.cur_frames_id = new_id
        self.cur_frame_index = 0
        self.__update_render_state()

    def get_render_order(self):
        return self.__order

    def _draw(self, event, _):
        if (self.__frame_count > 1) \
           and event.time - self.last_time > self.__speed:
            self.cur_frame_index = (self.cur_frame_index + 1) \
                                   % self.__frame_count
            self.__update_render_state()
            self.last_time = event.time

        if self.show and self.__show:
            event.console.set_fg(self.__color)
            event.console.set_bg(self.__bg)
            event.console.draw_char(event.screen_pos, ord(self.__glyph),
                                    self.__bg_blend)

    def serialize(self):
        return {'show': self.show, 'cur_frames_id': self.cur_frames_id}

    def deserialize(self, data):
        self.show = bool(data['show'])
        self.cur_frames_id = int(data['cur_frames_id'])

        self.cur_frame_index = 0
        self.__update_render_state()
