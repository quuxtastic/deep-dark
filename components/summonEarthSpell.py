"""
Summon earth spell
"""

from component import BaseComponent, component
from events import EventID, ItemSuccessEvent
from world_map import DIGGABLE_BIT, EXPLORED_BIT


@component('summonEarthSpell', after=['usable'])
class SummonEarthSpellComponent(BaseComponent):
    """
    Turns a tile into impassable dirt
    """

    def __init__(self, owner, dig_level=50):
        super().__init__(owner)

        self.dig_level = dig_level

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for pos in event.targets:
            event.user.get_component('world').world.set_tile(
                pos, DIGGABLE_BIT | EXPLORED_BIT, self.dig_level)

        chain.append(ItemSuccessEvent(self, event.ctx))
        return True

    def serialize(self):
        return {'dig_level': self.dig_level}

    def deserialize(self, data):
        self.dig_level = int(data['dig_level'])
