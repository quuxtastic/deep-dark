from component import BaseComponent, component
from events import EventID, UseItemEvent
from geom import Coord, centered_rect


@component('areaEffect')
class AreaEffectComponent(BaseComponent):
    """
    Activates on all entities in the specified area every turn
    """

    def __init__(self, owner, area_width=1, area_height=1, selector=None):
        super().__init__(owner)

        self.area_dim = Coord(area_width, area_height)
        self.selector = selector

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _think(self, event, chain):
        world = self.owner.get_component('world')
        ents = world.world.get_ents_in_rect(
            centered_rect(world.position, self.area_dim),
            selector=self.selector)
        self.owner.send(UseItemEvent(self, event.ctx, self.owner, ents), chain)

        return True
