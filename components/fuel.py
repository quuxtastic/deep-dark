"""
Fuel item
"""

from component import BaseComponent, component
from events import EventID, StoreFuelEvent, ItemSuccessEvent


@component('fuel', after=['usable'])
class FuelComponent(BaseComponent):
    """
    Fuel powers lanterns and other machinery
    """

    def __init__(self, owner, amount):
        super().__init__(owner)

        self.amount = amount

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for ent in event.targets:
            ent.send(
                StoreFuelEvent(self, event.ctx, self.amount, self.owner),
                chain)

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {'amount': self.amount}

    def deserialize(self, data):
        self.amount = int(data['amount'])
