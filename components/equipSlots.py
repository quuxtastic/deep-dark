"""
Equipment slots
"""

from enum import IntEnum, auto

from component import BaseComponent, component
from events import EventID, find_event, EquippedEvent, UnequippedEvent


class EquipSlots(IntEnum):
    """
    Equipment slot ids
    """
    HEAD = auto()
    NECK = auto()
    BODY = auto()
    GLOVES = auto()
    FEET = auto()
    LEGS = auto()
    RING_LEFT = auto()
    RING_RIGHT = auto()

    MAIN_HAND = auto()
    OFF_HAND = auto()

    LIGHT_SOURCE = auto()


@component('equipSlots')
class EquipSlotsComponent(BaseComponent):
    """
    Allows an entity to have items equipped
    """

    def __init__(self, owner):
        super().__init__(owner)

        self.slots = dict()

        self._bind_event(EventID.EQUIP_ITEM, self._equip_item)
        self._bind_event(EventID.UNEQUIP_ITEM, self._unequip_item)

    def has_equipped(self, item):
        """
        :returns: True if the given item is equipped
        """
        return item in self.slots.values()

    def get_at_slot(self, slot):
        """
        :returns: Entity at given slot, or None if nothing is equipped
        """
        return self.slots.get(slot)

    def _equip_item(self, event, chain):
        items_to_unequip = set()
        for slot in event.slots:
            existing = self.get_at_slot(slot)
            if existing:
                items_to_unequip.add(existing)

        for item in items_to_unequip:
            item.send(UnequippedEvent(self, event.ctx, self.owner), chain)
            if not find_event(chain, EventID.ITEM_UNEQUIP_SUCCESS):
                event.ctx.message_log.add("Can't unequip {}".format(item.name))
                return True

        event.item.send(EquippedEvent(self, event.ctx, self.owner), chain)
        if find_event(chain, EventID.ITEM_EQUIP_SUCCESS):
            for slot in event.slots:
                self.slots[slot] = event.item
        else:
            event.ctx.message_log.add("Can't equip {}".format(event.item.name))

        return True

    def _unequip_item(self, event, chain):
        event.item.send(UnequippedEvent(self, event.ctx, self.owner), chain)
        if find_event(chain, EventID.ITEM_UNEQUIP_SUCCESS):
            for slot in self.slots.keys():
                if self.slots[slot] is event.item:
                    self.slots[slot] = None
        else:
            event.ctx.message_log.add("Can't unequip {}".format(
                event.item.name))

        return True

    def serialize(self):
        out_slots = dict()
        for slot, item in self.slots.items():
            if item is not None:
                out_slots[int(slot)] = int(item.id())
        return {'slots': out_slots}

    def deserialize(self, data):
        for slot, eid in data['slots'].items():
            self.slots[EquipSlots(
                int(slot))] = self.owner.factory.get_ent_by_id(eid)
