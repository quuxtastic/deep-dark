"""
Map revealer
"""

from component import component
from components.duration_spell import SpellDurationBaseComponent
from events import EventID


@component('mapRevealEffect')
class MapRevealEffectComponent(SpellDurationBaseComponent):
    """
    Temporarily puts the whole map in the player FOV
    """

    def __init__(self, owner, turns=0):
        super().__init__(owner, 'esp', turns)

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _on_apply(self, event, chain):
        event.ctx.message_log.add('{} broadens {}\'s perception'.format(
            event.source.owner.name, self.owner.name))
        event.ctx.renderer.get_map_view().force_visible = True

    def _on_end(self, event, chain):
        event.ctx.renderer.get_map_view().force_visible = False

    def _think(self, event, chain):
        self._duration_think(event, chain)

        return True
