from component import BaseComponent, component
from events import EventID, find_event, TakeEvent


@component('stealth')
class StealthComponent(BaseComponent):
    def __init__(self, owner):
        super().__init__(owner)

        self._bind_event(EventID.STEAL, self._steal)

    def _steal(self, event, chain):
        event.target_item.send(
            TakeEvent(self, event.ctx, event.target, self.owner), chain)
        if find_event(chain, EventID.STORE_ITEM_SUCCESS):
            event.ctx.message_log.add('{} steals {} from {}'.format(
                self.owner.name, event.target_item.name, event.target.name))

        return True
