"""
Stairs world entity
"""

from component import BaseComponent, component
from events import EventID, StateChangeEvent
from state.stateman import GameStates
from message import Priority


@component('stairs')
class StairsComponent(BaseComponent):
    """
    Allows travel between floors
    """

    def __init__(self, owner, dest_level=-1):
        super().__init__(owner)

        self.dest_level = dest_level

        self._bind_event(EventID.INTERACT, self._interact)

    def _interact(self, event, chain):
        world_comp = self.owner.get_component('world')
        # do nothing if this is not the player (shouldn't happen)
        if event.interactor is not world_comp.world.player:
            return True

        chain.append(
            StateChangeEvent(self, event.ctx, GameStates.MAP_CHANGE,
                             [self.dest_level]))

        event.ctx.message_log.add(
            '{} travels to level {}'.format(world_comp.world.player.name,
                                            self.dest_level), Priority.HIGH)

        return True

    def serialize(self):
        return {'dest_level': self.dest_level}

    def deserialize(self, data):
        self.dest_level = int(data['dest_level'])
