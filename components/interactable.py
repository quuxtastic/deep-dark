"""
World interaction
"""

from component import BaseComponent, component


@component('interactable')
class InteractableComponent(BaseComponent):
    """
    Entity can be interacted with in the world
    Right now this is empty, used only for tagging
    """
