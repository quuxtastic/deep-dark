"""
Allows identification of randomized items
"""

from component import BaseComponent, component


@component('identifier')
class IdentifierComponent(BaseComponent):
    def __init__(self, owner):
        super().__init__(owner)

        self.identified_names = dict()
        self.identified_descs = dict()

    def add(self, item, real_name, real_desc):
        self.identified_names[item.name] = real_name
        self.identified_descs[item.name] = real_desc

    def get_name(self, item):
        return self.identified_names.get(item.name, item.name)

    def get_desc(self, item):
        return self.identified_descs.get(item.name, item.description)

    def is_identified(self, ent):
        return self.identified_names.get(ent.name) is not None

    def serialize(self):
        return {'names': self.identified_names, 'descs': self.identified_descs}

    def deserialize(self, data):
        self.identified_names = data['names']
        self.identified_descs = data['descs']
