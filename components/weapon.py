"""
Weapon equipment
"""

from component import BaseComponent, component
from events import EventID


@component('weapon')
class WeaponComponent(BaseComponent):
    """
    Buffs attack when equipped
    """

    def __init__(self, owner, attack_mod, damage_mod, crit_mod):
        super().__init__(owner)

        self.attack_mod = attack_mod
        self.damage_mod = damage_mod
        self.crit_mod = crit_mod

        self._bind_event(EventID.EQUIPPED, self._equipped)
        self._bind_event(EventID.UNEQUIPPED, self._unequipped)

        desc_fmt = '{} ({})'
        self.owner.description = desc_fmt.format(self.owner.description,
                                                 self.__make_info_str())

    def __make_info_str(self, invert=False):
        inv = 1 if not invert else -1
        parts = []
        if self.attack_mod != 0:
            parts.append('{:+d} atk'.format(self.attack_mod * inv))
        if self.damage_mod != 0:
            parts.append('{:+d} dmg'.format(self.damage_mod * inv))
        if self.crit_mod != 0:
            parts.append('{:+d} crit'.format(self.crit_mod * inv))

        return ', '.join(parts)

    def _equipped(self, event, _):
        combat = event.user.get_component('combat')
        if combat:
            combat.base_attack += self.attack_mod
            combat.base_damage_bonus += self.damage_mod
            combat.base_crit_chance += self.crit_mod
            event.ctx.message_log.add('{} equips {} ({})'.format(
                event.user.name, self.owner.name, self.__make_info_str()))

        return True

    def _unequipped(self, event, _):
        combat = event.user.get_component('combat')
        if combat:
            combat.base_attack -= self.attack_mod
            combat.base_damage_bonus -= self.damage_mod
            combat.base_crit_chance -= self.crit_mod
            event.ctx.message_log.add('{} unequips {} ({})'.format(
                event.user.name, self.owner.name, self.__make_info_str(True)))

        return True

    def serialize(self):
        return {
            'attack_mod': int(self.attack_mod),
            'damage_mod': int(self.damage_mod),
            'crit_mod': int(self.crit_mod)
        }

    def deserialize(self, data):
        self.attack_mod = int(data['attack_mod'])
        self.damage_mod = int(data['damage_mod'])
        self.crit_mod = int(data['crit_mod'])
