"""
Inventory component
"""

import copy

from component import BaseComponent, component
from events import EventID, StoreItemSuccessEvent, RemoveItemSuccessEvent, \
                   MonsterThinkEvent, DropEvent
from message import Priority


@component('inventory', before=['death'])
class InventoryComponent(BaseComponent):
    """
    Allows an entity to carry items
    """

    def __init__(self, owner, base_capacity):
        super().__init__(owner)

        self.base_capacity = base_capacity
        self.cur_weight = 0
        self.items = []

        self._bind_event(EventID.STORE_ITEM, self._store_item)
        self._bind_event(EventID.REMOVE_ITEM, self._remove_item)
        self._bind_event(EventID.MONSTER_THINK, self._think)
        self._bind_event(EventID.KILLED, self._killed)

    @property
    def capacity(self):
        stats = self.owner.get_component('stats')

        str_mod = 0
        if stats:
            str_mod = stats.str

        return self.base_capacity + (str_mod * 10)

    @property
    def cur_empty(self):
        return self.capacity - self.cur_weight

    def _store_item(self, event, chain):
        item = event.item.get_component('item')
        if item.weight <= (self.capacity - self.cur_weight):
            self.items.append(event.item)
            self.cur_weight += item.weight
            chain.append(StoreItemSuccessEvent(self, event.ctx))
        else:
            event.ctx.message_log.add(
                '{} has no space for {}'.format(self.owner.name,
                                                event.item.name), Priority.LOW)

        return True

    def _remove_item(self, event, chain):
        self.items.remove(event.item)
        self.cur_weight -= event.item.get_component('item').weight
        chain.append(RemoveItemSuccessEvent(self, event.ctx))

        return True

    def _think(self, event, chain):
        for ent in self.items:
            ent.send(MonsterThinkEvent(event.ctx), chain)

        return True

    def _killed(self, event, chain):
        for ent in copy.copy(self.items):
            ent.send(DropEvent(self, event.ctx, self.owner), chain)

        return True

    def serialize(self):
        return {
            'base_capacity': self.base_capacity,
            'cur_weight': self.cur_weight,
            'items': [ent.id() for ent in self.items]
        }

    def deserialize(self, data):
        self.base_capacity = int(data['base_capacity'])
        self.cur_weight = int(data['cur_weight'])
        for eid in data['items']:
            self.items.append(self.owner.factory.get_ent_by_id(int(eid)))
