"""
Damage dealing effect
"""

from enum import IntEnum, auto

from component import BaseComponent, component
from events import EventID, TakeDamageEvent, ItemSuccessEvent


class DamageTypes(IntEnum):
    UNTYPED = auto()
    PHYSICAL = auto()
    FIRE = auto()
    ACID = auto()
    COLD = auto()
    SHOCK = auto()
    POISON = auto()


@component('damage', after=['usable'])
class DamageComponent(BaseComponent):
    """
    Applies damage to specified actors on use
    """

    def __init__(self, owner, damage, tags=None):
        super().__init__(owner)
        self.damage = damage
        if not tags:
            tags = [DamageTypes.UNTYPED]
        self.tags = [DamageTypes(int(tag)) for tag in tags]

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for ent in event.targets:
            event.ctx.message_log.add('{} deals {} {} damage to {}'.format(
                self.owner.name, self.damage, self.tags[0].name, ent.name))
            ent.send(
                TakeDamageEvent(self, event.ctx, self.owner, self.damage,
                                self.tags), chain)

        chain.append(ItemSuccessEvent(self, event.ctx))

    def serialize(self):
        return {'damage': self.damage, 'tags': self.tags}

    def deserialize(self, data):
        self.damage = data['damage']
        self.tags = [DamageTypes(int(tag)) for tag in data['tags']]
