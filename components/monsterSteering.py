"""
Monster AI low-level steering
"""

import math
from enum import IntEnum, auto

from component import BaseComponent, component
from events import EventID, MoveEvent, AttackEvent, StealEvent
from geom import Coord, distance


class ThinkStates(IntEnum):
    IDLE_WANDER = auto()
    IDLE_SLEEP = auto()
    HUNT = auto()
    ATTACK = auto()
    FLEE = auto()
    STEAL = auto()


@component('monsterSteering')
class MonsterSteeringComponent(BaseComponent):
    def __init__(self, owner):
        super().__init__(owner)

        self._bind_event(EventID.MONSTER_THINK, self._think)

        self.__state_fns = {
            ThinkStates.IDLE_WANDER: self.__idle_wander,
            ThinkStates.IDLE_SLEEP: self.__idle_sleep,
            ThinkStates.HUNT: self.__hunt,
            ThinkStates.ATTACK: self.__attack,
            ThinkStates.FLEE: self.__flee,
            ThinkStates.STEAL: self.__steal
        }

    def __move_to(self, event, chain, world_comp, target):
        path = world_comp.world.calc_path(world_comp.position, target)
        if path:
            self.owner.send(
                MoveEvent(self, event.ctx,
                          Coord(*path[0]) - world_comp.position), chain)
        else:
            dmov = target - world_comp.position
            dist = math.sqrt(dmov.x**2 + dmov.y**2)
            dpos = Coord(int(round(dmov.x / dist)), int(round(dmov.y / dist)))
            self.owner.send(MoveEvent(self, event.ctx, dpos), chain)

    def __idle_wander(self, event, chain):
        if event.ctx.rand.perc(50):
            directions = [
                Coord(1, 0),
                Coord(0, 1),
                Coord(1, 1),
                Coord(-1, 0),
                Coord(0, -1),
                Coord(-1, 1),
                Coord(1, -1),
                Coord(-1, -1)
            ]
            self.owner.send(
                MoveEvent(self, event.ctx, event.ctx.rand.choice(directions)),
                chain)

    def __idle_sleep(self, event, chain):
        pass

    def __hunt(self, event, chain):
        world = self.owner.get_component('world')
        if world.position != event.think_target:
            self.__move_to(event, chain, world, event.think_target)

    def __attack(self, event, chain):
        world = self.owner.get_component('world')
        dpos = event.think_target.get_component('world').position
        if distance(world.position, dpos) >= 2:
            self.__move_to(event, chain, world, dpos)
        else:
            self.owner.send(
                AttackEvent(self, event.ctx, event.think_target), chain)

    def __flee(self, event, chain):
        world = self.owner.get_component('world')
        if event.think_target != world.position:
            self.__move_to(event, chain, world, event.think_target)

    def __steal(self, event, chain):
        world = self.owner.get_component('world')
        dpos = event.think_target.get_component('world').position
        if distance(world.position, dpos) >= 2:
            self.__move_to(event, chain, world, dpos)
        else:

            def _filter_stealable(ent):
                equipment = ent.get_component('equipment')
                item = ent.get_component('item')
                return item and item.weight > 0 and (
                    not equipment or equipment.equipped_by is None)

            stealable = sorted(
                filter(_filter_stealable,
                       event.think_target.get_component('inventory').items),
                key=lambda ent: ent.get_component('item').weight)
            if stealable:
                if self.owner.get_component(
                        'inventory').cur_empty >= stealable[0].get_component(
                            'item').weight:
                    self.owner.send(
                        StealEvent(self, event.ctx, event.think_target,
                                   stealable[0]), chain)

    def _think(self, event, chain):
        state = event.think_state

        self.__state_fns[state](event, chain)

        return True
