"""
Door world object
"""

from enum import IntEnum

from component import BaseComponent, component
from events import EventID, DespawnEvent
from message import Priority


class DoorRenderStates(IntEnum):
    """
    Used by RenderStateComponent to decide how to draw the door
    """
    CLOSED = 0
    OPEN = 1


@component('door')
class DoorComponent(BaseComponent):
    """
    Door that can be opened, closed, or locked
    """

    def __init__(self, owner, closed, locked=False):
        super().__init__(owner)

        self.closed = closed
        self.locked = locked

        self.is_dead = False

        self._bind_event(EventID.INTERACT, self._interact)
        self._bind_event(EventID.KILLED, self._killed)

    def __update_state(self):
        world = self.owner.get_component('world')
        render = self.owner.get_component('renderable')

        world.block_mov = self.closed
        world.block_los = self.closed
        world.world.update_tile(world.position)

        render.set_frame_id(DoorRenderStates.CLOSED \
                            if self.closed else DoorRenderStates.OPEN)

    def _interact(self, event, _):
        if self.locked:
            event.ctx.message_log.add('{} is locked'.format(self.owner.name),
                                      Priority.LOW)

        else:
            ents = event.ctx.world.get_ents_at(
                self.owner.get_component('world').position,
                {'components': {
                    'world': None
                }})
            ents.remove(self.owner)
            if ents:
                event.ctx.message_log.add('{} blocks the door'.format(
                    ents[0].name))
            else:
                self.closed = not self.closed

                event.ctx.message_log.add('{} is {}'.format(
                    self.owner.name,
                    'opened' if not self.closed else 'closed'))

                self.__update_state()

        return True

    def _killed(self, event, chain):
        if not self.is_dead:
            self.is_dead = True

            event.ctx.message_log.add('{} broke down {}'.format(
                event.cod, self.owner.name))

            self.owner.send(
                DespawnEvent(self, event.ctx,
                             self.owner.get_component('world').world), chain)
            self.owner.factory.destroy_entity(self.owner)

        return False

    def serialize(self):
        return {'closed': self.closed, 'locked': self.locked}

    def deserialize(self, data):
        self.closed = bool(data['closed'])
        self.locked = bool(data['locked'])
