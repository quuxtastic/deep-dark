"""
Track hunger
"""

from component import BaseComponent, component
from events import EventID, TakeDamageEvent
from components.damage import DamageTypes
from message import Priority


@component('hunger')
class HungerComponent(BaseComponent):
    """
    Tracks hunger based on exertion and deals damage if at max hunger
    """

    def __init__(self, owner, max_hunger, cur_hunger=None):
        super().__init__(owner)

        self.base_max_hunger = max_hunger
        if cur_hunger is None:
            cur_hunger = 0

        self.cur_hunger = cur_hunger

        self._bind_event(EventID.EXHAUST, self._exhaust)
        self._bind_event(EventID.EAT, self._eat)

    @property
    def max_hunger(self):
        stats = self.owner.get_component('stats')
        con_mod = 0
        if stats:
            con_mod = stats.con

        return self.base_max_hunger + (con_mod * 20)

    def _exhaust(self, event, chain):
        self.cur_hunger += event.hunger
        if self.cur_hunger > self.max_hunger:
            diff = self.cur_hunger - self.max_hunger
            event.ctx.message_log.add(
                '{} loses {} HP from exhaustion'.format(self.owner.name, diff),
                Priority.HIGH)
            self.owner.send(
                TakeDamageEvent(
                    self,
                    event.ctx,
                    None,
                    diff,
                    tags=[DamageTypes.UNTYPED],
                    source_name='hunger'), chain)
            self.cur_hunger = self.max_hunger

        return True

    def _eat(self, event, _):
        event.ctx.message_log.add(
            '{} eats {} ({} hunger)'.format(self.owner.name, event.item.name,
                                            event.nutrition), Priority.LOW)
        self.cur_hunger = max(0, self.cur_hunger - event.nutrition)

        return True

    def serialize(self):
        return {
            'base_max_hunger': self.base_max_hunger,
            'cur_hunger': self.cur_hunger
        }

    def deserialize(self, data):
        self.base_max_hunger = int(data['base_max_hunger'])
        self.cur_hunger = int(data['cur_hunger'])
