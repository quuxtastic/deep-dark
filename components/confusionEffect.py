"""
Confuses enemies
"""

from component import component
from components.duration_spell import SpellDurationBaseComponent
from components.renderStatusEffect import RenderEffects
from events import EventID, RemoveRenderEffectEvent, ApplyRenderEffectEvent
from geom import Coord


@component('confusionEffect', before=['world'])
class ConfusionEffectComponent(SpellDurationBaseComponent):
    """
    Entity will move around randomly instead of chasing player
    """

    def __init__(self, owner, turns=0):
        super().__init__(owner, 'confused', turns)

        self.directions = [
            Coord(1, 0),
            Coord(0, 1),
            Coord(1, 1),
            Coord(-1, 0),
            Coord(0, -1),
            Coord(-1, 1),
            Coord(1, -1),
            Coord(-1, -1),
            Coord(0, 0)
        ]

        self._bind_event(EventID.MONSTER_THINK, self._monster_think)
        self._bind_event(EventID.PLAYER_TURN_DONE, self._player_turn_done)
        self._bind_event(EventID.MOVE, self._move)

    def _on_apply(self, event, chain):
        event.ctx.message_log.add('{} confuses {}'.format(
            event.source.owner.name, self.owner.name))
        self.owner.send(
            ApplyRenderEffectEvent(self, event.ctx, RenderEffects.CONFUSED),
            chain)

    def _on_end(self, event, chain):
        self.owner.send(
            RemoveRenderEffectEvent(self, event.ctx, RenderEffects.CONFUSED),
            chain)
        event.ctx.message_log.add('{} is no longer confused'.format(
            self.owner.name))

    def _move(self, event, _):
        event.direction = event.ctx.rand.choice(self.directions)

        return True

    def _monster_think(self, event, chain):
        # player has their own version of confusion
        if self.owner is event.ctx.player:
            return True

        self._duration_think(event, chain)

        return True

    def _player_turn_done(self, event, chain):
        self._duration_think(event, chain)

        return True
