from component import BaseComponent, component
from events import EventID, KilledEvent
from components.damage import DamageTypes


@component('hp')
class HpComponent(BaseComponent):
    def __init__(self, owner, max_hp, resist=None, hp=None):
        super().__init__(owner)

        self.base_max_hp = max_hp
        if hp is None:
            hp = max_hp
        self.hp = hp

        self.resist = {
            DamageTypes.UNTYPED: 0,
            DamageTypes.PHYSICAL: 0,
            DamageTypes.FIRE: 0,
            DamageTypes.ACID: 0,
            DamageTypes.COLD: 0,
            DamageTypes.SHOCK: 0,
            DamageTypes.POISON: 0
        }
        if resist:
            self.resist.update(resist)

        self._bind_event(EventID.TAKE_DAMAGE, self._take_dmg)
        self._bind_event(EventID.HEAL_DAMAGE, self._heal_dmg)
        self._bind_event(EventID.PLAYER_WAIT, self._rest)

    @property
    def max_hp(self):
        stats = self.owner.get_component('stats')

        con_mod = 0
        if stats:
            con_mod = stats.con

        return self.base_max_hp + (con_mod * 5)

    def _take_dmg(self, event, chain):
        dmg = event.damage

        best_resist = -1000
        best_tag = ''
        for tag in event.tags:
            if self.resist[tag] > best_resist:
                best_resist = self.resist[tag]
                best_tag = tag.name

        dmg -= int(event.damage * (float(best_resist) / 100.0))

        if dmg < event.damage:
            event.ctx.message_log.add('{} resists {} {} damage'.format(
                self.owner.name, event.damage - dmg, best_tag))
        elif dmg > event.damage:
            event.ctx.message_log.add('{} takes {} extra {} damage'.format(
                self.owner.name, dmg - event.damage, best_tag))

        if dmg > 0:
            self.hp -= dmg
            if self.hp <= 0:
                self.owner.send(
                    KilledEvent(self, event.ctx, event.source_name), chain)

        return True

    def __heal(self, amount):
        self.hp = min(self.max_hp, self.hp + amount)

    def _heal_dmg(self, event, _):
        self.__heal(event.heal_amount)

        event.ctx.message_log.add('{} heals {} for {} HP'.format(
            event.source.owner.name, self.owner.name, event.heal_amount))

        return True

    def _rest(self, _, __):
        self.__heal(1)

    def serialize(self):
        return {
            'base_max_hp': self.base_max_hp,
            'hp': self.hp,
            'resist': self.resist
        }

    def deserialize(self, data):
        self.base_max_hp = int(data['base_max_hp'])
        self.hp = int(data['hp'])
        for key, val in data['resist'].items():
            self.resist[DamageTypes(int(key))] = int(val)
