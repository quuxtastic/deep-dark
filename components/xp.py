from component import BaseComponent, component
from events import EventID, ItemSuccessEvent, GainXpEvent


@component('xp', after=['usable'])
class XpComponent(BaseComponent):
    """
    Adds XP to the player
    """

    def __init__(self, owner, amount=0):
        super().__init__(owner)

        self.amount = amount

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        event.user.send(
            GainXpEvent(self, event.ctx,
                        self.amount * (1 + event.ctx.map_index),
                        self.owner.name), chain)
        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {'amount': self.amount}

    def deserialize(self, data):
        self.amount = int(data['amount'])
