"""
Food item
"""

from component import BaseComponent, component
from events import EventID, EatEvent, ItemSuccessEvent


@component('food', after=['usable'])
class FoodComponent(BaseComponent):
    """
    Food lets you recover hunger
    """

    def __init__(self, owner, nutrition):
        super().__init__(owner)

        self.nutrition = nutrition

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for ent in event.targets:
            ent.send(
                EatEvent(self, event.ctx, self.owner, self.nutrition), chain)

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {'nutrition': self.nutrition}

    def deserialize(self, data):
        self.nutrition = int(data['nutrition'])
