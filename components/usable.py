"""
Usable items
"""

from enum import IntEnum

from component import BaseComponent, component
from message import Priority
from geom import Coord, line_bresenham, centered_rect
from entity import filter_entities
from events import find_event, EventID, ItemActivateEvent, \
                   ItemDeactivateEvent, UseItemEvent


class TargetTypes(IntEnum):
    SELF = 0
    SINGLE = 1
    SQUARE = 2
    LIST_IN_FOV = 3
    SINGLE_EMPTY_TILE = 4
    LINE = 5
    CONE2 = 6
    CONE3 = 7


@component('usable')
class UsableComponent(BaseComponent):
    """
    Item can be used when in inventory
    """

    def __init__(self,
                 owner,
                 target_type=TargetTypes.SELF,
                 target_radius=None,
                 target_selector=None,
                 target_cone_distance=None,
                 requires_equip=False,
                 toggle=False):
        super().__init__(owner)
        self.target_type = target_type
        self.target_radius = target_radius
        self.target_selector = target_selector
        self.target_cone_distance = target_cone_distance
        self.requires_equip = requires_equip
        self.toggle = toggle
        self.active = False

        self._bind_event(EventID.ITEM_ACTIVATE, self._item_activate)
        self._bind_event(EventID.ITEM_DEACTIVATE, self._item_deactivate)
        self._bind_event(EventID.ITEM_TOGGLE, self._item_toggle)
        self._bind_event(EventID.UNEQUIPPED, self._unequip)

    def __check_fov(self, user, pos):
        return user.get_component('vision').in_fov(pos)

    def get_targets(self,
                    user,
                    messages=None,
                    target_pos=None,
                    target_dir=None):
        ret = []

        if self.target_type == TargetTypes.SELF:
            ret = filter_entities([user], self.target_selector)
            if not ret:
                ret = False

        elif self.target_type == TargetTypes.SINGLE:
            if not self.__check_fov(user, target_pos):
                if messages:
                    messages.add('Target not in line of sight', Priority.LOW)
                ret = False
            else:
                ret = user.get_component('world').world.get_ents_at(
                    target_pos, self.target_selector)

        elif self.target_type == TargetTypes.SQUARE:
            if not self.__check_fov(user, target_pos):
                if messages:
                    messages.add('Target not in line of sight', Priority.LOW)
                ret = False
            else:
                target_rect = centered_rect(
                    target_pos,
                    Coord(self.target_radius * 2 + 1,
                          self.target_radius * 2 + 1))
                ret = user.get_component('world').world.get_ents_in_rect(
                    target_rect, self.target_selector)

        elif self.target_type == TargetTypes.LIST_IN_FOV:
            ret = user.get_component('vision').get_ents_in_fov(
                self.target_selector)

        elif self.target_type == TargetTypes.SINGLE_EMPTY_TILE:
            if not self.__check_fov(user, target_pos):
                if messages:
                    messages.add('Target not in line of sight', Priority.LOW)
                ret = False
            elif user.get_component('world').world.get_ents_at(target_pos):
                if messages:
                    messages.add('Must have an empty space', Priority.LOW)
                return False

            else:
                ret = [target_pos]

        elif self.target_type == TargetTypes.LINE:
            line = line_bresenham(
                target_pos + target_dir,
                target_pos + (target_dir * self.target_cone_distance))
            for coord in line:
                ret.extend(
                    user.get_component('world').world.get_ents_at(
                        coord, self.target_selector))
        elif self.target_type == TargetTypes.CONE2:
            if messages:
                messages.add('{} not implemented'.format(self.target_type),
                             Priority.HIGH)
            return False
        elif self.target_type == TargetTypes.CONE3:
            if messages:
                messages.add('{} not implemented'.format(self.target_type),
                             Priority.HIGH)
            return False

        return ret

    def _item_activate(self, event, chain):
        if self.requires_equip:
            user = self.owner.get_component('equipment').equipped_by
            if user is not event.user:
                event.ctx.message_log.add('{} must be equipped first')
                return False

        self.owner.send(
            UseItemEvent(self, event.ctx, event.user, event.targets,
                         event.uses), chain)
        if self.toggle and find_event(chain, EventID.ITEM_SUCCESS):
            self.active = True

        return True

    def _item_deactivate(self, _, __):
        if self.toggle:
            self.active = False

        return True

    def _item_toggle(self, event, chain):
        if self.toggle:
            if not self.active:
                self.owner.send(
                    ItemActivateEvent(self, event.ctx, event.user,
                                      event.targets, event.uses), chain)
            else:
                self.owner.send(
                    ItemDeactivateEvent(self, event.ctx, event.user), chain)

        return True

    def _unequip(self, event, chain):
        self.owner.send(
            ItemDeactivateEvent(self, event.ctx, event.user), chain)

        return True

    def serialize(self):
        return {
            'target_type': self.target_type,
            'target_radius': self.target_radius,
            'target_selector': self.target_selector,
            'target_cone_distance': self.target_cone_distance,
            'requires_equip': self.requires_equip,
            'toggle': self.toggle,
            'active': self.active
        }

    def deserialize(self, data):
        self.target_type = TargetTypes(int(
            data['target_type'])) if data['target_type'] is not None else None
        self.target_radius = int(
            data['target_radius']
        ) if data['target_radius'] is not None else None
        self.target_selector = data[
            'target_selector'] if data['target_selector'] is not None else None
        self.target_cone_distance = int(
            data['target_cone_distance']
        ) if data['target_cone_distance'] is not None else None
        self.requires_equip = bool(data['requires_equip'])
        self.active = bool(data['active'])
        self.toggle = bool(data['toggle'])
