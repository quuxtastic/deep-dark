"""
Armor equipment
"""

from component import BaseComponent, component
from events import EventID


@component('armor')
class ArmorComponent(BaseComponent):
    """
    Buffs defence when equipped
    """

    def __init__(self, owner, defence_mod):
        super().__init__(owner)

        self.defence_mod = defence_mod

        self._bind_event(EventID.EQUIPPED, self._equipped)
        self._bind_event(EventID.UNEQUIPPED, self._unequipped)

    def _equipped(self, event, _):
        combat = event.user.get_component('combat')
        if combat:
            combat.base_defence += self.defence_mod
            event.ctx.message_log.add('{} equips {} ({:+d} def)'.format(
                event.user.name, self.owner.name, self.defence_mod))

        return True

    def _unequipped(self, event, _):
        combat = event.user.get_component('combat')
        if combat:
            combat.base_defence -= self.defence_mod
            event.ctx.message_log.add('{} unequips {} ({:+d} def)'.format(
                event.user.name, self.owner.name, -self.defence_mod))

        return True

    def serialize(self):
        return {'defence_mod': int(self.defence_mod)}

    def deserialize(self, data):
        self.defence_mod = int(data['defence_mod'])
