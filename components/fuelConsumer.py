"""
Entities that consume fuel
"""

from component import BaseComponent, component
from events import EventID, find_event, ConsumeFuelEvent, ItemDeactivateEvent
from message import Priority


@component('fuelConsumer', after=['usable'])
class FuelConsumerComponent(BaseComponent):
    """
    Item needs fuel before it can be used
    """

    def __init__(self, owner, use_cost=0, periodic_cost=0):
        super().__init__(owner)
        self.use_cost = use_cost
        self.periodic_cost = periodic_cost

        self._bind_event(EventID.USE_ITEM, self._use_item)
        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _use_item(self, event, chain):
        if self.periodic_cost > 0:
            tank = event.user.get_component('fuelTank')
            if not tank or tank.current < self.periodic_cost:
                event.ctx.message_log.add(
                    '{} does not have fuel for {}'.format(
                        event.user.name, self.owner.name), Priority.LOW)
                return False

        if self.use_cost > 0:
            event.user.send(
                ConsumeFuelEvent(self, event.ctx, self.use_cost), chain)
            if not find_event(chain, EventID.FUEL_AVAILABLE):
                event.ctx.message_log.add(
                    '{} needs fuel'.format(self.owner.name), Priority.LOW)
                return False

        return True

    def _think(self, event, chain):
        use_info = self.owner.get_component('usable')
        if use_info.active and self.periodic_cost > 0:
            container = self.owner.get_component('item').container
            if not container:
                event.ctx.message_log.add('No fuel for {}'.format(
                    self.owner.name))
                self.owner.send(
                    ItemDeactivateEvent(self, event.ctx, container), chain)
            else:
                container.send(
                    ConsumeFuelEvent(self, event.ctx, self.periodic_cost),
                    chain)
                if not find_event(chain, EventID.FUEL_AVAILABLE):
                    event.ctx.message_log.add(
                        '{} does not have fuel for {}'.format(
                            container.name, self.owner.name))
                    self.owner.send(
                        ItemDeactivateEvent(self, event.ctx, container), chain)

        return True

    def serialize(self):
        return {'use_cost': self.use_cost, 'periodic_cost': self.periodic_cost}

    def deserialize(self, data):
        self.use_cost = int(data['use_cost'])
        self.periodic_cost = int(data['periodic_cost'])
