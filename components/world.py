"""
World interaction component
"""

from enum import Enum, auto

from geom import Coord
from component import BaseComponent, component
from events import EventID, MoveBlockedEvent, MoveSuccessEvent


class MoveBlockReasons(Enum):
    TILE = auto()
    TILE_DIG = auto()
    ENTITY = auto()
    STUCK = auto()


@component('world')
class WorldComponent(BaseComponent):
    """
    Allows an entity to be represented on the world map
    """

    def __init__(self, owner, block_mov=False, block_los=False):
        super().__init__(owner)

        self.position = None
        self.block_mov = block_mov
        self.block_los = block_los

        self.visible = True
        self.world = None

        self._bind_event(EventID.SPAWN, self._spawn)
        self._bind_event(EventID.DESPAWN, self._despawn)
        self._bind_event(EventID.MOVE, self._move)

    def _spawn(self, event, _):
        self.position = event.position
        self.world = event.world
        if not event.defer_placement:
            event.world.spawn(self.owner)

        return True

    def _despawn(self, event, _):
        if self.world:
            self.world.despawn(self.owner)

        if not self.owner is event.ctx.player:
            self.position = None
            self.world = None

        return True

    def _move(self, event, chain):
        npos = self.position + event.direction
        if npos == self.position:
            return True

        if self.world.is_passable(npos):
            opos = self.position
            self.world.on_move_ent(self.owner, self.position, npos)
            self.position = npos
            self.owner.send(
                MoveSuccessEvent(self, event.ctx, old_pos=opos, new_pos=npos),
                chain)
            return True

        if self.world.can_dig(npos):
            reason = MoveBlockReasons.TILE_DIG
            blockers = None
        else:
            ents = self.world.get_ents_at(
                npos, {'components': {
                    'world': {
                        'block_mov': True
                    }
                }})
            if not ents:
                reason = MoveBlockReasons.TILE
                blockers = None
            else:
                reason = MoveBlockReasons.ENTITY
                blockers = ents

        self.owner.send(
            MoveBlockedEvent(
                self, event.ctx, reason=reason, blockers=blockers), chain)

        return True

    def serialize(self):
        pos = [self.position.x, self.position.y] if self.position else None
        return {
            'position': pos,
            'block_mov': self.block_mov,
            'block_los': self.block_los,
            'visible': self.visible
        }

    def deserialize(self, data):
        self.block_mov = data['block_mov']
        self.block_los = data['block_los']
        self.visible = bool(data['visible'])
        position = data.get('position')
        if position:
            self.position = Coord(position[0], position[1])
