"""
Field of view information
"""

from component import BaseComponent, component


@component('vision')
class VisionComponent(BaseComponent):
    """
    Tracks Actor vision status for FOV calculations
    """

    def __init__(self, owner, view_distance, fov_algorithm):
        super().__init__(owner)
        self.view_distance = view_distance
        self.fov_algorithm = fov_algorithm

    def compute_fov(self, recompute=True, set_explored=False):
        world = self.owner.get_component('world')
        world.world.compute_fov(
            world.position,
            view_distance=self.view_distance,
            algorithm=self.fov_algorithm,
            recompute=recompute,
            set_explored=set_explored)

    def in_fov(self, pos, recompute=True):
        world = self.owner.get_component('world')
        return world.world.in_fov(
            world.position,
            pos,
            view_distance=self.view_distance,
            algorithm=self.fov_algorithm,
            recompute=recompute)

    def get_ents_in_fov(self, selector=None):
        world = self.owner.get_component('world')
        ents = world.world.get_ents_in_fov(
            world.position,
            self.view_distance,
            self.fov_algorithm,
            selector=selector)
        ents.remove(self.owner)

        return ents

    def get_nearest_ents_in_fov(self, selector=None):
        world = self.owner.get_component('world')
        return world.world.get_nearest_ents_in_fov(
            world.position,
            self.view_distance,
            self.fov_algorithm,
            selector=selector)

    def serialize(self):
        return {
            'view_distance': self.view_distance,
            'fov_algorithm': self.fov_algorithm
        }

    def deserialize(self, data):
        self.view_distance = int(data['view_distance'])
        self.fov_algorithm = int(data['fov_algorithm'])
