"""
Limited-use items
"""

from component import BaseComponent, component
from events import EventID, RemoveItemEvent
from message import Priority


@component('consumable', after=['usable'])
class ConsumableComponent(BaseComponent):
    """
    An item that is consumed after a certain number of uses
    """

    def __init__(self, owner, uses):
        super().__init__(owner)

        self.cur_uses = uses
        self.max_uses = uses

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        self.cur_uses -= event.uses
        if self.cur_uses <= 0:
            self.owner.get_component('item').container.send(
                RemoveItemEvent(self, event.ctx, self.owner), chain)
            event.ctx.message_log.add('{} is consumed'.format(self.owner.name),
                                      Priority.LOW)
            self.owner.factory.destroy_entity(self.owner)

        return True

    def serialize(self):
        return {'cur_uses': self.cur_uses, 'max_uses': self.max_uses}

    def deserialize(self, data):
        self.cur_uses = data['cur_uses']
        self.max_uses = data['max_uses']
