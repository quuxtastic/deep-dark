from enum import IntEnum

from component import BaseComponent, component
from events import EventID


class RenderEffects(IntEnum):
    CONFUSED = 1
    INVISIBLE = 2


@component('renderStatusEffect')
class RenderStatusEffectComponent(BaseComponent):
    """
    Keeps a stack of status effects to show
    """

    def __init__(self, owner):
        super().__init__(owner)

        self.effect_list = list()

        self._bind_event(EventID.APPLY_RENDER_EFFECT, self._apply)
        self._bind_event(EventID.REMOVE_RENDER_EFFECT, self._remove)

    def __update_state(self):
        if len(self.effect_list) > 0:
            top_effect = self.effect_list[len(self.effect_list) - 1]
        else:
            top_effect = 0

        self.owner.get_component('renderable').set_frame_id(top_effect)

    def _apply(self, event, _):
        self.effect_list.append(event.effect)

        if event.effect == RenderEffects.INVISIBLE \
           and self.owner is not event.ctx.player:
            self.owner.get_component('renderable').show = False

        self.__update_state()

        return True

    def _remove(self, event, _):
        self.effect_list.remove(event.effect)

        if event.effect == RenderEffects.INVISIBLE \
           and self.owner is not event.ctx.player:
            self.owner.get_component('renderable').show = True

        self.__update_state()

        return True

    def serialize(self):
        return {'effect_list': self.effect_list}

    def deserialize(self, data):
        for effect in data['effect_list']:
            self.effect_list.append(RenderEffects(int(effect)))
