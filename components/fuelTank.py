"""
Storage for fuel
"""

from component import BaseComponent, component
from events import EventID, FuelAvailableEvent


@component('fuelTank')
class FuelTankComponent(BaseComponent):
    """
    Allows an entity to store fuel, used for lights and other machinery
    """

    def __init__(self, owner, capacity, current=0):
        super().__init__(owner)

        self.capacity = capacity
        self.current = current

        self._bind_event(EventID.CONSUME_FUEL, self._consume_fuel)
        self._bind_event(EventID.STORE_FUEL, self._store_fuel)

    def _consume_fuel(self, event, chain):
        if self.current >= event.amount:
            self.current -= event.amount
            chain.append(FuelAvailableEvent(self, event.ctx, self.current))

        return True

    def _store_fuel(self, event, _):
        self.current = min(self.capacity, self.current + event.amount)

        return True

    def serialize(self):
        return {'capacity': self.capacity, 'current': self.current}

    def deserialize(self, data):
        self.capacity = int(data['capacity'])
        self.current = int(data['current'])
