from component import BaseComponent, component
from events import EventID, ItemSuccessEvent


@component('mapRevealSpell', after=['usable'])
class MapExploreSpellComponent(BaseComponent):
    """
    Reveals the map
    """

    def __init__(self, owner):
        super().__init__(owner)

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        event.ctx.world.reveal_map()
        chain.append(ItemSuccessEvent(self, event.ctx))

        return True
