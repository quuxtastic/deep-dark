from component import BaseComponent


class SpellDurationBaseComponent(BaseComponent):
    def __init__(self, owner, tag, duration):
        super().__init__(owner)

        self.tag = tag
        self.turns = duration

        self.ended = False

    def on_apply(self, event, chain):
        self.owner.attach(self)

        self._on_apply(event, chain)

    def _on_apply(self, event, chain):
        pass

    def _on_end(self, event, chain):
        pass

    def _end(self, event, chain):
        if not self.ended:
            self.ended = True
            self._on_end(event, chain)
            self.owner.detach(self)

    def _duration_think(self, event, chain):
        if self.turns is not None:
            self.turns -= 1
            if self.turns <= 0:
                self._end(event, chain)

    def _serialize(self):
        return dict()

    def _deserialize(self, data):
        pass

    def serialize(self):
        ret = {'tag': self.tag, 'turns': self.turns}
        ret.update(self._serialize())

        return ret

    def deserialize(self, data):
        self.tag = data['tag']
        self.turns = int(data['turns'])
        self._deserialize(data)
