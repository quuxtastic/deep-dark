"""
Entity combat component
"""

from component import BaseComponent, component
from events import EventID, TakeDamageEvent
from components.damage import DamageTypes
from message import Priority


@component('combat')
class CombatComponent(BaseComponent):
    """
    Allows an entity to attack and take damage
    """

    def __init__(self,
                 owner,
                 attack,
                 dmg_die,
                 defence,
                 crit_chance=1,
                 crit_mult=2,
                 damage_bonus=0):
        super().__init__(owner)

        self.base_attack = attack
        self.dmg_die = dmg_die
        self.base_defence = defence
        self.base_crit_chance = crit_chance
        self.base_damage_bonus = damage_bonus
        self.base_crit_mult = crit_mult

        self._bind_event(EventID.ATTACK, self._attack)

    @property
    def attack(self):
        stats = self.owner.get_component('stats')

        dex_mod = 0
        if stats:
            dex_mod = stats.dex

        return self.base_attack + dex_mod

    @property
    def defence(self):
        stats = self.owner.get_component('stats')

        dex_mod = 0
        if stats:
            dex_mod = stats.dex

        return self.base_defence + dex_mod

    @property
    def damage_bonus(self):
        stats = self.owner.get_component('stats')

        str_mod = 0
        if stats:
            str_mod = stats.str

        return self.base_damage_bonus + str_mod

    @property
    def crit_chance(self):
        stats = self.owner.get_component('stats')

        per_mod = 0
        if stats:
            per_mod = stats.per

        return self.base_crit_chance + per_mod

    @property
    def crit_mult(self):
        return self.base_crit_mult

    def __crit(self, event):
        dmg = event.ctx.rand.roll(
            self.dmg_die, count=self.crit_mult, mod=self.damage_bonus)
        event.ctx.message_log.add(
            '{} crits {} for {} damage'.format(
                self.owner.name, event.target.name, str(dmg)), Priority.HIGH)

        return dmg

    def __sneak_attack(self, event):
        dmg = event.ctx.rand.roll(self.dmg_die, count=2, mod=self.damage_bonus)
        event.ctx.message_log.add(
            '{} sneak-attacks {} for {} damage'.format(
                self.owner.name, event.target.name, str(dmg)), Priority.HIGH)

        return dmg

    def __dmg(self, event):
        dmg = event.ctx.rand.roll(self.dmg_die, mod=self.damage_bonus)
        event.ctx.message_log.add('{} hits {} for {} damage'.format(
            self.owner.name, event.target.name, str(dmg)))

        return dmg

    def _attack(self, event, chain):
        target = event.target.get_component('combat')

        if event.ctx.rand.perc(self.crit_chance):
            event.target.send(
                TakeDamageEvent(
                    self,
                    event.ctx,
                    self.owner,
                    self.__crit(event),
                    tags=[DamageTypes.PHYSICAL]), chain)
        elif event.ctx.rand.roll(20, mod=self.attack) >= target.defence:
            if not self.owner.get_component('world').visible:
                dmg = self.__sneak_attack(event)
            else:
                dmg = self.__dmg(event)
            event.target.send(
                TakeDamageEvent(
                    self,
                    event.ctx,
                    self.owner,
                    dmg,
                    tags=[DamageTypes.PHYSICAL]), chain)
        else:
            event.ctx.message_log.add('{} misses {}'.format(
                self.owner.name, event.target.name))

        return True

    def serialize(self):
        return {
            'base_attack': self.base_attack,
            'dmg_die': self.dmg_die,
            'base_defence': self.base_defence,
            'base_crit_chance': self.base_crit_chance,
            'base_damage_bonus': self.base_damage_bonus,
            'base_crit_mult': self.base_crit_mult
        }

    def deserialize(self, data):
        self.base_attack = int(data['base_attack'])
        self.dmg_die = int(data['dmg_die'])
        self.base_defence = int(data['base_defence'])
        self.base_crit_chance = int(data['base_crit_chance'])
        self.base_damage_bonus = int(data['base_damage_bonus'])
        self.base_crit_mult = int(data['base_crit_mult'])
