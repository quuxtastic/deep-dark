from component import BaseComponent, component
from events import EventID
from geom import Coord, centered_rect


@component('spawner')
class SpawnerComponent(BaseComponent):
    def __init__(self, owner, spawn_template, radius, spawn_chance,
                 max_spawns):
        super().__init__(owner)

        self.spawn_template = spawn_template
        self.radius = radius
        self.spawn_chance = spawn_chance
        self.max_spawns = max_spawns

        self._bind_event(EventID.MONSTER_THINK, self._think)

    def _think(self, event, chain):
        world = self.owner.get_component('world')

        spawn_rect = centered_rect(world.position,
                                   Coord(self.radius, self.radius))

        def _calc_spawn_pos():
            for pos in spawn_rect.coords():
                if world.world.is_passable(pos):
                    return pos

            return None

        if event.ctx.rand.perc(self.spawn_chance):
            existing = world.world.get_ents_in_rect(
                spawn_rect, selector={'entity': {
                    'name': self.spawn_template
                }})
            if len(existing) < self.max_spawns:
                spawn_pos = _calc_spawn_pos()
                if spawn_pos:
                    new_ent = world.world.create(self.spawn_template, chain,
                                                 spawn_pos)

        return True
