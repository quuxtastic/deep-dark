from component import BaseComponent, component
from events import EventID


@component('slimeTrail')
class SlimeTrailComponent(BaseComponent):
    """
    Spawns in the path of an entity's movement
    """

    def __init__(self, owner, spawn_template, spawn_args=None):
        super().__init__(owner)

        self.spawn_template = spawn_template
        self.spawn_args = spawn_args

        self._bind_event(EventID.MOVE_SUCCESS, self._after_move)

    def _after_move(self, event, chain):
        world = self.owner.get_component('world')

        world.world.create(
            self.spawn_template,
            chain,
            event.old_pos,
            overrides=self.spawn_args)

        return True
