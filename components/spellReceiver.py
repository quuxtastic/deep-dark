"""
Receives spell affects
"""

from component import BaseComponent, component
from events import EventID


@component('spellReceiver')
class SpellReceiverComponent(BaseComponent):
    """
    Allows an actor to be affected by spells
    """

    def __init__(self, owner):
        super().__init__(owner)

        self._bind_event(EventID.APPLY_SPELL, self._apply_spell)

    def _apply_spell(self, event, chain):
        event.spell_component.on_apply(event, chain)

        return True
