"""
Cleanup on actor death
"""

from component import BaseComponent, component
from events import DespawnEvent, EventID
from message import Priority


@component('death', after=['monsterThink'])
class DeathComponent(BaseComponent):
    """
    Handles the aftermath of 'killed' event
    """

    def __init__(self, owner, nutrition=0, spawn_corpse=True):
        super().__init__(owner)

        self.nutrition = nutrition
        self.spawn_corpse = spawn_corpse

        self._bind_event(EventID.KILLED, self._killed)

        self.is_dead = False

    def _killed(self, event, chain):
        if not self.is_dead:
            self.is_dead = True

            if self.owner is event.ctx.player:
                event.ctx.message_log.add('You died!', Priority.HIGH)
            else:
                event.ctx.message_log.add('{} is dead'.format(self.owner.name),
                                          Priority.HIGH)

            world_comp = self.owner.get_component('world')

            if self.spawn_corpse:
                world_comp.world.create(
                    'corpse',
                    chain,
                    world_comp.position,
                    overrides={
                        'food': {
                            'nutrition': self.nutrition
                        },
                        'entity': {
                            'name': 'remains of ' + self.owner.name
                        }
                    })

            self.owner.send(
                DespawnEvent(self, event.ctx, world_comp.world), chain)
            self.owner.factory.destroy_entity(self.owner)

        return True

    def serialize(self):
        return {'nutrition': self.nutrition, 'spawn_corpse': self.spawn_corpse}

    def deserialize(self, data):
        self.nutrition = int(data['nutrition'])
        self.spawn_corpse = bool(data['spawn_corpse'])
