"""
Applies spell effects
"""

from component import BaseComponent, create_component, component
from events import EventID, ApplySpellEvent, ItemSuccessEvent


@component('spellCaster', after=['usable'])
class SpellCasterComponent(BaseComponent):
    """
    Applies an effect to an enemy
    """

    def __init__(self, owner, effect, effect_args):
        super().__init__(owner)

        self.effect = effect
        self.effect_args = effect_args

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, chain):
        for ent in event.targets:
            ent.send(
                ApplySpellEvent(
                    self, event.ctx,
                    create_component(self.effect, ent, **self.effect_args)),
                chain)

        chain.append(ItemSuccessEvent(self, event.ctx))

        return True

    def serialize(self):
        return {'effect': self.effect, 'effect_args': self.effect_args}

    def deserialize(self, data):
        self.effect = data['effect']
        self.effect_args = data['effect_args']
