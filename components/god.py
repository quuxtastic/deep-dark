from component import BaseComponent, component
from events import EventID


@component('god', before=['death', 'playerThink'])
class GodComponent(BaseComponent):
    def __init__(self, owner):
        super().__init__(owner)

        self._bind_event(EventID.KILLED, self._killed)

    def _killed(self, _, __):
        return False
