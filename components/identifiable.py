"""
Item identification
"""

from component import BaseComponent, component
from events import EventID
from message import Priority


@component('identifiable', after=['usable'])
class IdentifiableComponent(BaseComponent):
    """
    Used by items with randomized names to provide to store the 'real' names
    of the items
    """

    def __init__(self, owner, real_name=None, real_description=None):
        super().__init__(owner)
        self.real_name = real_name
        self.real_desc = real_description

        self._bind_event(EventID.USE_ITEM, self._use_item)

    def _use_item(self, event, _):
        ident = event.ctx.player.get_component('identifier')
        if not ident.is_identified(self.owner):
            ident.add(self.owner, self.real_name, self.real_desc)
            event.ctx.message_log.add('Identified {}'.format(self.real_name),
                                      Priority.HIGH)

        return True

    def serialize(self):
        return {
            'real_name': self.real_name,
            'real_description': self.real_desc
        }

    def deserialize(self, data):
        self.real_name = data['real_name']
        self.real_desc = data['real_description']
