from component import BaseComponent, component
from events import EventID
from message import Priority


@component('level')
class LevelComponent(BaseComponent):
    """
    Tracks level and XP
    """

    def __init__(self, owner):
        super().__init__(owner)

        self.xp = 0
        self.level = 1

        self._bind_event(EventID.GAIN_XP, self._gain_xp)

    def next_level_xp(self):
        return (2**self.level) + (2 * self.level)

    def can_level_up(self):
        return self.xp >= self.next_level_xp()

    def level_up(self):
        if self.xp >= self.next_level_xp():
            self.level += 1
            return True

        return False

    def _gain_xp(self, event, _):
        self.xp += event.amount
        event.ctx.message_log.add('{} gained {} XP from {}'.format(
            self.owner.name, event.amount, event.reason))
        if self.can_level_up():
            event.ctx.message_log.add(
                '{} can level up!'.format(self.owner.name), Priority.HIGH)

        return True

    def serialize(self):
        return {'xp': self.xp, 'level': self.level}

    def deserialize(self, data):
        self.xp = int(data['xp'])
        self.level = int(data['level'])
