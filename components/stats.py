from component import BaseComponent, component


@component('stats')
class StatsComponent(BaseComponent):
    def __init__(self, owner, str=0, dex=0, con=0, wil=0, per=0, cha=0):
        super().__init__(owner)

        self.str = str  # strength: carrying capacity, damage mod
        self.dex = dex  # dexterity: defence, attack roll
        self.con = con  # constitution: HP, hunger capacity
        self.wil = wil  # will: magic?, mental defence
        self.per = per  # perception: detect stealth, detect traps, crit %
        self.cha = cha  # charisma: trade

    def serialize(self):
        return {
            'str': self.str,
            'dex': self.dex,
            'con': self.con,
            'wil': self.wil,
            'per': self.per,
            'cha': self.cha
        }

    def deserialize(self, data):
        self.str = int(data['str'])
        self.dex = int(data['dex'])
        self.con = int(data['con'])
        self.wil = int(data['wil'])
        self.per = int(data['per'])
        self.cha = int(data['cha'])
