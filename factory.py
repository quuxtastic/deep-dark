"""
Creating entities
"""

import copy
import pkgutil
import importlib
import sys

from entity import Entity, filter_entities

import templates as template_modules
import components as component_modules
from component import preload_components


def _find_component(name, cmp_list):
    for cur in cmp_list:
        if cur[0] == name:
            return cur[1]

    return None


def _merge_dict(dest, import_dict):
    for import_comp_name, import_vals in import_dict.items():
        comp = _find_component(import_comp_name, dest)
        if comp is not None:
            for key, val in import_vals.items():
                comp[key] = val


class EntityFactory:
    """
    Builds entities from a template of components

    __templates structure:
    {
        'template name': [
            ['component name', { component values dict }]
        ]
    }
    __generators structure:
    {
        'generator name': [
            [max level, [
                [% chance, template name, optional overrides]
            ]]
        ]
    }

    Component property precedence:
        meta templates in order of inclusion,
        base template,
        randomizer,
        genlist,
        overrides
    """

    def __init__(self, rand):
        self.__templates = dict()
        self.__generators = dict()

        self.__entity_list = []
        self.__next_id = 1

        self.__rand = rand

        preload_components(component_modules)

    def __generate(self, name, tables, level, overrides):
        level_table = None
        for max_level, entry in tables:
            level_table = entry
            if level <= max_level:
                break

        perc = self.__rand.randint(1, 100)
        perc_sum = 0

        for option in level_table:
            perc_sum += option[0]
            if perc <= perc_sum:
                tmpl_name = option[1]
                gen = self.__generators.get(tmpl_name)
                if gen:
                    return self.__generate(tmpl_name, gen, level, overrides)

                return self.__create(tmpl_name, option[2], overrides)

        raise ValueError('No entry for {} in {}'.format(perc, name))

    def __create(self, name, template, overrides):
        entity = Entity(factory=self, uid=self.__next_id, template=name)
        self.__next_id += 1

        my_template = copy.deepcopy(template)
        if overrides:
            _merge_dict(my_template, overrides)

        entity.apply_template(my_template)

        self.__entity_list.append(entity)

        return entity

    def create(self, template_name, overrides=None, level=0):
        genlist = self.__generators.get(template_name)
        if genlist:
            return self.__generate(template_name, genlist, level, overrides)

        template = self.__templates.get(template_name)
        if not template:
            raise ValueError('No template named {}'.format(template_name))

        return self.__create(template_name, template, overrides)

    def destroy_entity(self, ent):
        self.__entity_list.remove(ent)

    def get_all_ents(self, selector=None):
        return filter_entities(self.__entity_list, selector)

    def get_ent_by_id(self, in_id):
        for ent in self.__entity_list:
            if in_id == ent.id():
                return ent

        return None

    def reset(self):
        self.__entity_list.clear()
        self.__next_id = 1

    def reset_all(self):
        self.reset()
        self.__templates = dict()
        self.__generators = dict()

    def __load_templates(self, templates):
        def _build_template(out, args):
            if args.get('include'):
                for name in args['include']:
                    _build_template(out, templates[name])

            if args.get('components'):
                for comp_entry in args['components']:
                    comp_name = comp_entry[0]

                    new_entry = _find_component(comp_name, out)
                    if new_entry is None:
                        new_entry = dict()
                        out.append([comp_name, new_entry])

                    if len(comp_entry) == 2:
                        for key, val in comp_entry[1].items():
                            new_entry[key] = val

        for template_name, template_data in templates.items():
            out_data = list()
            _build_template(out_data, template_data)

            self.__templates[template_name] = out_data

    def __load_randomizers(self, templates, randomizers):
        def _apply_randomizer(template, randomizer):
            for comp_name, comp_data in template:
                if comp_name not in randomizer.keys():
                    continue

                rnd_data = randomizer[comp_name]

                add_keys = dict()
                remove_keys = list()

                for key, val in comp_data.items():
                    if key.startswith('genstr:'):
                        real_key = key.split(':')[1]
                        add_keys[real_key] = val.format(**rnd_data)
                        remove_keys.append(key)

                    elif key.startswith('gen:'):
                        real_key = key.split(':')[1]
                        add_keys[real_key] = rnd_data[val]
                        remove_keys.append(key)

                for key in remove_keys:
                    del comp_data[key]
                for key, val in add_keys.items():
                    comp_data[key] = val

        for randomizer_name, randomizer_entries in randomizers.items():

            for template_name, template_data in templates.items():
                if template_data.get('randomizer') == randomizer_name:
                    _apply_randomizer(self.__templates[template_name],
                                      self.__rand.draw(randomizer_entries))

    def __load_genlists(self, genlists):
        for gen_name, levelled_table in genlists.items():
            out_table = []

            for max_level, gen_table in levelled_table:
                level_table = []

                for entry in gen_table:
                    perc = entry[0]
                    tmpl_name = entry[1]

                    base_tmpl = self.__templates.get(tmpl_name)
                    if base_tmpl is not None:
                        if len(entry) == 3:
                            tmpl_args = entry[2]
                            base_tmpl = copy.deepcopy(base_tmpl)
                            _merge_dict(base_tmpl, tmpl_args)

                        level_table.append([perc, tmpl_name, base_tmpl])
                    else:
                        level_table.append([perc, tmpl_name])

                out_table.append([max_level, level_table])
                out_table.sort(key=lambda x: x[0])

            self.__generators[gen_name] = out_table

    def initialize(self):
        merged_templates = dict()
        merged_randomizers = dict()
        merged_genlists = dict()

        for importer, sub_name, is_pkg in pkgutil.iter_modules(
                template_modules.__path__):
            module_name = 'templates.' + sub_name

            if module_name not in sys.modules:
                importlib.import_module(module_name, 'templates')
            cur = sys.modules[module_name]

            templates = copy.deepcopy(getattr(cur, 'templates', dict()))
            for key, val in templates.items():
                if merged_templates.get(key):
                    raise ValueError('Overwriting template {}'.format(key))

                merged_templates[key] = val

            randomizers = copy.deepcopy(getattr(cur, 'randomizers', dict()))
            for key, val in randomizers.items():
                if merged_randomizers.get(key):
                    raise ValueError('Overwriting randomizer {}'.format(key))

                merged_randomizers[key] = val

            genlists = copy.deepcopy(getattr(cur, 'genlists', dict()))
            for key, val in genlists.items():
                if merged_genlists.get(key):
                    for level, data in val:
                        for existing, _ in merged_genlists[key]:
                            if level == existing:
                                raise ValueError(
                                    'Genlist {} level {} conflicts with {}'.
                                    format(key, level, existing))

                        merged_genlists[key].append([level, data])
                else:
                    merged_genlists[key] = val

        self.__load_templates(merged_templates)
        self.__load_randomizers(merged_templates, merged_randomizers)
        self.__load_genlists(merged_genlists)

    def serialize(self):
        ent_map = {}
        for ent in self.get_all_ents():
            ent_map[int(ent.id())] = ent.serialize()

        return {
            'entities': ent_map,
            'templates': self.__templates,
            'generators': self.__generators
        }

    def deserialize(self, data):
        self.__templates = data['templates']
        self.__generators = data['generators']

        for uid, ent_data in data['entities'].items():
            self.__next_id = int(uid)
            self.create(ent_data['template'])

        # initialize components in a separate step because we need all entities
        # to be created and have their uids registered first
        for uid, ent_data in data['entities'].items():
            self.get_ent_by_id(int(uid)).deserialize(ent_data)
