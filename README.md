A roguelike game, in an endless underground.

Uses libtcod for rendering, an entity-component framework, and data-driven map generation.

# Requirements

* Python 3.6+
* tcod (https://pypi.org/project/tcod/)
* numpy (http://www.numpy.org/)
* libtcod https://bitbucket.org/libtcod/libtcod
* SDL2 (https://www.libsdl.org/)

# Running

    cd deep-dark
    python3 main.py

# Windows Setup Guide

1. Download and install the Python 3.7 installer here:
   https://www.python.org/downloads/
   The most recent installer should also include PIP.
2. From the command line, install numpy and tcod:

   > pip install numpy

   > pip install tcod

3. Install git: https://gitforwindows.org/
4. Clone the repo
5. From the command line in the folder where you've cloned the repo, start
    the game with:

    > python3 main.py

    If this doesn't work, try:
   
    > python main.py
