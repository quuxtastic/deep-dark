"""
Storing game data on disk
"""

import json
import os
import time

from world_map import WorldMap


def __get_saves_dir():
    xdg_path = os.environ.get('XDG_DATA_HOME')
    if not xdg_path:
        if os.environ.get('HOME'):
            xdg_path = os.path.join(os.environ['HOME'], '.local', 'share')
        else:
            xdg_path = os.getcwd()

    save_dir_path = os.path.join(xdg_path, 'deep-dark')
    if not os.path.isdir(save_dir_path):
        os.makedirs(save_dir_path)

    return save_dir_path


def load_from_save(save_name, ctx):
    """
    Load a game save from disk
    :param save_name: Name of the save file
    :param ctx: Global context (used to get entity factory)
    :returns: Tuple of (current map index, dict of maps)
    """
    save_file = open(os.path.join(__get_saves_dir(), save_name + '.save'), 'r')
    data = json.loads(save_file.read())

    ctx.entity_factory.deserialize(data['factory'])
    ctx.lore_levels_shown = set(int(x) for x in data['lore_shown'])
    ctx.world_seed = int(data['world_seed'])

    ctx.map_seeds = dict()
    for key, val in data['map_seeds'].items():
        ctx.map_seeds[int(key)] = int(val)

    ctx.maps = dict()

    for data_index, data_vals in data['maps'].items():
        width = int(data_vals['dimensions'][0])
        height = int(data_vals['dimensions'][1])
        index = int(data_index)
        ctx.maps[index] = WorldMap(index, width, height, ctx)
        ctx.maps[index].deserialize(data_vals)

    ctx.map_index = int(data['map_index'])


def save_game(save_name, ctx):
    """
    Save the current game state to disk
    :param save_name: Name of the save file
    :ctx: Global context to save
    """
    map_data = {}
    for index, cur_map in ctx.maps.items():
        map_data[int(index)] = cur_map.serialize()
    data = {
        'factory': ctx.entity_factory.serialize(),
        'maps': map_data,
        'map_index': int(ctx.map_index),
        'lore_shown': list(ctx.lore_levels_shown),
        'world_seed': ctx.world_seed,
        'map_seeds': ctx.map_seeds
    }

    save_file = open(os.path.join(__get_saves_dir(), save_name + '.save'), 'w')

    save_file.write(json.dumps(data))


def read_saves():
    """
    Read saves from disk
    :returns: List of all save names
    """
    saves = os.listdir(__get_saves_dir())

    out = []
    for save_file in saves:
        if save_file.endswith('.save'):
            out.append(save_file[:-5])

    return out


def delete_save(save_name):
    """
    Delete a save from disk
    :param save_name: Save file to delete
    """
    save_path = os.path.join(__get_saves_dir(), save_name + '.save')
    if os.path.exists(save_path):
        os.unlink(save_path)


def read_deaths():
    """
    Read all death information from disk
    :returns: List of deaths
    """
    deaths_path = os.path.join(__get_saves_dir(), 'catacombs')
    if not os.path.exists(deaths_path):
        return []

    return json.loads(open(deaths_path, 'r').read())


def save_death(ctx, cod):
    """
    Save information about a death to disk
    :param ctx: Global context
    :param cod: Cause of death string
    """
    deaths_path = os.path.join(__get_saves_dir(), 'catacombs')
    deaths = read_deaths()

    tile_total = 0
    explore_total = 0
    for cur_map in ctx.maps.values():
        tiles, explored = cur_map.calc_explored()
        tile_total += tiles
        explore_total += explored

    deaths.append({
        'name':
        ctx.player.name,
        'steps':
        int(ctx.player.get_component('playerThink').steps),
        'cod':
        str(cod),
        'map_level':
        int(len(ctx.maps) - 1),
        'explore_avg':
        int(float(explore_total) / float(tile_total) * 100),
        'death_time':
        time.time(),
        'xp':
        ctx.player.get_component('level').xp,
        'level':
        ctx.player.get_component('level').level
    })

    open(deaths_path, 'w').write(json.dumps(deaths))
