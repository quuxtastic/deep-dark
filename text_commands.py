"""
Commands that can be run through the text input interface
"""

from state.stateman import GameStates
from components.god import GodComponent
from components.renderStatusEffect import RenderEffects
from events import PickUpEvent, ApplyRenderEffectEvent, RemoveRenderEffectEvent
from message import Priority
from geom import Coord, centered_rect


def __add_player_item(ctx, *args):
    level = ctx.world.index
    if len(args) == 3:
        level = int(args[2])
    item = ctx.entity_factory.create(args[1], level=level)
    item.send(PickUpEvent(None, ctx, ctx.player), [])


def __set_godmode(ctx, _):
    if ctx.player.get_component('god'):
        ctx.player.detach(ctx.player.get_component('god'))
        ctx.message_log.add('God mode off', Priority.DEBUG)
    else:
        ctx.player.attach(GodComponent(ctx.player))
        ctx.message_log.add('God mode on', Priority.DEBUG)


def __jump_to_level(ctx, *args):
    return (GameStates.MAP_CHANGE, [int(args[1])])


def __spawn_ent(ctx, *args):
    level = ctx.world.index
    if len(args) == 3:
        level = int(args[2])

    pos = ctx.player.get_component('world').position
    for pos in centered_rect(pos, Coord(5, 5)).coords():
        if ctx.world.is_passable(pos):
            ctx.world.create(args[1], [], pos, level=level)
            break
    else:
        ctx.message_log.add('No open space', Priority.DEBUG)


def __explore_map(ctx, _):
    ctx.world.reveal_map()


def __reveal_map(ctx, _):
    ctx.renderer.get_map_view(
    ).force_visible = not ctx.renderer.get_map_view().force_visible


def __find_ent(ctx, *args):
    name = ' '.join(args[1:])
    player_w = ctx.player.get_component('world')

    ents = ctx.world.get_all_ents(selector={'entity': {'name': name}})
    if not ents:
        ctx.message_log.add('No ents named {}'.format(name), Priority.DEBUG)
        return None

    for ent in ents:
        if ent.name == name:
            pos = ent.get_component('world').position
            for npos in centered_rect(pos, Coord(5, 5)).coords():
                if ctx.world.is_passable(npos):
                    ctx.world.on_move_ent(ctx.player, player_w.position, npos)
                    player_w.position = npos
                    return None

    ctx.message_log.add('No space available', Priority.DEBUG)
    return None


def __set_stat(ctx, *args):
    stat = args[1]

    if stat == 'xp':
        ctx.player.get_component('level').xp = int(args[2])

    elif stat == 'fuel':
        ctx.player.get_component('fuelTank').current = int(args[2])

    elif stat == 'hunger':
        ctx.player.get_component('hunger').cur_hunger = int(args[2])

    elif stat == 'hp':
        ctx.player.get_component('hp').hp = int(args[2])

    elif stat in ('str', 'dex', 'con', 'wil', 'per', 'cha'):
        setattr(ctx.player.get_component('stats'), stat, int(args[2]))

    else:
        ctx.message_log.add('No stat named {}'.format(stat), Priority.DEBUG)


def __toggle_hide(ctx, *args):
    world = ctx.player.get_component('world')
    if world.visible:
        ctx.player.send(
            ApplyRenderEffectEvent(None, ctx, RenderEffects.INVISIBLE), [])
        world.visible = False
    else:
        ctx.player.send(
            RemoveRenderEffectEvent(None, ctx, RenderEffects.INVISIBLE), [])
        world.visible = True


__COMMANDS = {
    'give': __add_player_item,
    'god': __set_godmode,
    'warp': __jump_to_level,
    'spawn': __spawn_ent,
    'explore': __explore_map,
    'reveal': __reveal_map,
    'find': __find_ent,
    'set': __set_stat,
    'hide': __toggle_hide
}


def run_text_command(input_str, ctx):
    parts = input_str[1:].split(' ')
    next_state = (GameStates.PLAYER_TURN_INPUT, None)

    cmd = __COMMANDS.get(parts[0])
    if not cmd:
        ctx.message_log.add("I don't understand '{}'".format(parts[0]))
        return next_state

    try:
        next_state = cmd(ctx, *parts)
        if next_state is None:
            next_state = (GameStates.PLAYER_TURN_INPUT, None)
    except Exception as e:
        ctx.message_log.add("Command failed: {}".format(e), Priority.DEBUG)
        print(str(e))

    return next_state
